package cache

import (
	"gitlab.com/cake/goctx"
	"log"
	"m800-campaign-tracker/dbaccess/local/dbop"
	"m800-campaign-tracker/dbaccess/local/model"
	"text/template"
)


var DbTemplateCache map[string]*model.EmailTemplate
var EmailTemplateCache map[string]*template.Template
var CampaignCache map[string]*model.MarketingCampaign
var EventActionTemplateCache map[string]*template.Template


func ReloadInMemoryCache() {
	reloadEmailTemplateCache()
	reloadCampaignCache()
	reloadEventActionCache()
}

func reloadEventActionCache() {
	EventActionTemplateCache = make(map[string]*template.Template)
}

func reloadEmailTemplateCache() {
	DbTemplateCache = make(map[string]*model.EmailTemplate)
	EmailTemplateCache = make(map[string]*template.Template)
}

func reloadCampaignCache() {
	allObjs, err := dbop.NewMarketingCampaign().GetAll(goctx.Background())
	if err != nil {
		log.Fatalf("cannot load all campaigns err=%+v", err)
	}
	ret := make(map[string]*model.MarketingCampaign)
	for _, obj := range allObjs {
		ret[obj.CampaignID] = obj
	}
	CampaignCache = ret
}
