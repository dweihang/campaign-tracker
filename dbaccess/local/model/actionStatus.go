package model

const (
	ActionStatusDone  = "Done"
	ActionStatusError = "Error"
)

type ActionStatus struct {
	CampaignId       string                 `bson:"campaignId"`
	ActionName       string                 `bson:"actionName"`
	ActionId         string                 `bson:"actionId"`
	TrackerName      string                 `bson:"trackerName"`
	TrackingId       string                 `bson:"trackingId"`
	Status           string                 `bson:"status"`
	ErrMsg           *string                `bson:"error,omitempty"`
	TriggeredAt      int64                  `bson:"triggeredAt"`
	ActionOutputData map[string]interface{} `bson:"outputData,omitempty"`
	ActionExtraData  map[string]interface{} `bson:"extraData,omitempty"`
}
