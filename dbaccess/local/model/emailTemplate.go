package model

type EmailTemplate struct {
	Name      string                                   `bson:"name"`
	Templates map[string]*EmailTemplateLanguageContent `bson:"templates"`
	UpdatedAt int64                                    `bson:"updatedAt"`
}

type EmailTemplateLanguageContent struct {
	Subject string `bson:"subject"`
	Content string `bson:"content"`
}
