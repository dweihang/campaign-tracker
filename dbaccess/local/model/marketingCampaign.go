package model

type MarketingCampaign struct {
	CampaignID                 string    `json:"campaignId" bson:"campaignId"`
	Name                       string    `json:"name" bson:"name"`
	Owner                      string    `json:"owner" bson:"owner"`
	OwnerEmail                 string    `json:"ownerEmail" bson:"ownerEmail"`
	RedirectionURL             string    `json:"redirectionUrl" bson:"redirectionUrl"`
	PreCampaignRedirectionURL  string    `json:"preCampaignRedirectionUrl" bson:"preCampaignRedirectionUrl"`
	PostCampaignRedirectionURL string    `json:"postCampaignRedirectionUrl" bson:"postCampaignRedirectionUrl"`
	CampaignStartTime          int64     `json:"campaignStartTime" bson:"campaignStartTime"`
	CampaignExpiryTime         int64     `json:"campaignExpiryTime" bson:"campaignExpiryTime"`
	EntryTrackerName           string    `json:"entryTrackerName" bson:"entryTrackerName"`
	Trackers                   []Tracker `json:"trackers" bson:"trackers"`
}

type Tracker struct {
	Name    string          `json:"name" bson:"name"`
	Actions []TrackerAction `json:"actions" bson:"actions"`
}

type TrackerAction struct {
	Type     string                 `json:"type" bson:"type"`
	Params   map[string]interface{} `json:"params" bson:"params"`
	Sequence int                    `json:"sequence" bson:"sequence"`
	ActionId string                 `json:"actionId" action:"actionId"`
}
