package model

type TrackerEvent struct {
	TrackerName string                 `json:"trackerName" bson:"trackerName"`
	CampaignID  string                 `json:"campaignId" bson:"campaignId"`
	TrackingID  string                 `json:"trackingId" bson:"trackingId"`
	TriggeredAt int64                  `json:"triggeredAt" bson:"triggeredAt"`
	IP          string                 `json:"ip" bson:"ip"`
	Data        map[string]interface{} `json:"data,omitempty" bson:"data,omitempty"`
}
