package model

import "go.mongodb.org/mongo-driver/bson/primitive"

type EmailReminder struct {
	Id             primitive.ObjectID `bson:"_id,omitempty"`
	RemindAt       int64              `bson:"remindAt"`
	RemindInterval int64              `bson:"remindInterval"`
	TemplateName   string             `bson:"templateName"`
	TemplateParams map[string]string  `bson:"templateParams"`
	Tries          int                `bson:"tries"`
	Sender         string             `bson:"sender"`
	Recipients     []string           `bson:"recipients"`
	BccRecipients  []string           `bson:"bccRecipients"`
	Language       string             `bson:"language"`
	Cron           string             `bson:"cron"`
	CancelByEvent  *string            `bson:"cancelByEvent"`
	ContentType    string             `bson:"contentType"`
	TrackingId     string             `bson:"trackingId"`
}
