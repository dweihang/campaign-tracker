package local

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/cake/goctx"
	"gitlab.com/cake/m800log"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"m800-campaign-tracker/dbaccess/db"
)

const (
	CollectionEmailTemplate         = "EmailTemplate"
	CollectionMarketingCampaign     = "MarketingCampaign"
	MarketingCampaignIndexIdLabel   = "campaignId"
	MarketingCampaignIndexNameLabel = "name"

	CollectionMarketingTrackerEvent           = "MarketingTrackerEvent"
	MarketingTrackerEventIndexNameLabel       = "trackerName"
	MarketingTrackerEventIndexCampaignIdLabel = "campaignId"
	MarketingTrackerEventIndexTrackingIdLabel = "trackingId"

	CollectionActionStatus           = "CollectionActionStatus"
	ActionStatusIndexTrackerName     = "trackerName"
	ActionStatusIndexCampaignIdLabel = "campaignId"
	ActionStatusIndexTrackingIdLabel = "trackingId"
	ActionStatusIndexActionIdLabel   = "actionId"

	CollectionEmailReminder           = "EmailReminder"
	EmailReminderIndexEventNameLabel  = "eventName"
	EmailReminderIndexTrackingIdLabel = "trackingId"
	EmailReminderIndexCronLabel       = "cron"
	EmailReminderIndexRemindAtLabel   = "remindAt"
	EmailReminderIndexTriesLabel      = "tries"
)

// Init the local MPS database
func Init(dbName string, options *options.ClientOptions) db.MongoDatabase {
	ctx := goctx.Background()
	client, err := mongo.Connect(ctx, options)

	if err != nil {
		m800log.Logf(ctx, logrus.FatalLevel,
			"Connection error, unable to establish Connection with Mongo Database %s. Error %v", dbName, err)
	}

	err = client.Ping(ctx, nil)
	if err != nil {
		m800log.Logf(ctx, logrus.FatalLevel,
			"Connection error, unable to read from Mongo Database %s. Error: %v", dbName, err)
	}

	ret := db.NewMongoDatabase(client.Database(dbName))
	return ret
}
