package dbop

import (
	"fmt"
	"m800-campaign-tracker/dbaccess"
	"m800-campaign-tracker/dbaccess/local"
	"m800-campaign-tracker/dbaccess/local/model"
	"m800-campaign-tracker/dbaccess/utils"
	"m800-campaign-tracker/errors"

	"go.mongodb.org/mongo-driver/mongo"

	"gitlab.com/cake/m800log"

	"gitlab.com/cake/goctx"
	"gitlab.com/cake/gopkg"

	"go.mongodb.org/mongo-driver/bson"
)

type MarketingCampaign interface {
	GetById(ctx goctx.Context, id string) (*model.MarketingCampaign, gopkg.CodeError)
	GetAll(ctx goctx.Context) ([]*model.MarketingCampaign, gopkg.CodeError)
	GetByName(ctx goctx.Context, name string) (*model.MarketingCampaign, gopkg.CodeError)
	Create(ctx goctx.Context, data *model.MarketingCampaign) gopkg.CodeError
	Update(ctx goctx.Context, data *model.MarketingCampaign) gopkg.CodeError
	Delete(ctx goctx.Context, id string) gopkg.CodeError
}

type marketingCampaign struct{}

func NewMarketingCampaign() MarketingCampaign {
	return &marketingCampaign{}
}

func (p *marketingCampaign) GetAll(ctx goctx.Context) ([]*model.MarketingCampaign, gopkg.CodeError) {
	cursor, err := dbaccess.LocalDatabase.Collection(local.CollectionMarketingCampaign).Find(ctx, bson.M{})
	if err != nil {
		return nil, gopkg.NewWrappedCodeError(errors.MongoFailedToFind, "cannot find all marketing campaign", err)
	}
	var ret []*model.MarketingCampaign
	if err = cursor.All(ctx, &ret); err != nil {
		return nil, gopkg.NewWrappedCodeError(errors.MongoUnmarshallingFailed, "cannot unmarshall all marketing campaign", err)
	}
	return ret, nil
}

func (p *marketingCampaign) GetById(ctx goctx.Context, id string) (*model.MarketingCampaign, gopkg.CodeError) {
	return p.getByField(ctx, local.MarketingCampaignIndexIdLabel, id)
}

func (p *marketingCampaign) GetByName(ctx goctx.Context, name string) (*model.MarketingCampaign, gopkg.CodeError) {
	return p.getByField(ctx, local.MarketingCampaignIndexNameLabel, name)
}

func (p *marketingCampaign) getByField(ctx goctx.Context, fieldName, fieldVal string) (*model.MarketingCampaign, gopkg.CodeError) {
	filter := bson.M{fieldName: fieldVal}

	res := dbaccess.LocalDatabase.Collection(local.CollectionMarketingCampaign).FindOne(ctx, filter)
	if res.Err() != nil {
		if res.Err() == mongo.ErrNoDocuments {
			m800log.Errorf(ctx, "Failed to find any marketing campaign by field (%s = %s): %+v", fieldName, fieldVal, res.Err())
			return nil, errors.NewErrorWithStacktrace(errors.MongoEntityNotExists,
				fmt.Sprintf("Unable to retrieve marketing campaign by field (%s = %s)", fieldName, fieldName), res.Err())
		}
		m800log.Errorf(ctx, "Failed to find any marketing campaign by field (%s = %s): %+v", fieldName, fieldVal, res.Err())
		return nil, errors.NewErrorWithStacktrace(errors.MongoQueryFailed,
			fmt.Sprintf("Unable to retrieve marketing campaign by field (%s = %s)", fieldName, fieldVal), res.Err())
	}
	ret := &model.MarketingCampaign{}
	if err := res.Decode(ret); err != nil {
		m800log.Errorf(ctx, "Failed to get marketing campaign by field (%s = %s): %+v", fieldName, fieldVal, err)
		return nil, errors.NewErrorWithStacktrace(errors.MongoEntityDecodingFailed,
			fmt.Sprintf("Unable to retrieve marketing campaign due to decoding error (%s = %s)", fieldName, fieldVal), err)
	}
	return ret, nil
}

func (p *marketingCampaign) Create(ctx goctx.Context, data *model.MarketingCampaign) gopkg.CodeError {
	campaign, err := p.GetById(ctx, data.CampaignID)
	if campaign != nil {
		return gopkg.NewCodeError(errors.MongoDuplicateKeyError, fmt.Sprintf("Marketing campaign with id %s already exists", data.CampaignID))
	}
	if err != nil && err.ErrorCode() != errors.MongoEntityNotExists {
		// Mongo error
		return err
	}
	if _, err := dbaccess.LocalDatabase.Collection(local.CollectionMarketingCampaign).InsertOne(ctx, data); err != nil {
		return errors.NewErrorWithStacktrace(errors.MongoFailedToInsert, fmt.Sprintf("Failed to create marketing campaign=%+v", data), err)
	}
	return nil
}

func (p *marketingCampaign) Update(ctx goctx.Context, data *model.MarketingCampaign) gopkg.CodeError {
	filter:= bson.M{local.MarketingCampaignIndexIdLabel: data.CampaignID}
	bsonData, codeErr := utils.ConvertToBson(data)
	if codeErr!=nil{
		m800log.Errorf(ctx, "Failed to update marketing campaign (campaign = %+v): %+v", data, codeErr)
		return codeErr
	}
	_, err := dbaccess.LocalDatabase.Collection(local.CollectionMarketingCampaign).UpdateOne(ctx,
		filter,
		bson.M{
			"$set": bsonData,
		})
	if err != nil {
		m800log.Errorf(ctx, "Failed to update marketing campaign (campaign = %+v): %+v", data, err)
		return errors.NewErrorWithStacktrace(errors.MongoFailedToUpdate,
			fmt.Sprintf("Failed to update marketing campaign (campaign = %+v)", data), err)
	}
	return nil
}

func (p *marketingCampaign) Delete(ctx goctx.Context, id string) gopkg.CodeError {
	filter := bson.M{local.MarketingCampaignIndexIdLabel: id}
	_, err := dbaccess.LocalDatabase.Collection(local.CollectionMarketingCampaign).DeleteOne(ctx, filter)
	if err != nil {
		m800log.Errorf(ctx, "Failed to delete marketing campaign (id = %s): %+v", id, err)
		return errors.NewErrorWithStacktrace(errors.MongoFailedToDelete,
			fmt.Sprintf("Failed to update marketing campaign (id = %s)", id), err)
	}
	return nil
}
