package dbop

import (
	"fmt"
	"gitlab.com/cake/goctx"
	"gitlab.com/cake/gopkg"
	"gitlab.com/cake/m800log"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"m800-campaign-tracker/dbaccess"
	"m800-campaign-tracker/dbaccess/local"
	"m800-campaign-tracker/dbaccess/local/model"
	"m800-campaign-tracker/errors"
)

type ActionStatus interface {
	Upsert(ctx goctx.Context, status *model.ActionStatus) gopkg.CodeError
	Get(ctx goctx.Context, trackingId string, actionId string) (*model.ActionStatus, gopkg.CodeError)
}

func NewActionStatus() ActionStatus {
	return &actionStatus{}
}

type actionStatus struct{}

func (a *actionStatus) Upsert(ctx goctx.Context, status *model.ActionStatus) gopkg.CodeError {
	upsert := true
	filter := bson.M{
		local.ActionStatusIndexTrackingIdLabel: status.TrackingId,
		local.ActionStatusIndexActionIdLabel:   status.ActionId,
	}
	_, err := dbaccess.LocalDatabase.Collection(local.CollectionActionStatus).ReplaceOne(ctx, filter, *status, &options.ReplaceOptions{
		Upsert: &upsert,
	})
	if err != nil {
		return gopkg.NewWrappedCarrierCodeError(errors.MongoFailedToInsert, fmt.Sprintf("failed to insert obj=%+v filter=%+v", *status, filter), err)
	}
	return nil
}

func (a *actionStatus) Get(ctx goctx.Context, trackingId string, actionId string) (*model.ActionStatus, gopkg.CodeError) {
	ret := &model.ActionStatus{}
	filter := bson.M{
		local.ActionStatusIndexTrackingIdLabel: trackingId,
		local.ActionStatusIndexActionIdLabel:   actionId,
	}
	err := dbaccess.LocalDatabase.Collection(local.CollectionActionStatus).FindOne(ctx, filter).Decode(ret)
	if err == mongo.ErrNoDocuments {
		m800log.Errorf(ctx, "email template not exists with filter=%+v", filter)
		return nil, gopkg.NewCodeError(errors.MongoEntityNotExists, "email template not exists")
	} else if err != nil {
		m800log.Errorf(ctx, "failed to find action status with filter=%+v", filter)
		return nil, gopkg.NewCodeError(errors.MongoFailedToFind, "failed to find email template")
	}
	return ret, nil
}
