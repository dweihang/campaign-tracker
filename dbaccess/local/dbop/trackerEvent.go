package dbop

import (
	"fmt"
	"m800-campaign-tracker/dbaccess"
	"m800-campaign-tracker/dbaccess/local"
	"m800-campaign-tracker/dbaccess/local/model"
	"m800-campaign-tracker/errors"

	"go.mongodb.org/mongo-driver/mongo"

	"gitlab.com/cake/goctx"
	"gitlab.com/cake/gopkg"

	"go.mongodb.org/mongo-driver/bson"
)

type TrackerEvent interface {
	GetByFields(ctx goctx.Context, dataFieldKey []string, dataFieldVal []interface{}) (*model.TrackerEvent, gopkg.CodeError)
	GetByField(ctx goctx.Context, dataFieldKey string, dataFieldVal interface{}) (*model.TrackerEvent, gopkg.CodeError)
	GetByTrackingId(ctx goctx.Context, trackingId string) ([]*model.TrackerEvent, gopkg.CodeError)
	GetByCampaignId(ctx goctx.Context, campaignId string) ([]*model.TrackerEvent, gopkg.CodeError)
	GetByName(ctx goctx.Context, eventName, campaignId, trackingId string) ([]*model.TrackerEvent, gopkg.CodeError)
	Create(ctx goctx.Context, data *model.TrackerEvent, isMultiple bool) gopkg.CodeError
}

type trackerEvent struct{}

func NewTrackerEvent() TrackerEvent {
	return &trackerEvent{}
}

func (p *trackerEvent) GetByFields(ctx goctx.Context, dataFieldKeys []string, dataFieldVals []interface{}) (*model.TrackerEvent, gopkg.CodeError) {
	ret := &model.TrackerEvent{}
	filter := bson.M{}
	for i, dataFieldKey := range dataFieldKeys {
		filter[dataFieldKey] = dataFieldVals[i]
	}

	if err := dbaccess.LocalDatabase.Collection(local.CollectionMarketingTrackerEvent).FindOne(ctx, filter).Decode(ret); err != nil {
		if err == mongo.ErrNoDocuments {
			return nil, errors.NewErrorWithStacktrace(errors.MongoEntityNotExists, fmt.Sprintf("No tracker events found for %s : %s", dataFieldKeys, dataFieldVals), err)
		} else {
			return nil, gopkg.NewWrappedCodeError(errors.MongoFailedToFind, fmt.Sprintf("failed to find tracker event"), err)
		}
	} else {
		return ret, nil
	}
}

func (p *trackerEvent) GetByField(ctx goctx.Context, dataFieldKey string, dataFieldVal interface{}) (*model.TrackerEvent, gopkg.CodeError) {
	ret := &model.TrackerEvent{}
	filter := bson.M{
		dataFieldKey: dataFieldVal,
	}
	if err := dbaccess.LocalDatabase.Collection(local.CollectionMarketingTrackerEvent).FindOne(ctx, filter).Decode(ret); err != nil {
		if err == mongo.ErrNoDocuments {
			return nil, errors.NewErrorWithStacktrace(errors.MongoEntityNotExists, fmt.Sprintf("No tracker events found for %s : %s", dataFieldKey, dataFieldVal), err)
		} else {
			return nil, gopkg.NewWrappedCodeError(errors.MongoFailedToFind, fmt.Sprintf("failed to find tracker event"), err)
		}
	} else {
		return ret, nil
	}
}

func (p *trackerEvent) GetByTrackingId(ctx goctx.Context, trackingId string) ([]*model.TrackerEvent, gopkg.CodeError) {
	return p.getListByField(ctx, local.MarketingTrackerEventIndexTrackingIdLabel, trackingId)
}

func (p *trackerEvent) GetByCampaignId(ctx goctx.Context, campaignId string) ([]*model.TrackerEvent, gopkg.CodeError) {
	return p.getListByField(ctx, local.MarketingTrackerEventIndexCampaignIdLabel, campaignId)
}

func (p *trackerEvent) getListByField(ctx goctx.Context, fieldName, fieldVal string) ([]*model.TrackerEvent, gopkg.CodeError) {
	filter := bson.M{fieldName: fieldVal}

	var dbObjs []*model.TrackerEvent
	cursor, dbErr := dbaccess.LocalDatabase.Collection(local.CollectionMarketingTrackerEvent).Find(ctx, filter)
	if dbErr != nil {
		if dbErr == mongo.ErrNoDocuments {
			dbObjs = nil
			err := errors.NewErrorWithStacktrace(errors.MongoEntityNotExists, fmt.Sprintf("No tracker events found for %s : %s", fieldName, fieldVal), dbErr)
			return nil, err
		} else {
			err := errors.NewErrorWithStacktrace(errors.MongoQueryFailed, fmt.Sprintf("DB lookup error tracker event db obj"), dbErr)
			return nil, err
		}
	}
	for cursor.Next(ctx) {
		dbObj := &model.TrackerEvent{}
		if decodeErr := cursor.Decode(dbObj); decodeErr != nil {
			dbObjs = nil
			err := errors.NewErrorWithStacktrace(errors.MongoEntityDecodingFailed, "failed to decode TrackerEvent", decodeErr)
			return nil, err
		}
		dbObjs = append(dbObjs, dbObj)
	}
	if len(dbObjs) == 0 {
		err := errors.NewErrorWithStacktrace(errors.MongoEntityNotExists, fmt.Sprintf("No tracker events found for %s : %s", fieldName, fieldVal), dbErr)
		return nil, err
	}
	return dbObjs, nil
}

func (p *trackerEvent) GetByName(ctx goctx.Context, eventName, campaignId, trackingId string) ([]*model.TrackerEvent, gopkg.CodeError) {

	if len(eventName) == 0 || len(campaignId) == 0 {
		return nil, gopkg.NewCodeError(errors.MongoInvalidQuery, fmt.Sprintf("eventName and campaignId cannot be null, eventName: %s, campaignId: %s", eventName, campaignId))
	}

	innerFilter := bson.A{
		bson.M{local.MarketingTrackerEventIndexNameLabel: eventName},
		bson.M{local.MarketingTrackerEventIndexCampaignIdLabel: campaignId},
	}

	if len(trackingId) > 0 {
		innerFilter = append(innerFilter, bson.M{local.MarketingTrackerEventIndexTrackingIdLabel: trackingId})
	}

	filter := bson.M{"$and": innerFilter}

	var dbObjs []*model.TrackerEvent
	cursor, dbErr := dbaccess.LocalDatabase.Collection(local.CollectionMarketingTrackerEvent).Find(ctx, filter)
	if dbErr != nil {
		if dbErr == mongo.ErrNoDocuments {
			dbObjs = nil
			err := errors.NewErrorWithStacktrace(errors.MongoEntityNotExists, fmt.Sprintf("No tracker events found for eventName : %s, campaignId %s", eventName, campaignId), dbErr)
			return nil, err
		} else {
			err := errors.NewErrorWithStacktrace(errors.MongoQueryFailed, fmt.Sprintf("DB lookup error tracker event db obj"), dbErr)
			return nil, err
		}
	}
	for cursor.Next(ctx) {
		dbObj := &model.TrackerEvent{}
		if decodeErr := cursor.Decode(dbObj); decodeErr != nil {
			dbObjs = nil
			err := errors.NewErrorWithStacktrace(errors.MongoEntityDecodingFailed, "failed to decode TrackerEvent", decodeErr)
			return nil, err
		}
		dbObjs = append(dbObjs, dbObj)
	}
	if len(dbObjs) == 0 {
		err := errors.NewErrorWithStacktrace(errors.MongoEntityNotExists, fmt.Sprintf("No tracker event found for eventName : %s, campaignId %s", eventName, campaignId), dbErr)
		return nil, err
	}
	return dbObjs, nil
}

func (p *trackerEvent) Create(ctx goctx.Context, data *model.TrackerEvent, isMultiple bool) gopkg.CodeError {
	if !isMultiple {
		trackerEvents, err := p.GetByName(ctx, data.TrackerName, data.CampaignID, data.TrackingID)
		if err != nil && err.ErrorCode() != errors.MongoEntityNotExists {
			return err
		}
		if len(trackerEvents) > 0 {
			return gopkg.NewCodeError(errors.MongoDuplicateKeyError, fmt.Sprintf("Duplicate found for tracker event of type %s", data.TrackerName))
		}
	}
	if _, err := dbaccess.LocalDatabase.Collection(local.CollectionMarketingTrackerEvent).InsertOne(ctx, data); err != nil {
		return errors.NewErrorWithStacktrace(errors.MongoFailedToInsert, fmt.Sprintf("Failed to create tracker event = %+v", data), err)
	}

	return nil
}
