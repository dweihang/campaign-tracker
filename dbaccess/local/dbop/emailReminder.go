package dbop

import (
	"fmt"
	"gitlab.com/cake/m800log"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"m800-campaign-tracker/dbaccess"
	"m800-campaign-tracker/dbaccess/local"
	"m800-campaign-tracker/dbaccess/local/model"
	"m800-campaign-tracker/errors"
	"time"

	"go.mongodb.org/mongo-driver/mongo"

	"gitlab.com/cake/goctx"
	"gitlab.com/cake/gopkg"

	"go.mongodb.org/mongo-driver/bson"
)

type EmailReminder interface {
	GetRemindableEmails(ctx goctx.Context, cronInterval string) ([]*model.EmailReminder, gopkg.CodeError)
	Create(ctx goctx.Context, data *model.EmailReminder) gopkg.CodeError
	UpdateReminder(ctx goctx.Context, data *model.EmailReminder) gopkg.CodeError
	Delete(ctx goctx.Context, id  primitive.ObjectID) gopkg.CodeError
	DeleteByEvent(ctx goctx.Context, eventName, trackingId string) gopkg.CodeError
}

type emailReminder struct{}

func NewEmailReminder() EmailReminder {
	return &emailReminder{}
}

func (p *emailReminder) GetRemindableEmails(ctx goctx.Context, cronInterval string) ([]*model.EmailReminder, gopkg.CodeError) {
	now := time.Now().Unix() * 1000
	filter := bson.M{"$and": bson.A{
		bson.M{local.EmailReminderIndexRemindAtLabel: bson.M{"$lt": now}},
		bson.M{local.EmailReminderIndexCronLabel: cronInterval},
	}}

	var dbObjs [] *model.EmailReminder
	cursor, dbErr := dbaccess.LocalDatabase.Collection(local.CollectionEmailReminder).Find(ctx, filter)
	if dbErr != nil {
		if dbErr == mongo.ErrNoDocuments {
			dbObjs = nil
			return dbObjs, nil
		} else {
			err := errors.NewErrorWithStacktrace(errors.MongoQueryFailed, fmt.Sprintf("DB lookup error email reminder db obj"), dbErr)
			return nil, err
		}
	}
	for cursor.Next(ctx) {
		dbObj := &model.EmailReminder{}
		if decodeErr := cursor.Decode(dbObj); decodeErr != nil {
			dbObjs = nil
			err := errors.NewErrorWithStacktrace(errors.MongoEntityDecodingFailed, "failed to decode EmailReminder", decodeErr)
			return nil, err
		}
		dbObjs = append(dbObjs, dbObj)
	}
	return dbObjs, nil
}

func (p *emailReminder) Create(ctx goctx.Context, data *model.EmailReminder) gopkg.CodeError {
	if _, err := dbaccess.LocalDatabase.Collection(local.CollectionEmailReminder).InsertOne(ctx, data); err != nil {
		return errors.NewErrorWithStacktrace(errors.MongoFailedToInsert, fmt.Sprintf("Failed to create email reminder = %+v", data), err)
	}
	return nil
}

func (p *emailReminder) UpdateReminder(ctx goctx.Context, data *model.EmailReminder) gopkg.CodeError {
	filter := bson.M{"_id": data.Id}
	_, err := dbaccess.LocalDatabase.Collection(local.CollectionEmailReminder).UpdateOne(ctx,
		filter,
		bson.M{
			"$set": bson.M{
				local.EmailReminderIndexRemindAtLabel: data.RemindAt,
				local.EmailReminderIndexTriesLabel:    data.Tries,
			},
		})
	if err != nil {
		m800log.Errorf(ctx, "Failed to update email reminder (email reminder = %+v): %+v", data, err)
		return errors.NewErrorWithStacktrace(errors.MongoFailedToUpdate,
			fmt.Sprintf("Failed to update marketing campaign (email reminder = %+v)", data), err)
	}
	return nil
}

func (p *emailReminder) Delete(ctx goctx.Context, id primitive.ObjectID) gopkg.CodeError {
	filter := bson.M{"_id": id}
	_, err := dbaccess.LocalDatabase.Collection(local.CollectionEmailReminder).DeleteOne(ctx, filter)
	if err != nil {
		m800log.Errorf(ctx, "Failed to delete email reminder (id = %s): %+v", id, err)
		return errors.NewErrorWithStacktrace(errors.MongoFailedToDelete,
			fmt.Sprintf("Failed to delete email reminder (id = %s)", id), err)
	}
	return nil
}

func (p *emailReminder) DeleteByEvent(ctx goctx.Context, eventName, trackingId string) gopkg.CodeError {
	filter := bson.M{
		local.EmailReminderIndexEventNameLabel:  eventName,
		local.EmailReminderIndexTrackingIdLabel: trackingId,
	}
	_, err := dbaccess.LocalDatabase.Collection(local.CollectionEmailReminder).DeleteOne(ctx, filter)
	if err != nil {
		m800log.Errorf(ctx, "Failed to delete email reminder (eventName = %s, trackingId = %s): %+v", eventName,trackingId, err)
		return errors.NewErrorWithStacktrace(errors.MongoFailedToDelete,
			fmt.Sprintf("Failed to delete email reminder (eventName = %s, trackingId = %s)", eventName,trackingId), err)
	}
	return nil
}
