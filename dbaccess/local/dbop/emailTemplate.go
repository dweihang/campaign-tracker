package dbop

import (
	"fmt"
	"gitlab.com/cake/goctx"
	"gitlab.com/cake/gopkg"
	"gitlab.com/cake/m800log"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"m800-campaign-tracker/dbaccess"
	"m800-campaign-tracker/dbaccess/local"
	"m800-campaign-tracker/dbaccess/local/model"
	"m800-campaign-tracker/errors"
)

type EmailTemplate interface {
	ReplaceTemplate(ctx goctx.Context, template *model.EmailTemplate) gopkg.CodeError
	GetTemplateByName(ctx goctx.Context, name string) (*model.EmailTemplate, gopkg.CodeError)
}

func NewEmailTemplate() EmailTemplate {
	return &emailTemplate{}
}

type emailTemplate struct {
}

func (e *emailTemplate) GetTemplateByName(ctx goctx.Context, name string) (*model.EmailTemplate, gopkg.CodeError) {
	ret := &model.EmailTemplate{}
	filter := bson.M{
		"name": name,
	}
	err := dbaccess.LocalDatabase.Collection(local.CollectionEmailTemplate).FindOne(ctx, filter).Decode(ret)
	if err == mongo.ErrNoDocuments {
		m800log.Errorf(ctx, "email template not exists with filter=%+v", filter)
		return nil, gopkg.NewCodeError(errors.MongoEntityNotExists, "email template not exists")
	} else if err != nil {
		m800log.Errorf(ctx, "failed to find email template with filter=%+v", filter)
		return nil, gopkg.NewCodeError(errors.MongoFailedToFind, "failed to find email template")
	}
	return ret, nil
}

func (e *emailTemplate) ReplaceTemplate(ctx goctx.Context, template *model.EmailTemplate) gopkg.CodeError {
	upsert := true
	_, err := dbaccess.LocalDatabase.Collection(local.CollectionEmailTemplate).ReplaceOne(ctx, bson.M{
		"name": template.Name,
	}, template, &options.ReplaceOptions{
		Upsert: &upsert,
	})
	if err != nil {
		m800log.Errorf(ctx, "failed to replace template=%+v, err=%+v", *template, err)
		return gopkg.NewWrappedCodeError(errors.MongoReplaceOneFailed, fmt.Sprintf("failed to replace email template with name=%s", template.Name), err)
	}
	return nil
}
