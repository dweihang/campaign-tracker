package utils

import (
	"fmt"
	"m800-campaign-tracker/dbaccess/db"

	"gitlab.com/cake/goctx"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

type CollectionIndex struct {
	Indices        []mongo.IndexModel
	CollectionName string
}

func AddCollectionIndex(ctx goctx.Context, collection db.MongoCollection, indexModels ...mongo.IndexModel) {
	// Check if index already exists
	cur, err := collection.Indexes().List(ctx)
	if err != nil {
		panic(fmt.Sprintf("Failed to get list of indexes for '%s': %+v", collection.Name(), err))
	}
	// Collect the existing indexes for the collection
	var existingIndexes []bson.E
	for cur.Next(ctx) {
		b := bson.D{}
		err := cur.Decode(&b)
		if err != nil {
			panic(fmt.Sprintf("Failed to decode existing index for '%s': %+v", collection.Name(), err))
		}
		m := b.Map()
		keys := m["key"].(bson.D).Map()
		for key, value := range keys {
			existingIndexes = append(existingIndexes, bson.E{Key: key, Value: value})
		}
	}
	for _, indexModel := range indexModels {
		// Only insert index if it is does not already exist
		unique := func(index mongo.IndexModel, existingIndexes []bson.E) bool {
			keys := index.Keys.(bson.M)
			unique := true
			for _, existingIndex := range existingIndexes {
				// All values have to be same for index to be considered a duplicate
				// i.e. would generate the same index name (same name and order)
				value, duplicateName := keys[existingIndex.Key]
				uniqueValue := value != int(existingIndex.Value.(int32))
				unique = unique && !duplicateName && uniqueValue
				if !unique {
					// exit early
					return unique
				}
			}
			return unique
		}(indexModel, existingIndexes)
		if !unique {
			continue
		}
		_, err := collection.Indexes().CreateOne(ctx, indexModel)
		if err != nil {
			panic(fmt.Sprintf("Failed to create index for '%s' (index = %+v): %+v", collection.Name(), indexModel, err))
		}
	}
}
