package utils

import (
	"m800-campaign-tracker/errors"

	"gitlab.com/cake/gopkg"

	"go.mongodb.org/mongo-driver/bson"
)

func ConvertToBson(obj interface{}, excludeParams ...string) (*bson.M, gopkg.CodeError) {
	data, err := bson.Marshal(obj)
	if err != nil {
		return nil, gopkg.NewWrappedCodeError(errors.MongoMarshallingFailed,
			"Failed to marshal object to byte array", err)
	}
	bm := bson.M{}
	err = bson.Unmarshal(data, &bm)
	if err != nil {
		return nil, gopkg.NewWrappedCodeError(errors.MongoUnmarshallingFailed,
			"Failed to unmarshal byte-array to bson object", err)
	}

	// Delete the params that should be excluded from the bson
	for _, param := range excludeParams {
		delete(bm, param)
	}

	return &bm, nil
}
