package dbaccess

import (
	"m800-campaign-tracker/config"
	"m800-campaign-tracker/dbaccess/db"
	"m800-campaign-tracker/dbaccess/local"
	"time"
)

var LocalDatabase db.MongoDatabase

func Init() {
	LocalDatabase = local.Init("m800-campaign-tracker", config.LocalMongo.ClientOptions())
}

// Disconnect the local database
func Disconnect() {
	LocalDatabase.Disconnect()
}

func Ping() (time.Duration, error) {
	localTime, err := LocalDatabase.Ping()
	if err != nil {
		return localTime, err
	}

	return localTime, nil
}
