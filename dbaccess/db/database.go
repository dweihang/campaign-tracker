package db

import (
	"context"
	"gitlab.com/cake/goctx"
	"time"

	"gitlab.com/cake/m800log"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readconcern"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"go.mongodb.org/mongo-driver/mongo/writeconcern"
)

type MongoDatabase interface {
	Ping() (time.Duration, error)
	Disconnect()

	Client() *mongo.Client
	Name() string
	Collection(name string, opts ...*options.CollectionOptions) MongoCollection
	Aggregate(ctx context.Context, pipeline interface{}, opts ...*options.AggregateOptions) (MongoCursor, error)
	RunCommand(ctx context.Context, runCommand interface{}, opts ...*options.RunCmdOptions) MongoSingleResult
	RunCommandCursor(ctx context.Context, runCommand interface{}, opts ...*options.RunCmdOptions) (MongoCursor, error)
	Drop(ctx context.Context) error
	ListCollections(ctx context.Context, filter interface{}, opts ...*options.ListCollectionsOptions) (MongoCursor, error)
	ListCollectionNames(ctx context.Context, filter interface{}, opts ...*options.ListCollectionsOptions) ([]string, error)
	ReadConcern() *readconcern.ReadConcern
	ReadPreference() *readpref.ReadPref
	WriteConcern() *writeconcern.WriteConcern
	Watch(ctx context.Context, pipeline interface{}, opts ...*options.ChangeStreamOptions) (MongoChangeStream, error)
}

func NewMongoDatabase(db *mongo.Database) MongoDatabase {
	return &mongoDatabase{db}
}

type mongoDatabase struct {
	*mongo.Database
}

func (md *mongoDatabase) Aggregate(ctx context.Context, pipeline interface{}, opts ...*options.AggregateOptions) (MongoCursor, error) {
	return md.Database.Aggregate(ctx, pipeline, opts...)
}

func (md *mongoDatabase) RunCommand(ctx context.Context, runCommand interface{}, opts ...*options.RunCmdOptions) MongoSingleResult {
	return md.Database.RunCommand(ctx, runCommand, opts...)
}

func (md *mongoDatabase) RunCommandCursor(ctx context.Context, runCommand interface{}, opts ...*options.RunCmdOptions) (MongoCursor, error) {
	return md.Database.RunCommandCursor(ctx, runCommand, opts...)
}

func (md *mongoDatabase) ListCollections(ctx context.Context, filter interface{}, opts ...*options.ListCollectionsOptions) (MongoCursor, error) {
	return md.Database.ListCollections(ctx, filter, opts...)
}

func (md *mongoDatabase) Watch(ctx context.Context, pipeline interface{}, opts ...*options.ChangeStreamOptions) (MongoChangeStream, error) {
	return md.Database.Watch(ctx, pipeline, opts...)
}

func (md *mongoDatabase) Collection(colName string, opts ...*options.CollectionOptions) MongoCollection {
	return &mongoCollection{md.Database.Collection(colName, opts...)}
}

func (md *mongoDatabase) Disconnect() {
	ctx := goctx.Background()
	err := md.Database.Client().Disconnect(ctx)
	if err != nil {
		m800log.Errorf(ctx, "Mongo error during disconnection: %v", err)
	}
}

func (md *mongoDatabase) Ping() (time.Duration, error) {
	t := time.Now()
	ctx := goctx.Background()
	err := md.Database.Client().Ping(ctx, nil)
	if err != nil {
		m800log.Errorf(ctx, "Connection error, unable to read from MongoDB %v", err)
	}
	return time.Since(t), nil
}

type MongoCollection interface {
	Clone(opts ...*options.CollectionOptions) (*mongo.Collection, error)
	Name() string
	Database() *mongo.Database
	BulkWrite(ctx context.Context, models []mongo.WriteModel, opts ...*options.BulkWriteOptions) (*mongo.BulkWriteResult, error)
	InsertOne(ctx context.Context, document interface{}, opts ...*options.InsertOneOptions) (*mongo.InsertOneResult, error)
	InsertMany(ctx context.Context, documents []interface{}, opts ...*options.InsertManyOptions) (*mongo.InsertManyResult, error)
	DeleteOne(ctx context.Context, filter interface{}, opts ...*options.DeleteOptions) (*mongo.DeleteResult, error)
	DeleteMany(ctx context.Context, filter interface{}, opts ...*options.DeleteOptions) (*mongo.DeleteResult, error)
	UpdateOne(ctx context.Context, filter interface{}, update interface{}, opts ...*options.UpdateOptions) (MongoUpdateResult, error)
	UpdateMany(ctx context.Context, filter interface{}, update interface{}, opts ...*options.UpdateOptions) (MongoUpdateResult, error)
	ReplaceOne(ctx context.Context, filter interface{}, replacement interface{}, opts ...*options.ReplaceOptions) (MongoUpdateResult, error)
	Aggregate(ctx context.Context, pipeline interface{}, opts ...*options.AggregateOptions) (MongoCursor, error)
	CountDocuments(ctx context.Context, filter interface{}, opts ...*options.CountOptions) (int64, error)
	EstimatedDocumentCount(ctx context.Context, opts ...*options.EstimatedDocumentCountOptions) (int64, error)
	Distinct(ctx context.Context, fieldName string, filter interface{}, opts ...*options.DistinctOptions) ([]interface{}, error)
	Find(ctx context.Context, filter interface{}, opts ...*options.FindOptions) (MongoCursor, error)
	FindOne(ctx context.Context, filter interface{}, opts ...*options.FindOneOptions) MongoSingleResult
	FindOneAndDelete(ctx context.Context, filter interface{}, opts ...*options.FindOneAndDeleteOptions) MongoSingleResult
	FindOneAndReplace(ctx context.Context, filter interface{}, replacement interface{}, opts ...*options.FindOneAndReplaceOptions) MongoSingleResult
	FindOneAndUpdate(ctx context.Context, filter interface{}, update interface{}, opts ...*options.FindOneAndUpdateOptions) MongoSingleResult
	Watch(ctx context.Context, pipeline interface{}, opts ...*options.ChangeStreamOptions) (MongoChangeStream, error)
	Indexes() mongo.IndexView
	Drop(ctx context.Context) error
}

type mongoCollection struct {
	*mongo.Collection
}

func (mc *mongoCollection) Aggregate(ctx context.Context, pipeline interface{}, opts ...*options.AggregateOptions) (MongoCursor, error) {
	return mc.Collection.Aggregate(ctx, pipeline, opts...)
}

func (mc *mongoCollection) FindOneAndDelete(ctx context.Context, filter interface{}, opts ...*options.FindOneAndDeleteOptions) MongoSingleResult {
	return mc.Collection.FindOneAndDelete(ctx, filter, opts...)
}

func (mc *mongoCollection) FindOneAndReplace(ctx context.Context, filter interface{}, replacement interface{}, opts ...*options.FindOneAndReplaceOptions) MongoSingleResult {
	return mc.Collection.FindOneAndReplace(ctx, filter, replacement, opts...)
}

func (mc *mongoCollection) Watch(ctx context.Context, pipeline interface{}, opts ...*options.ChangeStreamOptions) (MongoChangeStream, error) {
	return mc.Collection.Watch(ctx, pipeline, opts...)
}

func (mc *mongoCollection) Find(ctx context.Context, filter interface{}, opts ...*options.FindOptions) (MongoCursor, error) {
	return mc.Collection.Find(ctx, filter, opts...)
}

func (mc *mongoCollection) FindOne(ctx context.Context, filter interface{}, opts ...*options.FindOneOptions) MongoSingleResult {
	return mc.Collection.FindOne(ctx, filter, opts...)
}

func (mc *mongoCollection) FindOneAndUpdate(ctx context.Context, filter interface{}, update interface{}, opts ...*options.FindOneAndUpdateOptions) MongoSingleResult {
	return mc.Collection.FindOneAndUpdate(ctx, filter, update, opts...)
}

func (mc *mongoCollection) UpdateOne(ctx context.Context, filter interface{}, update interface{}, opts ...*options.UpdateOptions) (MongoUpdateResult, error) {
	u, e := mc.Collection.UpdateOne(ctx, filter, update, opts...)
	return &mongoUpdateResult{*u}, e
}

func (mc *mongoCollection) UpdateMany(ctx context.Context, filter interface{}, update interface{}, opts ...*options.UpdateOptions) (MongoUpdateResult, error) {
	u, e := mc.Collection.UpdateMany(ctx, filter, update, opts...)
	return &mongoUpdateResult{*u}, e
}

func (mc *mongoCollection) ReplaceOne(ctx context.Context, filter interface{}, replacement interface{}, opts ...*options.ReplaceOptions) (MongoUpdateResult, error) {
	u, e := mc.Collection.ReplaceOne(ctx, filter, replacement, opts...)
	return &mongoUpdateResult{*u}, e
}

type mongoUpdateResult struct {
	mongo.UpdateResult
}

func (mur *mongoUpdateResult) UnmarshalBSON(b []byte) error {
	return mur.UnmarshalBSON(b)
}

func (mur *mongoUpdateResult) GetUpsertedCount() int64 {
	return mur.UpsertedCount
}

func (mur *mongoUpdateResult) GetModifiedCount() int64 {
	return mur.ModifiedCount
}

type MongoSingleResult interface {
	Decode(v interface{}) error
	DecodeBytes() (bson.Raw, error)
	Err() error
}

type MongoUpdateResult interface {
	UnmarshalBSON(b []byte) error
	GetUpsertedCount() int64
	GetModifiedCount() int64
}

type MongoCursor interface {
	ID() int64
	Next(ctx context.Context) bool
	Decode(val interface{}) error
	Err() error
	Close(ctx context.Context) error
	All(ctx context.Context, results interface{}) error
}

type MongoChangeStream interface {
	ID() int64
	Decode(val interface{}) error
	Err() error
	Close(ctx context.Context) error
	ResumeToken() bson.Raw
	Next(ctx context.Context) bool
}
