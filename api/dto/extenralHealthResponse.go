package dto

type ExternalHealthResponse struct {
	Components []ExternalComponentHealth `json:"components" doc:"desc(components health)"`
}

type ExternalComponentHealth struct {
	Name   string `json:"name" doc:"desc(external component name)"`
	Status string `json:"status" doc:"desc(external component status message)"`
}
