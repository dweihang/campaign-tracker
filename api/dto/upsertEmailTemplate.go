package dto

type UpsertEmailTemplateRequest struct {
	Name      string                    `json:"name" doc:"required desc(email template name)"`
	Templates map[string]*EmailTemplate `json:"templates" doc:"required desc(template per language)"`
}

type EmailTemplate struct {
	Subject string `json:"subject" doc:"required desc(email template subject)"`
	Content string `json:"content" doc:"required desc(email template content)"`
}
