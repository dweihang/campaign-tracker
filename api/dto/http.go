package dto

import "gitlab.com/cake/goctx"

type HttpResponse struct {
	Code    int    `json:"code" doc:"required desc(0 for success, non 0 code for error)"`
	Message string `json:"message" doc:"required desc(feedback message)"`
	Cid     string `json:"cid" doc:"required desc(cid for tracing)"`
}

type HttpResponseWithResult struct {
	Code    int         `json:"code" doc:"required desc(0 for success, non 0 code for error)"`
	Message string      `json:"message" doc:"required desc(feedback message)"`
	Cid     string      `json:"cid" doc:"required desc(cid for tracing)"`
	Result  interface{} `json:"result,omitempty" doc:"desc()"`
}

func NewHttpResponse(ctx goctx.Context, code int, message string) *HttpResponse {
	cid, _ := ctx.GetCID()
	return &HttpResponse{Code: code, Message: message, Cid: cid}
}

func NewHttpResponseWithResult(ctx goctx.Context, code int, message string, result interface{}) *HttpResponseWithResult {
	cid, _ := ctx.GetCID()
	return &HttpResponseWithResult{Code: code, Message: message, Result: result, Cid: cid}
}
