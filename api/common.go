package api

import (
	"m800-campaign-tracker/api/dto"
	"m800-campaign-tracker/server"
)

var (
	DocCid = "1535436457112"
	DocErrorResponse = server.JSONResponseBody(dto.HttpResponse{
		Code:    4800001,
		Message: "something goes wrong",
		Cid:     DocCid,
	})
	DocSuccessResponse = server.JSONResponseBody(dto.HttpResponse{
		Code:    0,
		Message: "success",
		Cid:     DocCid,
	})
)
