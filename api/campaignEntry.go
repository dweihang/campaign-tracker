package api

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/cake/gopkg"
	"gitlab.com/cake/m800log"
	"m800-campaign-tracker/action"
	"m800-campaign-tracker/api/dto"
	"m800-campaign-tracker/cache"
	"m800-campaign-tracker/concurrent"
	"m800-campaign-tracker/config"
	"m800-campaign-tracker/errors"
	"m800-campaign-tracker/server"
	"m800-campaign-tracker/util"
	"net/http"
	"strings"
)

const (
	PathParamCampaignId = "campaignId"
)

func NewCampaignEntryAPI(actionResolver action.Resolver) CampaignEntryAPI {
	return &campaignEntryAPI{
		actionResolver: actionResolver,
	}
}

type CampaignEntryAPI interface {
	server.API
	MiddleWare() gin.HandlerFunc
}

type campaignEntryAPI struct {
	actionResolver action.Resolver
}

func (c *campaignEntryAPI) MiddleWare() gin.HandlerFunc {
	return func(context *gin.Context) {
		if strings.Count(context.Request.URL.Path, "/") < 2 && context.Request.Method == http.MethodGet && config.Campaign.ExcludePathMap[context.Request.URL.Path] == nil {
			c.EntryPoint(context)
		}
	}
}

func (c *campaignEntryAPI) RouteGroup() *server.RouteGroup {
	docTag := "campaign entrypoint"
	rg := server.NewRouteGroup("/")
	rg.
		Get("/:%s", PathParamCampaignId).
		To(c.EntryPoint).
		Doc().
		Tag(docTag).
		Summary("campaign entry point url, will generate tracking id (e.g scan QR code)").
		Path(server.Path(PathParamCampaignId).Schema("m1").Description("campaign id")).
		Response(server.Response302, server.EmptyResponseBody().Header("Location", "https://onlinesignup.maaiiconnect.com?tid=scanQRCode.k75OHVtMJx&campaignId=m1", "redirect url"), "redirect").
		Response(server.Response404, server.JSONResponseBody(dto.HttpResponse{
			Cid:     DocCid,
			Message: "campaign not found",
			Code:    errors.CampaignNotFound,
		}), "campaign not found")
	return rg
}

func (c *campaignEntryAPI) EntryPoint(ginCtx *gin.Context) {
	ctx := util.GetContextFromGin(ginCtx)
	internalObj := util.GetInternalObjectFromGin(ginCtx)
	internalObj[util.KeyCampaignId] = ginCtx.Request.URL.Path[1:]
	responseChan := make(chan struct{}, 1)
	defer close(responseChan)
	m800log.Infof(ctx, "start process entry point event, internal object = %+v", internalObj)
	if model := cache.CampaignCache[internalObj.GetCampaignId()]; model == nil {
		ginCtx.AbortWithStatusJSON(http.StatusBadRequest, dto.NewHttpResponse(ctx, errors.CampaignNotFound, "campaign not found"))
	} else {
		internalObj[util.KeyEvent] = model.EntryTrackerName
		internalObj[util.KeyTrackingId] = model.CampaignID + "." + model.EntryTrackerName + "." + util.RandString(config.Campaign.TrackingIdLength)
		if actions, execution, err := c.actionResolver.Resolve(model, model.EntryTrackerName, ginCtx, responseChan); err != nil {
			ginCtx.AbortWithStatusJSON(http.StatusInternalServerError, dto.NewHttpResponse(ctx, err.ErrorCode(), err.ErrorMsg()))
		} else {
			for _, act := range actions {
				if err := act.Validate(ginCtx); err != nil {
					ginCtx.AbortWithStatusJSON(http.StatusInternalServerError, dto.NewHttpResponse(ctx, err.ErrorCode(), err.ErrorMsg()))
					return
				}
			}

			execution.Async(ctx, concurrent.NewAsyncExecutor(), func(iterator *concurrent.ExecutionResultIterator, err gopkg.CodeError) {
				if err != nil {
					m800log.Errorf(ctx, "process event failed event=%s error=%+v", internalObj.GetEvent(), err)
				}
				if !ginCtx.IsAborted() {
					if err != nil {
						ginCtx.AbortWithStatusJSON(http.StatusInternalServerError, dto.NewHttpResponse(ctx, err.ErrorCode(), err.ErrorMsg()))
					} else {
						ginCtx.AbortWithStatusJSON(http.StatusOK, dto.NewHttpResponse(ctx, 0, "success"))
					}
					responseChan <- struct{}{}
				}
			})
			<-responseChan

		}
	}
}
