package api

import (
	"github.com/gin-gonic/gin"
	"m800-campaign-tracker/api/dto"
	"m800-campaign-tracker/cache"
	"m800-campaign-tracker/server"
	"m800-campaign-tracker/util"
	"net/http"
)

type cacheAPI struct {
}

func NewCacheAPI() *cacheAPI {
	return &cacheAPI{}
}

func (c cacheAPI) RouteGroup() *server.RouteGroup {
	docTag := "cache"
	rg := server.NewRouteGroup("/v1/cache")
	rg.
		Put("/reload").
		To(c.RealoadCache).
		Doc().
		Tag(docTag).
		Summary("cache operation").
		Response(server.Response200, DocSuccessResponse, "success")
	return rg
}

func (c cacheAPI) RealoadCache(context *gin.Context) {
	ctx := util.GetContextFromGin(context)
	cache.ReloadInMemoryCache()
	context.AbortWithStatusJSON(http.StatusOK, dto.NewHttpResponse(ctx, 0, "success"))
}
