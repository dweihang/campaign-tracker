package api

import (
	"github.com/gin-gonic/gin"
	"m800-campaign-tracker/api/dto"
	"m800-campaign-tracker/email"
	"m800-campaign-tracker/errors"
	"m800-campaign-tracker/server"
	"m800-campaign-tracker/util"
	"net/http"
)

type healthAPI struct {
	emailCli email.Client
}

func NewHealthAPI(emailCli email.Client) server.API {
	return &healthAPI{
		emailCli: emailCli,
	}
}

func (h *healthAPI) RouteGroup() *server.RouteGroup {
	docTag := "health check"
	rg := server.NewRouteGroup("/health")
	rg.
		Get("/").
		To(h.Health).
		Doc().
		Tag(docTag).
		Summary("check application whether running").
		Response(server.Response200, server.JSONResponseBody(&dto.HttpResponse{
			Code:    0,
			Message: "i am running",
			Cid: DocCid,
		}), "success")

	rg.
		Get("/external").
		To(h.ExternalHealth).
		Doc().
		Tag(docTag).
		Summary("check external components health").
		Response(server.Response200, server.JSONResponseBody(&dto.HttpResponseWithResult{
			Code:    0,
			Message: "success",
			Result: dto.ExternalHealthResponse{
				Components: []dto.ExternalComponentHealth{
					{
						Name:   "email",
						Status: "ok",
					},
				},
			},
			Cid: DocCid,
		}), "success").
		Response(server.ResponseDefault, server.JSONResponseBody(&dto.HttpResponseWithResult{
			Code:    errors.EmailSMTPServerNotAvailable,
			Message: "cannot connect to smtp server",
			Cid:     "tracingId",
			Result: dto.ExternalHealthResponse{
				Components: []dto.ExternalComponentHealth{
					{
						Name:   "email",
						Status: "cannot connect to smtp server",
					},
				},
			},
		}), "1535436457112")
	return rg
}

func (h *healthAPI) Health(ctx *gin.Context) {
	c := util.GetContextFromGin(ctx)
	ctx.AbortWithStatusJSON(http.StatusOK, dto.NewHttpResponse(c, 0, "i am running"))
}

func (h *healthAPI) ExternalHealth(ctx *gin.Context) {
	c := util.GetContextFromGin(ctx)
	code := 0
	msg := "success"
	emailStatus := "ok"
	httpStatus := http.StatusOK
	if err := h.emailCli.Ping(); err != nil {
		code = err.ErrorCode()
		msg = err.ErrorMsg()
		emailStatus = err.Error()
		httpStatus = http.StatusInternalServerError
	}
	ctx.AbortWithStatusJSON(httpStatus, dto.NewHttpResponseWithResult(c, code, msg, dto.ExternalHealthResponse{
		Components: []dto.ExternalComponentHealth{
			{
				Name:   "email",
				Status: emailStatus,
			},
		},
	}))
}
