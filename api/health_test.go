package api

import (
	"github.com/gin-gonic/gin"
	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cake/gopkg"
	"m800-campaign-tracker/email"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestHealthAPI_RouteGroup(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	underTest := NewHealthAPI(email.NewMockEmailClient(ctrl))
	underTest.RouteGroup()
}

func TestHealthAPI_Health(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	underTest := &healthAPI{
		emailCli: email.NewMockEmailClient(ctrl),
	}
	r := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(r)
	c.Request = &http.Request{}
	underTest.Health(c)
	c.Request = httptest.NewRequest(http.MethodGet, "/health", nil)
	assert.Equal(t, http.StatusOK, r.Code)
}

func TestHealthAPI_ExternalHealth(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	mockEmail := email.NewMockEmailClient(ctrl)
	mockEmail.EXPECT().Ping().Return(nil)
	underTest := &healthAPI{
		emailCli: mockEmail,
	}
	r := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(r)
	c.Request = &http.Request{}
	underTest.ExternalHealth(c)
	c.Request = httptest.NewRequest(http.MethodGet, "/health/external", nil)
	assert.Equal(t, http.StatusOK, r.Code)
}

func TestHealthAPI_ExternalHealthError(t *testing.T) {
	ctrl := gomock.NewController(t)
	defer ctrl.Finish()
	mockEmail := email.NewMockEmailClient(ctrl)
	mockEmail.EXPECT().Ping().Return(gopkg.NewCodeError(100, "error"))
	underTest := &healthAPI{
		emailCli: mockEmail,
	}
	r := httptest.NewRecorder()
	c, _ := gin.CreateTestContext(r)
	c.Request = &http.Request{}
	underTest.ExternalHealth(c)
	c.Request = httptest.NewRequest(http.MethodGet, "/health/external", nil)
	assert.Equal(t, http.StatusInternalServerError, r.Code)
}
