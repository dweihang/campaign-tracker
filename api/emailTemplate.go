package api

import (
	"encoding/base64"
	"github.com/gin-gonic/gin"
	"gitlab.com/cake/m800log"
	"m800-campaign-tracker/api/dto"
	"m800-campaign-tracker/dbaccess/local/dbop"
	"m800-campaign-tracker/dbaccess/local/model"
	"m800-campaign-tracker/errors"
	"m800-campaign-tracker/server"
	"m800-campaign-tracker/util"
	"net/http"
	"time"
)

func NewEmailTemplateAPI(emailTemplateOp dbop.EmailTemplate) server.API {
	return &emailTemplateAPI{
		emailTemplateOp: emailTemplateOp,
	}
}

type emailTemplateAPI struct {
	emailTemplateOp dbop.EmailTemplate
}

func (e *emailTemplateAPI) RouteGroup() *server.RouteGroup {
	docTag := "email template"
	rg := server.NewRouteGroup("/v1/email-template")
	rg.
		Put("/").
		To(e.UpsertTemplate).
		Doc().
		Tag(docTag).
		Summary("create/update email template").
		RequestBody(server.JSONRequestBody(dto.UpsertEmailTemplateRequest{
			Name: "test-campaign-1",
			Templates: map[string]*dto.EmailTemplate{
				"en": {
					Subject: "test subject",
					Content: "test content {{.Greeting}}",
				},
			},
		})).
		Response(server.Response200, DocSuccessResponse, "success").
		Response(server.ResponseDefault, DocErrorResponse, "error")
	return rg
}

func (e *emailTemplateAPI) UpsertTemplate(ctx *gin.Context) {
	c := util.GetContextFromGin(ctx)
	req := &dto.UpsertEmailTemplateRequest{}
	if err := ctx.BindJSON(req); err != nil {
		m800log.Error(c, "failed to bind UpsertEmailTemplateRequest")
		ctx.AbortWithStatusJSON(http.StatusBadRequest, dto.NewHttpResponse(c, errors.GinBindJSONError, "failed to bind UpsertEmailTemplateRequest"))
	} else {
		etls := make(map[string]*model.EmailTemplateLanguageContent)
		for k, t := range req.Templates {
			content, err := base64.StdEncoding.DecodeString(t.Content)
			if err != nil {
				ctx.AbortWithStatusJSON(http.StatusBadRequest, dto.NewHttpResponse(c, errors.EmailCannotParseTemplate, "failed to decode base64 email content"))
				return
			}
			etls[k] = &model.EmailTemplateLanguageContent{
				Subject: t.Subject,
				Content: string(content),
			}
		}
		if err := e.emailTemplateOp.ReplaceTemplate(c, &model.EmailTemplate{
			Name:      req.Name,
			Templates: etls,
			UpdatedAt: time.Now().UnixNano() / 1e6,
		}); err != nil {
			m800log.Errorf(c, "failed to replace email template, request=%+v", *req)
			ctx.AbortWithStatusJSON(http.StatusBadRequest, dto.NewHttpResponse(c, errors.GinBindJSONError, "failed to replace email template"))
		} else {
			ctx.AbortWithStatusJSON(http.StatusOK, dto.NewHttpResponse(c, 0, "success"))
		}
	}
}
