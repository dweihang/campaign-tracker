package api

import (
	"bytes"
	"github.com/gin-gonic/gin"
	"gitlab.com/cake/gopkg"
	"gitlab.com/cake/m800log"
	"m800-campaign-tracker/api/dto"
	"m800-campaign-tracker/cache"
	"m800-campaign-tracker/dbaccess/local/dbop"
	"m800-campaign-tracker/dbaccess/local/model"
	"m800-campaign-tracker/email"
	"m800-campaign-tracker/errors"
	"m800-campaign-tracker/server"
	"m800-campaign-tracker/util"
	"net/http"
	"text/template"
)

func NewCronAPI(emailReminderDbOp dbop.EmailReminder, emailTemplateDbOp dbop.EmailTemplate, emailCli email.Client) server.API {
	return &cronAPI{
		emailReminderDbOp: emailReminderDbOp,
		emailTemplateDbOp: emailTemplateDbOp,
		emailCli:          emailCli,
	}
}

type cronAPI struct {
	emailReminderDbOp dbop.EmailReminder
	emailTemplateDbOp dbop.EmailTemplate
	emailCli          email.Client
}

func (c *cronAPI) RouteGroup() *server.RouteGroup {
	docTag := "campaign cron reminder"
	rg := server.NewRouteGroup("/v1/cron")
	rg.
		Get("/").
		To(c.PerformAction).
		Doc().
		Tag(docTag).
		Query(server.Query("interval").Schema("5/10/15/30/60").Required(true)).
		Summary("trigger email reminder checks").
		Response(server.Response200, DocSuccessResponse, "success").
		Response(server.ResponseDefault, DocErrorResponse, "error")

	return rg
}

func (c *cronAPI) PerformAction(ginCtx *gin.Context) {
	ctx := util.GetContextFromGin(ginCtx)

	interval := ginCtx.Query("interval")
	emailReminders, err := c.emailReminderDbOp.GetRemindableEmails(ctx, interval)
	if err != nil {
		ginCtx.AbortWithStatusJSON(http.StatusInternalServerError, dto.NewHttpResponse(ctx, err.ErrorCode(), err.ErrorMsg()))
	}

	for _, emailReminder := range emailReminders {

		//Get and send email
		emailTemplate := cache.DbTemplateCache[emailReminder.TemplateName]
		if emailTemplate == nil {
			emailTemplate, err = c.emailTemplateDbOp.GetTemplateByName(ctx, emailReminder.TemplateName)
			if err != nil {
				m800log.Errorf(ctx, "failed to get template with error=%+v", err)
				ginCtx.AbortWithStatusJSON(http.StatusInternalServerError, dto.NewHttpResponse(ctx, err.ErrorCode(), err.ErrorMsg()))
			}
		}

		if emailTemplate != nil {
			cache.DbTemplateCache[emailTemplate.Name] = emailTemplate

			if languageTemplate := emailTemplate.Templates[emailReminder.Language]; languageTemplate == nil {
				if err != nil {
					err = gopkg.NewWrappedCarrierCodeError(errors.EmailMissingLanguageTemplate, "missing email template for language "+emailReminder.Language, err)
				} else {
					err = gopkg.NewCodeError(errors.EmailMissingLanguageTemplate, "missing email template for language "+emailReminder.Language)
				}
				if err != nil {
					ginCtx.AbortWithStatusJSON(http.StatusInternalServerError, dto.NewHttpResponse(ctx, err.ErrorCode(), err.ErrorMsg()))
				}
			} else {
				var tplErr error
				emTpl := cache.EmailTemplateCache[emailReminder.TemplateName+":"+emailReminder.Language]
				if emTpl == nil {
					emTpl, tplErr = template.New(emailReminder.TemplateName).Parse(languageTemplate.Content)
					if tplErr != nil {
						if err != nil {
							err = gopkg.NewWrappedCodeError(errors.EmailCannotParseTemplate, "cannot parse email template", err)
						} else {
							err = gopkg.NewCodeError(errors.EmailCannotParseTemplate, "cannot parse email template")
						}
						if err != nil {
							ginCtx.AbortWithStatusJSON(http.StatusInternalServerError, dto.NewHttpResponse(ctx, err.ErrorCode(), err.ErrorMsg()))
						}
					}
					cache.EmailTemplateCache[emailReminder.TemplateName+":"+emailReminder.Language] = emTpl
				}

				if emTpl != nil {
					buffer := new(bytes.Buffer)
					if executeTemplateErr := emTpl.Execute(buffer, emailReminder.TemplateParams); executeTemplateErr != nil {
						if err != nil {
							err = gopkg.NewWrappedCodeError(errors.EmailCannotParseTemplate, "cannot fillin the email template parameters", err)
						} else {
							err = gopkg.NewWrappedCodeError(errors.EmailCannotParseTemplate, "cannot fillin the email template parameters", executeTemplateErr)
						}
					} else {
						m := &email.Mail{
							To:          emailReminder.Recipients,
							Bcc:         emailReminder.BccRecipients,
							Sender:      emailReminder.Sender,
							Subject:     languageTemplate.Subject,
							Message:     string(buffer.Bytes()),
							ContentType: emailReminder.ContentType,
						}
						sentErr := c.emailCli.Send(ctx, m)
						if sentErr != nil {
							if err != nil {
								err = gopkg.NewWrappedCodeError(sentErr.ErrorCode(), sentErr.ErrorMsg(), err)
							} else {
								err = sentErr
							}
						} else {
							m800log.Infof(ctx, "successfully send email %+v", *m)
						}
					}
					if err != nil {
						ginCtx.AbortWithStatusJSON(http.StatusInternalServerError, dto.NewHttpResponse(ctx, err.ErrorCode(), err.ErrorMsg()))
					}
				}
			}
		}

		//Update remaining tries
		if emailReminder.Tries-1 > 0 {
			er := &model.EmailReminder{
				Id:       emailReminder.Id,
				RemindAt: emailReminder.RemindAt + emailReminder.RemindInterval,
				Tries:    emailReminder.Tries - 1,
			}
			err = c.emailReminderDbOp.UpdateReminder(ctx, er)
			if err != nil {
				m800log.Errorf(ctx, "Unable to update email reminder %+v", er)
			}
		} else {
			err = c.emailReminderDbOp.Delete(ctx, emailReminder.Id)
			if err != nil {
				m800log.Errorf(ctx, "Unable to delete email reminder of id %s", emailReminder.Id)
			}
		}
		if err != nil {
			ginCtx.AbortWithStatusJSON(http.StatusInternalServerError, dto.NewHttpResponse(ctx, err.ErrorCode(), err.ErrorMsg()))
		}
	}
}
