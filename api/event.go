package api

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/cake/gopkg"
	"gitlab.com/cake/m800log"
	"m800-campaign-tracker/action"
	"m800-campaign-tracker/api/dto"
	"m800-campaign-tracker/cache"
	"m800-campaign-tracker/concurrent"
	"m800-campaign-tracker/dbaccess/local/dbop"
	"m800-campaign-tracker/errors"
	"m800-campaign-tracker/server"
	"m800-campaign-tracker/util"
	"net/http"
)

func NewCampaignEventAPI(emailReminderDbOp dbop.EmailReminder, actionResolver action.Resolver) server.API {
	return &campaignEventAPI{
		emailReminderDbOp: emailReminderDbOp,
		actionResolver:    actionResolver,
	}
}

type campaignEventAPI struct {
	emailReminderDbOp dbop.EmailReminder
	actionResolver    action.Resolver
}

func (c *campaignEventAPI) RouteGroup() *server.RouteGroup {
	docTag := "campaign events tracking"
	rg := server.NewRouteGroup("/v1/campaign-events")
	rg.
		Get("/").
		To(c.PerformAction).
		Doc().
		Tag(docTag).
		Query(server.Query("event").Schema("signupBtnClicked").Required(true)).
		Query(server.Query("campaignId").Schema("mc1").Required(true)).
		Query(server.Query("tid").Schema("scanQRCode.k75OHVtMJx").Required(false)).
		Summary("trigger campaign event corresponding action which predefined by the campaign").
		Response(server.Response200, DocSuccessResponse, "success").
		Response(server.Response302, server.EmptyResponseBody().Header("Location", "https://onlinesignup.maaiiconnect.com?tid=scanQRCode.k75OHVtMJx&campaignId=m1", "redirect url"), "redirect").
		Response(server.ResponseDefault, DocErrorResponse, "error")

	rg.
		Post("/").
		To(c.PerformAction).
		Doc().
		Tag(docTag).
		Query(server.Query("event").Schema("registerEmail").Required(true)).
		Query(server.Query("campaignId").Schema("mc1").Required(true)).
		Query(server.Query("tid").Schema("scanQRCode.k75OHVtMJx").Required(false)).
		Query(server.Query("email").Schema("email").Required(false)).
		Summary("trigger campaign event corresponding action which predefined by the campaign").
		Response(server.Response200, DocSuccessResponse, "success").
		Response(server.Response500, DocErrorResponse, "error")
	return rg
}

func (c *campaignEventAPI) PerformAction(ginCtx *gin.Context) {
	ctx := util.GetContextFromGin(ginCtx)
	iobj := util.GetInternalObjectFromGin(ginCtx)
	m800log.Infof(ctx, "start process event, internal object=%+v", iobj)
	responseChan := make(chan struct{}, 1)
	defer close(responseChan)
	if len(iobj.GetCampaignId()) == 0 {
		ginCtx.AbortWithStatusJSON(http.StatusBadRequest, dto.NewHttpResponse(ctx, errors.CampaignNotFound, "missing campaign id"))
	} else if cache.CampaignCache[iobj.GetCampaignId()] == nil {
		ginCtx.AbortWithStatusJSON(http.StatusBadRequest, dto.NewHttpResponse(ctx, errors.CampaignNotFound, "campaign not found"))
	} else {
		_ = c.emailReminderDbOp.DeleteByEvent(ctx, iobj.GetEvent(), iobj.GetTrackingId())
		actions, execution,  err := c.actionResolver.Resolve(cache.CampaignCache[iobj.GetCampaignId()], iobj.GetEvent(), ginCtx, responseChan)
		if err != nil {
			ginCtx.AbortWithStatusJSON(http.StatusInternalServerError, dto.NewHttpResponse(ctx, err.ErrorCode(), err.ErrorMsg()))
		} else {
			for _, act := range actions {
				if err := act.Validate(ginCtx); err != nil {
					ginCtx.AbortWithStatusJSON(http.StatusInternalServerError, dto.NewHttpResponse(ctx, err.ErrorCode(), err.ErrorMsg()))
					return
				}
			}

			execution.Async(ctx, concurrent.NewAsyncExecutor(), func(iterator *concurrent.ExecutionResultIterator, err gopkg.CodeError) {
				if err != nil {
					m800log.Errorf(ctx, "process event failed event=%s error=%+v", iobj.GetEvent(), err)
				}
				if !ginCtx.IsAborted() {
					if err != nil {
						ginCtx.AbortWithStatusJSON(http.StatusInternalServerError, dto.NewHttpResponse(ctx, err.ErrorCode(), err.ErrorMsg()))
					} else {
						ginCtx.AbortWithStatusJSON(http.StatusOK, dto.NewHttpResponse(ctx, 0, "success"))
					}
					responseChan <- struct{}{}
				}
			})
			<-responseChan
		}
	}
}
