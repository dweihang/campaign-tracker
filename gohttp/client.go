package gohttp

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"m800-campaign-tracker/errors"
	"net/http"
	"net/http/httputil"
	"strings"
	"time"

	"m800-campaign-tracker/trace"

	"gitlab.com/cake/goctx"
	"gitlab.com/cake/gopkg"
)

// A reusable stateful http client
//
// The methods that allow for an interceptor lambda
// are intended to allow the user to modify the request
// before it is executed, since the interceptor callback
// is executed on the http request before it is run
//
// One example of a use case of the interceptor is to
// add headers or specific parameters to the request
// that are specific to that request (not global for all requests)
//
// For headers that should be applied to all requests for every request
// specify those headers in the Config
//
// A function to get a helper interceptor to inject
// headers into the request is provided below
type Client interface {
	Get(ctx goctx.Context, url string) (*http.Response, gopkg.CodeError)
	GetWithInterceptor(ctx goctx.Context, url string, interceptor func(*http.Request)) (*http.Response, gopkg.CodeError)

	Post(ctx goctx.Context, url string, content []byte) (*http.Response, gopkg.CodeError)
	PostWithInterceptor(ctx goctx.Context, url string, content []byte, interceptor func(*http.Request)) (*http.Response, gopkg.CodeError)

	Put(ctx goctx.Context, url string, content []byte) (*http.Response, gopkg.CodeError)
	PutWithInterceptor(ctx goctx.Context, url string, content []byte, interceptor func(*http.Request)) (*http.Response, gopkg.CodeError)

	Patch(ctx goctx.Context, url string, content []byte) (*http.Response, gopkg.CodeError)
	PatchWithInterceptor(ctx goctx.Context, url string, content []byte, interceptor func(*http.Request)) (*http.Response, gopkg.CodeError)

	Delete(ctx goctx.Context, url string) (*http.Response, gopkg.CodeError)
	DeleteWithInterceptor(ctx goctx.Context, url string, interceptor func(*http.Request)) (*http.Response, gopkg.CodeError)
}

type client struct {
	config Config
	tracer trace.HTTPTracer
}

func newErrorWithStacktrace(errorCode int, message string, err error) gopkg.CodeError {
	return gopkg.NewWrappedCodeError(errorCode, message, err)
}

func NewClient(config Config) Client {
	return client{config: config, tracer: trace.DefaultHTTPTracer}
}

type Config struct {
	HttpClient http.Client
	// These headers are applied for every request
	// To insert unique headers for just a particular request
	// use the method that supports an interceptor
	Headers map[string]string
}

// Helper method to get an interceptor that injects
// the supplied headers into the request
func InsertHeadersInterceptor(headers ...map[string]string) func(*http.Request) {
	return func(req *http.Request) {
		for _, header := range headers {
			for k, v := range header {
				req.Header.Add(k, v)
			}
		}
	}
}

// Helper methods to parse the http response body
func GetResponseBodyAsMap(ctx goctx.Context, response *http.Response) (*map[string]interface{}, gopkg.CodeError) {
	mapResponse := make(map[string]interface{})
	err := GetResponseBodyAsType(ctx, response, &mapResponse)
	if err != nil {
		return nil, err
	}
	return &mapResponse, nil

}

func GetResponseBodyAsString(ctx goctx.Context, response *http.Response) (*string, gopkg.CodeError) {
	body, err := getByteArrayFromHttpResponse(response)
	if err != nil {
		return nil, err
	}
	parsed := string(body)
	return &parsed, nil
}

func getByteArrayFromHttpResponse(response *http.Response) ([]byte, gopkg.CodeError) {
	if response == nil {
		return nil, gopkg.NewCodeError(errors.GoHTTPResponseIsNil, "Response is nil")
	}
	if response.Body == nil {
		return nil, gopkg.NewCodeError(errors.GoHTTPResponseBodyIsNil, "Response body is nil")
	}
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return nil, newErrorWithStacktrace(errors.GoHTTPResponseBodyReadingError, "Failed to read body", err)
	}
	return body, nil
}

// GetResponseBodyAsType - Generic parsing method to any type
func GetResponseBodyAsType(ctx goctx.Context, response *http.Response, result interface{}) gopkg.CodeError {
	body, err := getByteArrayFromHttpResponse(response)
	if err != nil {
		return err
	}
	if len(body) == 0 {
		return newErrorWithStacktrace(errors.GoHTTPResponseBodyUnmarshallingError, "Response body is empty", err)
	}
	if err := json.Unmarshal(body, &result); err != nil {
		return newErrorWithStacktrace(errors.GoHTTPResponseBodyUnmarshallingError, "Failed to unmarshal body", err)
	}
	return nil
}

func NewConfig(httpClient http.Client, headers map[string]string) Config {
	return Config{HttpClient: httpClient, Headers: headers}
}

func (c client) Get(ctx goctx.Context, url string) (*http.Response, gopkg.CodeError) {
	return c.get(ctx, url, nil)
}

func (c client) GetWithInterceptor(ctx goctx.Context, url string, interceptor func(*http.Request)) (*http.Response, gopkg.CodeError) {
	return c.get(ctx, url, &interceptor)
}

func (c client) get(ctx goctx.Context, url string, interceptor *func(*http.Request)) (*http.Response, gopkg.CodeError) {
	return c.performRequest(ctx, http.MethodGet, url, nil, interceptor)
}

func (c client) Put(ctx goctx.Context, url string, content []byte) (*http.Response, gopkg.CodeError) {
	return c.put(ctx, url, content, nil)
}

func (c client) PutWithInterceptor(ctx goctx.Context, url string, content []byte, interceptor func(*http.Request)) (*http.Response, gopkg.CodeError) {
	return c.put(ctx, url, content, &interceptor)
}

func (c client) put(ctx goctx.Context, url string, content []byte, interceptor *func(*http.Request)) (*http.Response, gopkg.CodeError) {
	return c.performRequest(ctx, http.MethodPut, url, &content, interceptor)
}

func (c client) Post(ctx goctx.Context, url string, content []byte) (*http.Response, gopkg.CodeError) {
	return c.post(ctx, url, content, nil)
}

func (c client) PostWithInterceptor(ctx goctx.Context, url string, content []byte, interceptor func(*http.Request)) (*http.Response, gopkg.CodeError) {
	return c.post(ctx, url, content, &interceptor)
}

func (c client) post(ctx goctx.Context, url string, content []byte, interceptor *func(*http.Request)) (*http.Response, gopkg.CodeError) {
	return c.performRequest(ctx, http.MethodPost, url, &content, interceptor)
}

func (c client) Patch(ctx goctx.Context, url string, content []byte) (*http.Response, gopkg.CodeError) {
	return c.patch(ctx, url, content, nil)
}

func (c client) PatchWithInterceptor(ctx goctx.Context, url string, content []byte, interceptor func(*http.Request)) (*http.Response, gopkg.CodeError) {
	return c.patch(ctx, url, content, &interceptor)
}

func (c client) patch(ctx goctx.Context, url string, content []byte, interceptor *func(*http.Request)) (*http.Response, gopkg.CodeError) {
	return c.performRequest(ctx, http.MethodPatch, url, &content, interceptor)
}

func (c client) Delete(ctx goctx.Context, url string) (*http.Response, gopkg.CodeError) {
	return c.delete(ctx, url, nil)
}

func (c client) DeleteWithInterceptor(ctx goctx.Context, url string, interceptor func(*http.Request)) (*http.Response, gopkg.CodeError) {
	return c.delete(ctx, url, &interceptor)
}

func (c client) delete(ctx goctx.Context, url string, interceptor *func(*http.Request)) (*http.Response, gopkg.CodeError) {
	return c.performRequest(ctx, http.MethodDelete, url, nil, interceptor)
}

func (c client) performRequest(ctx goctx.Context, requestType string,
	url string,
	content *[]byte,
	interceptor *func(*http.Request)) (*http.Response, gopkg.CodeError) {
	req, err := c.createRequest(ctx, requestType, url, content)
	if err != nil {
		return nil, err
	}
	if interceptor != nil {
		(*interceptor)(req)
	}
	return c.executeRequest(ctx, req)
}

func (c client) executeRequest(ctx goctx.Context, request *http.Request) (*http.Response, gopkg.CodeError) {
	var traceInfo *trace.HTTPTraceInfo
	traceKey, _ := ctx.GetCID()
	if len(traceKey) > 0 {
		traceInfo = c.tracer.GetTrace(traceKey)
	}
	var reqOut, resOut []byte
	if traceInfo != nil {
		reqOut, _ = httputil.DumpRequestOut(request, true)
	}

	timeStart := time.Now()
	response, err := c.config.HttpClient.Do(request)
	if traceInfo != nil {
		timeElapsedNano := time.Since(timeStart).Nanoseconds()
		if response != nil {
			resOut, _ = httputil.DumpResponse(response, true)
		}
		c.tracer.AppendOutboundHTTPData(traceInfo, reqOut, resOut, timeElapsedNano)
	}
	err = c.handleRequestErrorChecking(ctx, err, response)
	if err != nil {
		return response, newErrorWithStacktrace(errors.GoHTTPRequestError, "Request execution failure", err)
	}
	return response, nil
}

func (c client) createRequest(ctx goctx.Context, requestType string, url string, content *[]byte) (*http.Request, gopkg.CodeError) {
	var req *http.Request
	var err error
	requestType = strings.ToUpper(requestType)
	if content != nil {
		req, err = http.NewRequest(requestType, url, bytes.NewBuffer(*content))
	} else {
		req, err = http.NewRequest(requestType, url, nil)
	}
	if err != nil {
		return nil, newErrorWithStacktrace(errors.GoHTTPRequestBodyCreationError,
			fmt.Sprintf("Failed to create request body "+
				"(requestType = %s, url = %s, content = %v)", requestType, url, content), err)
	}
	req.Close = true
	c.assignHeaders(ctx, req)
	return req, nil
}

func (c client) assignHeaders(ctx goctx.Context, r *http.Request) *http.Request {
	if c.config.Headers == nil {
		return r
	}
	for k, v := range c.config.Headers {
		r.Header.Add(k, v)
	}
	return r
}

func (c client) handleRequestErrorChecking(ctx goctx.Context, requestError error, response *http.Response) gopkg.CodeError {
	if requestError != nil {
		return newErrorWithStacktrace(errors.GoHTTPRequestError, "HTTP request failed", requestError)
	} else if response != nil {
		responseError := c.getResponseError(ctx, response)
		if responseError != nil {
			return responseError
		}
	}
	return nil
}

func (c client) getResponseError(ctx goctx.Context, response *http.Response) gopkg.CodeError {
	if response == nil {
		return nil
	}
	if response.StatusCode >= http.StatusOK && response.StatusCode < http.StatusMultipleChoices {
		return nil
	}
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		return newErrorWithStacktrace(errors.GoHTTPRequestError, fmt.Sprintf("%s Error: code = %d, status = %s, body = <failed to read> (%v)",
			response.Proto, response.StatusCode, response.Status, err), err)
	}
	return newErrorWithStacktrace(errors.GoHTTPRequestError, fmt.Sprintf("%s Error: code = %d, status = %s, body = %s",
		response.Proto, response.StatusCode, response.Status, string(body)), err)
}
