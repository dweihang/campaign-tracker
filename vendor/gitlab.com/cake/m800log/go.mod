module gitlab.com/cake/m800log

require (
	github.com/sirupsen/logrus v1.6.0
	github.com/stretchr/testify v1.5.1
	gitlab.com/cake/goctx v1.6.2
)

go 1.13
