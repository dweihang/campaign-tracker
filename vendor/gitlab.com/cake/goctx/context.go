package goctx

import (
	"context"
	"fmt"
	"net/http"
	"strconv"
	"strings"
	"sync"
	"time"

	"github.com/opentracing/opentracing-go"
)

var nullStruct = struct{}{}

// follow the suggestion from "context" package to avoid collision
type mapContextKey string

// Context defines interface of common usage, combing log, value, go context methods.
type Context interface {
	Set(key string, value interface{})
	Get(key string) interface{}
	GetCID() (string, error)
	GetString(key string) (value string, ok bool)
	GetStringSlice(key string) (value []string, ok bool)
	GetInt(key string) (value int, ok bool)
	GetInt64(key string) (value int64, ok bool)

	Map() (ret map[string]interface{})
	MapString() (ret map[string]string)
	HeaderKeyMap() (ret map[string]string)

	// tracing method
	GetSpan() opentracing.Span
	StartSpanFromContext(operationName string, opts ...opentracing.StartSpanOption) opentracing.Span
	SetSpan(span opentracing.Span)

	// header key transformation
	SetShortenKey(key string, value interface{})
	InjectHTTPHeader(s http.Header)

	// context related methods
	SetTimeout(duration time.Duration) (cancel context.CancelFunc)
	SetDeadline(d time.Time) (cancel context.CancelFunc)
	Cancel() (cancel context.CancelFunc)

	// create child goctx
	WithCancel() (ctx *MapContext, cancel context.CancelFunc)
	WithDeadline(d time.Time) (ctx *MapContext, cancel context.CancelFunc)

	// golang context() interface
	Done() <-chan struct{}
	Err() error
	Value(key interface{}) interface{}
	Deadline() (deadline time.Time, ok bool)
}

// MapContext implements the map concept and mixins the golang context
type MapContext struct {
	mu sync.RWMutex // protects following fields
	context.Context

	keys map[string]struct{} // for record keys
}

func Background() Context {
	return &MapContext{Context: context.Background()}
}
func TODO() Context {
	return &MapContext{Context: context.TODO()}
}

func (c *MapContext) Set(key string, value interface{}) {
	c.mu.Lock()
	defer c.mu.Unlock()
	c.Context = context.WithValue(c.Context, mapContextKey(key), value)
	if c.keys == nil {
		c.keys = make(map[string]struct{})
	}
	c.keys[key] = nullStruct
}

func (c *MapContext) Get(key string) interface{} {
	return c.Value(mapContextKey(key))
}

func (c *MapContext) Done() <-chan struct{} {
	c.mu.RLock()
	defer c.mu.RUnlock()
	return c.Context.Done()
}

func (c *MapContext) GetString(key string) (value string, ok bool) {
	v := c.Get(key)
	if v == nil {
		return
	}
	value, ok = v.(string)
	return
}

func (c *MapContext) GetStringSlice(key string) (value []string, ok bool) {
	v := c.Get(key)
	if v == nil {
		return
	}
	value, ok = v.([]string)
	return
}

func (c *MapContext) GetInt(key string) (value int, ok bool) {
	v := c.Get(key)
	if v == nil {
		return
	}
	value, ok = v.(int)
	return
}

func (c *MapContext) GetInt64(key string) (value int64, ok bool) {
	v := c.Get(key)
	if v == nil {
		return
	}
	value, ok = v.(int64)
	return
}

// Map needs directly call c.Context.Value prevent duplicate lock
func (c *MapContext) Map() (ret map[string]interface{}) {
	c.mu.RLock()
	defer c.mu.RUnlock()
	ret = make(map[string]interface{})
	for k := range c.keys {
		ret[k] = c.Context.Value(mapContextKey(k))
	}
	return
}

// MapString needs directly call c.Context.Value prevent duplicate lock
func (c *MapContext) MapString() (ret map[string]string) {
	c.mu.RLock()
	defer c.mu.RUnlock()
	ret = make(map[string]string)
	for k := range c.keys {
		if v, ok := c.Context.Value(mapContextKey(k)).(string); ok {
			ret[k] = v
		}
	}
	if sp := opentracing.SpanFromContext(c); sp != nil {
		ret[LogKeyTrace] = fmt.Sprintf("%s", sp)
	}
	return
}

func (c *MapContext) SetTimeout(duration time.Duration) (cancel context.CancelFunc) {
	c.mu.Lock()
	defer c.mu.Unlock()
	c.Context, cancel = context.WithTimeout(c.Context, duration)
	return
}

func (c *MapContext) SetDeadline(d time.Time) (cancel context.CancelFunc) {
	c.mu.Lock()
	defer c.mu.Unlock()
	c.Context, cancel = context.WithDeadline(c.Context, d)
	return
}

func (c *MapContext) Cancel() (cancel context.CancelFunc) {
	c.mu.Lock()
	defer c.mu.Unlock()
	c.Context, cancel = context.WithCancel(c.Context)
	return
}

func (c *MapContext) InjectHTTPHeader(rh http.Header) {
	for hk, sk := range c.HeaderKeyMap() {
		if s := rh.Get(hk); len(s) == 0 {
			rh.Set(hk, sk)
		}
	}
}

// HeaderKeyMap returns a map, key is HTTP Header Field, value is the field value stored in Context
func (c *MapContext) HeaderKeyMap() (ret map[string]string) {
	ret = make(map[string]string)
	for sk, hk := range sKMap {
		v, _ := c.GetString(sk)
		if len(v) > 0 {
			ret[hk] = v
		}
	}
	if sp := c.GetSpan(); sp != nil {
		ret[HTTPHeaderTrace] = fmt.Sprintf("%s", sp)
	}
	switch role := c.Get(LogKeyUserRole).(type) {
	case string:
		ret[HTTPHeaderUserRole] = role
	case []string:
		ret[HTTPHeaderUserRole] = strings.Join(role, ",")
	}
	return
}

// SetShortenKey sets Context with Header Key, Store in Context with LogKey Key
func (c *MapContext) SetShortenKey(headerField string, headerValue interface{}) {
	if sk, ok := hKMap[headerField]; ok {
		c.Set(sk, headerValue)
	}
}

// GetCID return the CID, if not, generate one
func (c *MapContext) GetCID() (string, error) {
	cid, ok := c.GetString(LogKeyCID)
	if !ok {
		cid = strconv.FormatInt(time.Now().UnixNano(), 10)
		c.Set(LogKeyCID, cid)
	}
	return cid, nil
}

func (c *MapContext) Err() error {
	c.mu.RLock()
	defer c.mu.RUnlock()
	return c.Context.Err()
}

func (c *MapContext) Value(key interface{}) interface{} {
	c.mu.RLock()
	defer c.mu.RUnlock()
	return c.Context.Value(key)
}

func (c *MapContext) Deadline() (deadline time.Time, ok bool) {
	c.mu.RLock()
	defer c.mu.RUnlock()
	return c.Context.Deadline()
}

// Note: if parent is not a cancelCtx, then there is no way to cancel children ctx from the parent
func (parent *MapContext) WithCancel() (ctx *MapContext, cancel context.CancelFunc) {
	c, cancel := context.WithCancel(parent.Context)
	ctx = &MapContext{Context: c, keys: parent.keys}
	return
}

// Note: if parent is not a cancelCtx, then there is no way to cancel children ctx from the parent
func (parent *MapContext) WithDeadline(d time.Time) (ctx *MapContext, cancel context.CancelFunc) {
	c, cancel := context.WithDeadline(parent.Context, d)
	ctx = &MapContext{Context: c, keys: parent.keys}
	return
}
