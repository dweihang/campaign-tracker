module gitlab.com/cake/goctx

require (
	github.com/opentracing/opentracing-go v1.1.0
	github.com/stretchr/testify v1.5.1 // indirect
)

go 1.13
