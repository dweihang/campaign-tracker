package errors

import (
	"gitlab.com/cake/gopkg"
	"log"
)

var (
	EmailWriteBodyError         = define(4800001)
	EmailEndTransactionError    = define(4800002)
	EmailSMTPServerNotAvailable = define(4800003)
	EmailStartTransactionError  = define(4800004)
	EmailRcptAddError           = define(4800005)
	EmailNoRecipients           = define(4800006)
	EmailStartDataWriterError   = define(4800007)

	JIRACreateTicketError        = define(4800008)
	JIRAGetCustomFieldsError     = define(4800009)
	JIRACreateTicketCommentError = define(4800010)
	JIRACloseTicketError         = define(4800011)

	GoHTTPRequestConversionFailed        = define(4800100)
	GoHTTPRequestIDRetrievalFailed       = define(4800101)
	GoHTTPRequestIDGenerationFailed      = define(4800102)
	GoHTTPMarshallingFailed              = define(4800104)
	GoHTTPResponseIsNil                  = define(4800105)
	GoHTTPResponseBodyReadingError       = define(4800106)
	GoHTTPResponseBodyUnmarshallingError = define(4800107)
	GoHTTPRequestError                   = define(4800108)
	GoHTTPRequestBodyCreationError       = define(4800109)
	GoHTTPResponseBodyIsNil              = define(4800110)

	//DB errors
	MongoUnmarshallingFailed       = define(4800200)
	MongoMarshallingFailed         = define(4800201)
	MongoQueryFailed               = define(4800202)
	MongoEntityNotExists           = define(4800203)
	MongoFailedToInsert            = define(4800204)
	MongoFailedToUpdate            = define(4800205)
	MongoFailedToDelete            = define(4800206)
	MongoEntityDecodingFailed      = define(4800207)
	MongoFailedToCountCollection   = define(4800208)
	MongoMissingEntityOrParamError = define(4800209)
	MongoDuplicateKeyError         = define(4800210)
	MongoSessionError              = define(4800211)
	MongoTransactionError          = define(4800212)
	MongoTransactionFailError      = define(4800213)
	MongoDataMigrationError        = define(4800214)
	MongoInvalidQuery              = define(4800215)
	MongoReplaceOneFailed          = define(4800216)
	MongoFailedToFind              = define(4800217)

	GinBindJSONError = define(4800300)

	CampaignNotFound       = define(4800400)
	CampaignUnknownTracker = define(4800401)

	JsonMarshallingFailed   = define(4800500)
	JsonUnmarshallingFailed = define(4800501)

	EmailValidationFailed        = define(4800600)
	EmailMissingLanguageTemplate = define(4800601)
	EmailCannotParseTemplate     = define(4800602)
	EmailAlreadySent             = define(4800604)

	InvalidRequest = define(4800703)
)
var placeHolder = struct{}{}
var codeMap = make(map[int]struct{})

func define(code int) int {
	if _, exists := codeMap[code]; exists {
		log.Fatalf("code %d already define", code)
	}
	codeMap[code] = placeHolder
	return code
}

func NewErrorWithStacktrace(errorCode int, message string, err error) gopkg.CodeError {
	return gopkg.NewWrappedCodeError(errorCode, message, err)
}
