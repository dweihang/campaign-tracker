package util

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/cake/goctx"
	"strings"
	"time"
)

const (
	KeyContext          = "internal.context"
	KeyTrackingId       = "trackingId"
	KeyCampaignId       = "campaignId"
	KeyEvent            = "event"
	KeyIp               = "ip"
	KeyEventTriggerTime = "eventTriggerTime"
	HeaderRequestId     = "X-M800-Client-ReqID"
)

type InternalObj map[string]interface{}

func (i InternalObj) GetEvent() string {
	return i[KeyEvent].(string)
}

func (i InternalObj) GetCampaignId() string {
	return i[KeyCampaignId].(string)
}

func (i InternalObj) GetTrackingId() string {
	return i[KeyTrackingId].(string)
}

func (i InternalObj) GetIP() string {
	return i[KeyIp].(string)
}

func (i InternalObj) GetEventTriggerTime() int64 {
	return i[KeyEventTriggerTime].(int64)
}

func GetContextFromGin(ctx *gin.Context) goctx.Context {
	if c, exists := ctx.Get(KeyContext); exists {
		return c.(goctx.Context)
	} else {
		ret := goctx.GetContextFromGetHeader(ctx)
		requestID := ctx.Request.Header.Get(HeaderRequestId)
		if len(requestID) > 0 {
			ret.Set(goctx.LogKeyCID, requestID)
		} else {
			_, _ = ret.GetCID()
		}
		ctx.Set(KeyContext, ret)
		return ret
	}
}

func GetInternalObjectFromGin(ctx *gin.Context) InternalObj {
	if obj, exists := ctx.Get("internal.obj"); exists {
		return obj.(map[string]interface{})
	}
	obj := make(map[string]interface{})
	obj[KeyTrackingId] = ctx.Query("tid")
	obj[KeyCampaignId] = ctx.Query("campaignId")
	obj[KeyEvent] = ctx.Query("event")
	obj[KeyIp] =    ctx.ClientIP()
	obj[KeyEventTriggerTime] = time.Now().UnixNano() / 1e6
	ctx.Set("internal.obj", obj)
	return obj
}

func GetQueryAsMapFromGin(ctx *gin.Context) map[string]interface{} {
	ret := make(map[string]interface{})
	for k, v := range ctx.Request.URL.Query() {
		k = strings.ReplaceAll(k, "-", "")
		k = strings.ToLower(k)
		if len(v) == 0 {
			ret[k] = ""
		} else if len(v) == 1 {
			ret[k] = v[0]
		} else {
			ret[k] = v
		}
	}
	return ret
}

func GetHeaderAsMapFromGin(ctx *gin.Context) map[string]interface{} {
	ret := make(map[string]interface{})
	for k, v := range ctx.Request.Header {
		k = strings.ReplaceAll(k, "-", "")
		k = strings.ToLower(k)
		if len(v) == 0 {
			ret[k] = ""
		} else if len(v) == 1 {
			ret[k] = v[0]
		} else {
			ret[k] = v
		}
	}
	return ret
}
