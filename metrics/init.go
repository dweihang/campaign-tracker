package metrics


import (
"log"
"sync"

"github.com/gin-gonic/gin"

"github.com/prometheus/client_golang/prometheus"
"github.com/spf13/viper"
ginPrometheus "gitlab.com/cake/gin-prometheus"
)

const (
	//Specific to Prometheus Metrics
	//Histogram config
	histogramStart = "timers.histogram.start"
	histogramWidth = "timers.histogram.width"
	histogramCount = "timers.histogram.count"
)

var defaultHistogramStart, defaultHistogramWidth float64
var defaultHistogramCount int
var once sync.Once
var metrics *ginPrometheus.GinPrometheus

func init() {
	defaultHistogramStart, defaultHistogramWidth, defaultHistogramCount = 0.1, 0.1, 10
}

func Init() {
	once.Do(func() {
		var err error
		metrics, err = ginPrometheus.NewPrometheus("gin",
			ginPrometheus.HistogramMetrics("gin", ginPrometheus.DefaultDurationBucket, ginPrometheus.DefaultSizeBucket),
			ginPrometheus.HistogramHandleFunc())

		if err != nil {
			log.Fatalf("Error while initializing metrics. Shutting down. error %s", err)
		}

		defaultHistogramStart = viper.GetFloat64(histogramStart)
		defaultHistogramWidth = viper.GetFloat64(histogramWidth)
		defaultHistogramCount = viper.GetInt(histogramCount)
	})
}

type Observer prometheus.Histogram

func Use(engine *gin.Engine) {
	metrics.Use(engine)
}

func NewObserver(name, description string) Observer {
	observer := prometheus.NewHistogram(prometheus.HistogramOpts{
		Name: name,
		Help: description,
		Buckets: prometheus.LinearBuckets(
			defaultHistogramStart,
			defaultHistogramWidth,
			defaultHistogramCount),
	})
	prometheus.MustRegister(observer)

	return observer
}

func NewTimer(observer Observer) *prometheus.Timer {
	return prometheus.NewTimer(observer)
}

