module m800-campaign-tracker

go 1.13

require (
	github.com/getkin/kin-openapi v0.2.0
	github.com/gin-gonic/gin v1.5.0
	github.com/golang/mock v1.4.0
	github.com/modern-go/concurrent v0.0.0-20180306012644-bacd9c7ef1dd // indirect
	github.com/modern-go/reflect2 v1.0.1 // indirect
	github.com/prometheus/client_golang v1.4.0
	github.com/sirupsen/logrus v1.6.0
	github.com/spf13/viper v1.6.2
	github.com/stretchr/testify v1.5.1
	gitlab.com/cake/gin-prometheus v1.1.3
	gitlab.com/cake/goctx v1.6.2
	gitlab.com/cake/gopkg v1.4.1
	gitlab.com/cake/m800log v1.4.9
	go.mongodb.org/mongo-driver v1.3.3
)
