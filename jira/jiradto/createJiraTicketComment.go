package jiradto

type CreateJiraTicketCommentRequest struct {
	Body         string `json:"body"`
	TicketNumber string `json:"ticketNumber"`
}

type CreateJiraTicketCommentResponse map[string]interface{}
