package jiradto

type JiraField struct {
	Name   string           `json:"name"`
	Id     string           `json:"id"`
	Custom bool             `json:"custom"`
	Schema JiraField_Schema `json:"schema"`
}

type JiraField_Schema struct {
	Type string `json:"type"`
}
