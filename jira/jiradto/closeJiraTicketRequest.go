package jiradto

type CloseJiraTicketRequest struct {
	TicketNumber     string `json:"ticketNumber"`
	JIRATransitionId string `json:"jiraTransitionId"`
}
