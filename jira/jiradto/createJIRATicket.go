package jiradto

type CreateJiraTicketRequest struct {
	ProjectKey   string
	Summary      string
	Labels       []string
	Description  string
	Assignee     string
	IssueType    string
	CustomFields map[string]interface{}
}

type CreateJiraTicketResponse struct {
	Id   string `json:"id"`
	Key  string `json:"key"`
	Self string `json:"self"`
}