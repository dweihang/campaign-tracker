package jira

import (
	"encoding/base64"
	"encoding/json"
	"fmt"
	"gitlab.com/cake/goctx"
	"gitlab.com/cake/gopkg"
	"m800-campaign-tracker/errors"
	"m800-campaign-tracker/gohttp"
	"m800-campaign-tracker/jira/jiradto"
	"net/http"
)

const (
	PathCreateTicket = "/rest/api/2/issue"
	PathGetField     = "/rest/api/2/field"
)

type Client interface {
	CreateJiraTicket(ctx goctx.Context, request *jiradto.CreateJiraTicketRequest) (*jiradto.CreateJiraTicketResponse, gopkg.CodeError)
	CreateJiraTicketComment(ctx goctx.Context, request *jiradto.CreateJiraTicketCommentRequest) (*jiradto.CreateJiraTicketCommentResponse, gopkg.CodeError)
	CloseJiraTicket(ctx goctx.Context, request *jiradto.CloseJiraTicketRequest) gopkg.CodeError
}

type client struct {
	httpCli          gohttp.Client
	url              string
	username         string
	password         string
	customFieldCache map[string]jiradto.JiraField
}

func NewJIRACli(url string, httpCli gohttp.Client, username string, password string) Client {
	if url[len(url)-1] == '/' {
		url = url[:len(url)-1]
	}
	return &client{
		url:      url,
		httpCli:  httpCli,
		username: username,
		password: password,
	}
}

func (p *client) CloseJiraTicket(ctx goctx.Context, request *jiradto.CloseJiraTicketRequest) gopkg.CodeError {
	jiraReq := make(map[string]interface{})

	auth := p.username + ":" + p.password
	authHeader := "Basic " + base64.StdEncoding.EncodeToString([]byte(auth))

	jiraReq["transition"] = map[string]interface{}{"id": request.JIRATransitionId}

	reqJsonBody, err := json.Marshal(jiraReq)
	if err != nil {
		return gopkg.NewWrappedCodeError(errors.JIRACloseTicketError, fmt.Sprintf("CloseJiraTicket Failed serialize payload %#v", request), err)
	}
	resp, err := p.httpCli.PostWithInterceptor(ctx, p.url+PathCreateTicket+"/"+request.TicketNumber+"/transitions", reqJsonBody, gohttp.InsertHeadersInterceptor(map[string]string{"Authorization": authHeader, "Content-Type": "application/json"}))
	if err != nil {
		return gopkg.NewWrappedCodeError(errors.JIRACloseTicketError, fmt.Sprintf("CloseJiraTicket Failed to send http request with payload %#v", request), err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusNoContent {
		return gopkg.NewCodeError(errors.JIRACloseTicketError, fmt.Sprintf("CloseJiraTicket Received unexpected http status code=%d", resp.StatusCode))
	}

	return nil
}

func (p *client) CreateJiraTicketComment(ctx goctx.Context, request *jiradto.CreateJiraTicketCommentRequest) (*jiradto.CreateJiraTicketCommentResponse, gopkg.CodeError) {
	jiraReq := make(map[string]interface{})

	auth := p.username + ":" + p.password
	authHeader := "Basic " + base64.StdEncoding.EncodeToString([]byte(auth))

	jiraReq["body"] = request.Body

	reqJsonBody, err := json.Marshal(jiraReq)
	if err != nil {
		return nil, gopkg.NewWrappedCodeError(errors.JIRACreateTicketCommentError, fmt.Sprintf("CreateJiraTicketComment Failed serialize payload %#v", request), err)
	}
	resp, err := p.httpCli.PostWithInterceptor(ctx, p.url+PathCreateTicket+"/"+request.TicketNumber+"/comment", reqJsonBody, gohttp.InsertHeadersInterceptor(map[string]string{"Authorization": authHeader, "Content-Type": "application/json"}))
	if err != nil {
		return nil, gopkg.NewWrappedCodeError(errors.JIRACreateTicketCommentError, fmt.Sprintf("CreateJiraTicketComment Failed to send http request with payload %#v", request), err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusCreated {
		return nil, gopkg.NewCodeError(errors.JIRACreateTicketCommentError, fmt.Sprintf("CreateJiraTicketComment Received unexpected http status code=%d", resp.StatusCode))
	}

	respObj := &jiradto.CreateJiraTicketCommentResponse{}
	err = gohttp.GetResponseBodyAsType(ctx, resp, respObj)

	if err != nil {
		return nil, gopkg.NewWrappedCodeError(errors.JIRACreateTicketCommentError, "CreateJiraTicketComment Failed to read response body", err)
	}

	return respObj, nil
}

func (p *client) CreateJiraTicket(ctx goctx.Context, request *jiradto.CreateJiraTicketRequest) (*jiradto.CreateJiraTicketResponse, gopkg.CodeError) {
	jiraReq := make(map[string]interface{})
	fields := make(map[string]interface{})

	for name, val := range request.CustomFields {
		jiraField, err := p.getJiraFieldByName(ctx, name)
		if err != nil {
			return nil, err
		}
		if jiraField.Schema.Type == "option" {
			fields[jiraField.Id] = map[string]interface{}{
				"value": val,
			}
		} else {
			fields[jiraField.Id] = val
		}
	}

	fields["summary"] = request.Summary
	fields["description"] = request.Description

	fieldsProject := make(map[string]interface{})
	fieldsProject["key"] = request.ProjectKey
	fields["project"] = fieldsProject

	if len(request.Labels) > 0 {
		fields["labels"] = request.Labels
	}

	fieldsIssueType := make(map[string]interface{})
	fieldsIssueType["name"] = request.IssueType
	fields["issuetype"] = fieldsIssueType

	if len(request.Assignee) > 0 {
		fieldsAssignee := make(map[string]interface{})
		fieldsAssignee["name"] = request.Assignee
		fields["assignee"] = fieldsAssignee
	}

	jiraReq["fields"] = fields

	reqJsonBody, err := json.Marshal(jiraReq)
	if err != nil {
		return nil, gopkg.NewWrappedCodeError(errors.JIRACreateTicketError, fmt.Sprintf("CreateJiraTicket Failed serialize payload %#v", request), err)
	}

	auth := p.username + ":" + p.password
	authHeader := "Basic " + base64.StdEncoding.EncodeToString([]byte(auth))

	resp, err := p.httpCli.PostWithInterceptor(ctx, p.url+PathCreateTicket, reqJsonBody, gohttp.InsertHeadersInterceptor(map[string]string{"Authorization": authHeader, "Content-Type": "application/json"}))
	if err != nil {
		return nil, gopkg.NewWrappedCodeError(errors.JIRACreateTicketError, fmt.Sprintf("CreateJiraTicket Failed to send http request with payload %#v", request), err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusCreated {
		return nil, gopkg.NewCodeError(errors.JIRACreateTicketError, fmt.Sprintf("CreateJiraTicket Received unexpected http status code=%d", resp.StatusCode))
	}

	respObj := &jiradto.CreateJiraTicketResponse{}
	err = gohttp.GetResponseBodyAsType(ctx, resp, respObj)

	if err != nil {
		return nil, gopkg.NewWrappedCodeError(errors.JIRACreateTicketError, "CreateJiraTicket Failed to read response body", err)
	}

	return respObj, nil
}

func (p *client) GetJiraCustomFields(ctx goctx.Context) ([]jiradto.JiraField, gopkg.CodeError) {
	auth := p.username + ":" + p.password
	authHeader := "Basic " + base64.StdEncoding.EncodeToString([]byte(auth))

	resp, err := p.httpCli.GetWithInterceptor(ctx, p.url+PathGetField, gohttp.InsertHeadersInterceptor(map[string]string{"Authorization": authHeader}))
	if err != nil {
		return nil, gopkg.NewWrappedCodeError(errors.JIRAGetCustomFieldsError, "GetJiraCustomFields Failed to send http request to JIRA server", err)
	}
	defer resp.Body.Close()

	ret := []jiradto.JiraField{}
	err = gohttp.GetResponseBodyAsType(ctx, resp, &ret)
	if err != nil {
		return nil, gopkg.NewWrappedCodeError(errors.JIRAGetCustomFieldsError, "GetJiraCustomFields Failed to decode JIRA server response", err)
	}
	return ret, nil
}

func (p *client) getJiraFieldByName(ctx goctx.Context, fieldName string) (*jiradto.JiraField, gopkg.CodeError) {
	if p.customFieldCache == nil {
		fields, err := p.GetJiraCustomFields(ctx)
		if err != nil {
			return nil, err
		}
		val := make(map[string]jiradto.JiraField)
		for _, f := range fields {
			val[f.Name] = f
		}
		p.customFieldCache = val
	}
	if field, exist := p.customFieldCache[fieldName]; !exist {
		return nil, gopkg.NewCodeError(errors.JIRAGetCustomFieldsError, fmt.Sprintf("GetJiraCustomFields custom field not defined in jira, field=%s", fieldName))
	} else {
		return &field, nil
	}
}
