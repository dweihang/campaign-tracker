package jira

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"m800-campaign-tracker/gohttp"
	"m800-campaign-tracker/jira/jiradto"
	"net/http"
	"testing"

	"github.com/golang/mock/gomock"
	"github.com/stretchr/testify/assert"
	"gitlab.com/cake/goctx"
)

func TestClient_CreateJiraTask(t *testing.T) {
	ctrl := gomock.NewController(t)
	httpCli := gohttp.NewMockClient(ctrl)
	testJiraUrl := "https://issuetracking.maaii.com:8443"
	getFieldRespBytes, _ := json.Marshal([]jiradto.JiraField{
		{
			Name:   "First Name",
			Id:     "1",
			Custom: true,
		},
		{
			Name:   "Last Name",
			Id:     "2",
			Custom: true,
		},
	})

	createRespBytes, _ := json.Marshal(jiradto.CreateJiraTicketResponse{
		Id:   "1",
		Key:  "1",
		Self: "1",
	})
	httpCli.EXPECT().GetWithInterceptor(gomock.Any(), gomock.Eq(testJiraUrl+PathGetField), gomock.Any()).Return(&http.Response{
		StatusCode: http.StatusOK,
		Body:       ioutil.NopCloser(bytes.NewReader(getFieldRespBytes)),
	}, nil)
	httpCli.EXPECT().PostWithInterceptor(gomock.Any(), gomock.Eq(testJiraUrl+PathCreateTicket), gomock.Any(), gomock.Any()).Return(&http.Response{
		StatusCode: http.StatusCreated,
		Body:       ioutil.NopCloser(bytes.NewReader(createRespBytes)),
	}, nil)

	jira := NewJIRACli(testJiraUrl, httpCli, "raymonddu", "Zxcv!234")

	_, err := jira.CreateJiraTicket(goctx.Background(), &jiradto.CreateJiraTicketRequest{
		IssueType:   "Story",
		ProjectKey:  "TPWSF",
		Summary:     "raymond test",
		Description: "",
		Assignee:    "",
		Labels:      []string{"MaaiiconnectSignupEngageEssential"},
		CustomFields: map[string]interface{}{
			"First Name":      "raymond",
			"Last Name":       "du",
		},
	})
	assert.Nil(t, err)
}
