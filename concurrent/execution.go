package concurrent

import (
	"gitlab.com/cake/goctx"
	"gitlab.com/cake/gopkg"
)

const (
	executionTypeParallel = iota
	executionTypeSerial
)

type ExecutionResultIterator struct {
	currentIndex int
	results      [][]*TaskResult
}

func (it *ExecutionResultIterator) Next() []*TaskResult {
	if it.currentIndex >= len(it.results) {
		return nil
	}
	ret := it.results[it.currentIndex]
	it.currentIndex = it.currentIndex + 1
	return ret
}

type Execution struct {
	root     *Execution
	next     *Execution
	tasks    []*Task
	execType int
}

func (e *Execution) parallelExec(ctx goctx.Context) ([]*TaskResult, gopkg.CodeError) {
	var futures []TaskFuture
	var ret []*TaskResult
	for i := range e.tasks {
		futures = append(futures, e.tasks[i].executor.Execute(ctx, e.tasks[i].taskFunc))
	}
	var err gopkg.CodeError
	for i := range futures {
		result := futures[i].Get()
		if result.Error != nil {
			if err == nil {
				err = result.Error
			} else {
				err = gopkg.NewWrappedCodeError(result.Error.ErrorCode(), result.Error.ErrorMsg(), err)
			}
		}
		ret = append(ret, result)
	}
	return ret, err
}

func (e *Execution) serialExec(ctx goctx.Context) ([]*TaskResult, gopkg.CodeError) {
	var ret []*TaskResult
	for i := range e.tasks {
		result := e.tasks[i].executor.Execute(ctx, e.tasks[i].taskFunc).Get()
		ret = append(ret, result)
		if result.Error != nil {
			return ret, result.Error
		}
	}
	return ret, nil
}

func (e *Execution) nextExecution(tasks []*Task, executionType int) *Execution {
	ret := &Execution{
		root:     nil,
		next:     nil,
		tasks:    tasks,
		execType: executionType,
	}
	if e.root == nil {
		ret.root = e
	} else {
		ret.root = e.root
	}
	e.next = ret
	return ret
}

func (e *Execution) ExecuteParallel(tasks ...*Task) *Execution {
	return e.nextExecution(tasks, executionTypeParallel)
}

func (e *Execution) ExecuteSerial(tasks ...*Task) *Execution {
	return e.nextExecution(tasks, executionTypeSerial)
}

func (e *Execution) Async(ctx goctx.Context, executor TaskExecutor, callback func(*ExecutionResultIterator, gopkg.CodeError)) {
	var execTaskFunc TaskFunc = func(ctx goctx.Context) *TaskResult {
		result, err := e.Await(ctx)
		if callback != nil {
			callback(result, err)
		}
		return NewTaskResult(result, err)
	}
	_ = executor.Execute(ctx, execTaskFunc)
}

func (e *Execution) Await(ctx goctx.Context) (*ExecutionResultIterator, gopkg.CodeError) {
	iter := e.root
	if e.root == nil {
		iter = e
	}
	ret := &ExecutionResultIterator{}
	var err gopkg.CodeError
	for {
		var execResult []*TaskResult
		switch iter.execType {
		case executionTypeParallel:
			execResult, err = iter.parallelExec(ctx)
		default:
			execResult, err = iter.serialExec(ctx)
		}
		ret.results = append(ret.results, execResult)
		if err != nil {
			break
		}
		iter = iter.next
		if iter == nil {
			break
		}
	}
	return ret, err
}

func ExecuteParallel(tasks ...*Task) *Execution {
	return &Execution{
		root:     nil,
		next:     nil,
		tasks:    tasks,
		execType: executionTypeParallel,
	}
}

func ExecuteSerial(tasks ...*Task) *Execution {
	return &Execution{
		root:     nil,
		next:     nil,
		tasks:    tasks,
		execType: executionTypeSerial,
	}
}
