package concurrent

import (
	"gitlab.com/cake/goctx"
	"gitlab.com/cake/gopkg"
)

type TaskFunc func(ctx goctx.Context) *TaskResult

type TaskResult struct {
	Result interface{}
	Error  gopkg.CodeError
}

func NewTaskResult(result interface{}, err gopkg.CodeError) *TaskResult {
	return &TaskResult{
		Result: result,
		Error:  err,
	}
}

type TaskFuture interface {
	Get() *TaskResult
}

type TaskExecutor interface {
	Execute(ctx goctx.Context, task TaskFunc) TaskFuture
}

type Task struct {
	taskFunc TaskFunc
	executor TaskExecutor
}

func NewTask(task TaskFunc, executor TaskExecutor) *Task {
	return &Task{
		taskFunc: task,
		executor: executor,
	}
}
