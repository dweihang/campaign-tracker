package concurrent

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/cake/goctx"
	"testing"
)

func TestAsyncExecutor_Execute(t *testing.T) {
	underTest := NewAsyncExecutor()
	ret := underTest.Execute(goctx.Background(), func(ctx goctx.Context) *TaskResult {
		return NewTaskResult("test", nil)
	})
	result := ret.Get()
	assert.Nil(t, result.Error)
	assert.Equal(t, "test", result.Result)
}
