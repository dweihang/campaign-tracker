package concurrent

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/cake/goctx"
	"gitlab.com/cake/gopkg"
	"sync"
	"testing"
	"time"
)

func TestExecuteSerial(t *testing.T) {
	var time1, time2 *time.Time
	var t1 TaskFunc = func(ctx goctx.Context) *TaskResult {
		time.Sleep(100 * time.Millisecond)
		now := time.Now()
		time1 = &now
		return NewTaskResult("task1", nil)
	}
	var t2 TaskFunc = func(ctx goctx.Context) *TaskResult {
		time.Sleep(100 * time.Millisecond)
		now := time.Now()
		time2 = &now
		return NewTaskResult("task2", nil)
	}
	executors := []TaskExecutor{NewPoolExecutor(20, 20), NewAsyncExecutor(), NewImmediateExecutor()}
	for i := range executors {
		iter, err := ExecuteSerial(NewTask(t1, executors[i]), NewTask(t2, executors[i])).Await(goctx.Background())
		results := iter.Next()
		assert.Nil(t, err)
		assert.Equal(t, 2, len(results))
		assert.NotNil(t, time1)
		assert.NotNil(t, time2)
		assert.True(t, (*time2).Sub(*time1) >= 100*time.Millisecond)
		assert.Equal(t, "task1", results[0].Result)
		assert.Equal(t, "task2", results[1].Result)
	}
}

func TestExecuteSerial_CanAbortWithError(t *testing.T) {
	var time1, time3 *time.Time
	var t1 TaskFunc = func(ctx goctx.Context) *TaskResult {
		time.Sleep(100 * time.Millisecond)
		now := time.Now()
		time1 = &now
		return NewTaskResult("task1", nil)
	}
	var t2 TaskFunc = func(ctx goctx.Context) *TaskResult {
		return NewTaskResult(nil, gopkg.NewCodeError(0, "test"))
	}
	var t3 TaskFunc = func(ctx goctx.Context) *TaskResult {
		time.Sleep(100 * time.Millisecond)
		now := time.Now()
		time3 = &now
		return NewTaskResult("task3", nil)
	}
	executors := []TaskExecutor{NewPoolExecutor(20, 20), NewAsyncExecutor(), NewImmediateExecutor()}
	for i := range executors {
		iter, err := ExecuteSerial(NewTask(t1, executors[i]), NewTask(t2, executors[i]), NewTask(t3, executors[i])).Await(goctx.Background())
		results := iter.Next()
		assert.NotNil(t, err)
		assert.Equal(t, 2, len(results))
		assert.NotNil(t, time1)
		assert.Nil(t, time3)
		assert.Equal(t, "task1", results[0].Result)
		assert.NotNil(t, results[1].Error)
	}
}

func TestExecuteParallel(t *testing.T) {
	var t1 TaskFunc = func(ctx goctx.Context) *TaskResult {
		time.Sleep(5000 * time.Millisecond)
		return NewTaskResult("task1", nil)
	}
	var t2 TaskFunc = func(ctx goctx.Context) *TaskResult {
		time.Sleep(1000 * time.Millisecond)
		return NewTaskResult("task2", nil)
	}

	executors := []TaskExecutor{NewPoolExecutor(20, 20), NewAsyncExecutor()}
	for i := range executors {
		now := time.Now()
		iter, err := ExecuteParallel(NewTask(t1, executors[i]), NewTask(t2, executors[i])).Await(goctx.Background())
		elapse := time.Now().Sub(now)
		assert.True(t, elapse < 5100*time.Millisecond)
		results := iter.Next()
		assert.Nil(t, err)
		assert.Equal(t, 2, len(results))
		assert.Equal(t, "task1", results[0].Result)
		assert.Equal(t, "task2", results[1].Result)
	}
}

func TestExecuteParallel_ImmediateExecutor(t *testing.T) {
	var time1, time2 *time.Time
	var t1 TaskFunc = func(ctx goctx.Context) *TaskResult {
		time.Sleep(100 * time.Millisecond)
		now := time.Now()
		time1 = &now
		return NewTaskResult("task1", nil)
	}
	var t2 TaskFunc = func(ctx goctx.Context) *TaskResult {
		time.Sleep(100 * time.Millisecond)
		now := time.Now()
		time2 = &now
		return NewTaskResult("task2", nil)
	}

	iter, err := ExecuteParallel(NewTask(t1, NewImmediateExecutor()), NewTask(t2, NewImmediateExecutor())).Await(goctx.Background())
	results := iter.Next()
	assert.Nil(t, err)
	assert.Equal(t, 2, len(results))
	assert.NotNil(t, time1)
	assert.NotNil(t, time2)
	assert.True(t, (*time2).Sub(*time1) >= 100*time.Millisecond)
	assert.Equal(t, "task1", results[0].Result)
	assert.Equal(t, "task2", results[1].Result)
}

func TestExecuteParallel_Error(t *testing.T) {
	var time1, time3 *time.Time
	var t1 TaskFunc = func(ctx goctx.Context) *TaskResult {
		time.Sleep(100 * time.Millisecond)
		now := time.Now()
		time1 = &now
		return NewTaskResult("task1", nil)
	}
	var t2 TaskFunc = func(ctx goctx.Context) *TaskResult {
		return NewTaskResult(nil, gopkg.NewCodeError(0, "test"))
	}
	var t3 TaskFunc = func(ctx goctx.Context) *TaskResult {
		time.Sleep(100 * time.Millisecond)
		now := time.Now()
		time3 = &now
		return NewTaskResult("task2", nil)
	}
	var t4 TaskFunc = func(ctx goctx.Context) *TaskResult {
		return NewTaskResult(nil, gopkg.NewCodeError(0, "test"))
	}
	executors := []TaskExecutor{NewPoolExecutor(20, 20), NewAsyncExecutor()}
	for i := range executors {
		iter, err := ExecuteParallel(NewTask(t1, executors[i]), NewTask(t2, executors[i]), NewTask(t3, executors[i]), NewTask(t4, executors[i])).Await(goctx.Background())
		results := iter.Next()
		assert.NotNil(t, err)
		assert.Equal(t, 4, len(results))
		assert.NotNil(t, time1)
		assert.NotNil(t, time3)
		assert.Equal(t, "task1", results[0].Result)
		assert.Equal(t, "task2", results[2].Result)
		assert.NotNil(t, results[1].Error)
		assert.NotNil(t, results[3].Error)
	}
}

func TestCascade_SerialAfterParallel(t *testing.T) {
	var time1, time2, time3, time4 *time.Time
	var t1 TaskFunc = func(ctx goctx.Context) *TaskResult {
		time.Sleep(100 * time.Millisecond)
		now := time.Now()
		time1 = &now
		return NewTaskResult("task1", nil)
	}
	var t2 TaskFunc = func(ctx goctx.Context) *TaskResult {
		time.Sleep(100 * time.Millisecond)
		now := time.Now()
		time2 = &now
		return NewTaskResult("task2", nil)
	}
	var t3 TaskFunc = func(ctx goctx.Context) *TaskResult {
		time.Sleep(100 * time.Millisecond)
		now := time.Now()
		time3 = &now
		return NewTaskResult("task3", nil)
	}
	var t4 TaskFunc = func(ctx goctx.Context) *TaskResult {
		time.Sleep(100 * time.Millisecond)
		now := time.Now()
		time4 = &now
		return NewTaskResult("task4", nil)
	}
	executors := []TaskExecutor{NewPoolExecutor(20, 20), NewAsyncExecutor()}
	for i := range executors {
		iter, err := ExecuteSerial(NewTask(t1, executors[i]), NewTask(t2, executors[i])).
			ExecuteParallel(NewTask(t3, executors[i]), NewTask(t4, executors[i])).
			Await(goctx.Background())
		assert.Nil(t, err)
		i := 0
		for {
			results := iter.Next()
			if results == nil {
				break
			}
			assert.Equal(t, 2, len(results))
			assert.NotNil(t, time1)
			assert.NotNil(t, time2)
			assert.True(t, (*time2).Sub(*time1) >= 100*time.Millisecond)
			assert.NotNil(t, time3)
			assert.True(t, (*time3).Sub(*time2) >= 100*time.Millisecond)
			assert.NotNil(t, time4)
			assert.True(t, (*time4).Sub(*time3) < 10*time.Millisecond)
			if i == 0 {
				assert.Equal(t, "task1", results[0].Result)
				assert.Equal(t, "task2", results[1].Result)
			} else if i == 1 {
				assert.Equal(t, "task3", results[0].Result)
				assert.Equal(t, "task4", results[1].Result)
			}
			i++
		}
		assert.Equal(t, 2, i)
	}
}

func TestCascade_ParallelAfterSerial(t *testing.T) {
	var time1, time2, time3, time4 *time.Time
	var t1 TaskFunc = func(ctx goctx.Context) *TaskResult {
		time.Sleep(100 * time.Millisecond)
		now := time.Now()
		time1 = &now
		return NewTaskResult("task1", nil)
	}
	var t2 TaskFunc = func(ctx goctx.Context) *TaskResult {
		time.Sleep(100 * time.Millisecond)
		now := time.Now()
		time2 = &now
		return NewTaskResult("task2", nil)
	}
	var t3 TaskFunc = func(ctx goctx.Context) *TaskResult {
		time.Sleep(100 * time.Millisecond)
		now := time.Now()
		time3 = &now
		return NewTaskResult("task3", nil)
	}
	var t4 TaskFunc = func(ctx goctx.Context) *TaskResult {
		time.Sleep(100 * time.Millisecond)
		now := time.Now()
		time4 = &now
		return NewTaskResult("task4", nil)
	}
	executors := []TaskExecutor{NewPoolExecutor(20, 20), NewAsyncExecutor()}
	for i := range executors {
		iter, err := ExecuteParallel(NewTask(t1, executors[i]), NewTask(t2, executors[i])).
			ExecuteSerial(NewTask(t3, executors[i]), NewTask(t4, executors[i])).
			Await(goctx.Background())
		assert.Nil(t, err)
		i := 0
		for {
			results := iter.Next()
			if results == nil {
				break
			}
			assert.Equal(t, 2, len(results))
			assert.NotNil(t, time1)
			assert.NotNil(t, time2)
			assert.True(t, (*time2).Sub(*time1) <= 10*time.Millisecond)
			assert.NotNil(t, time3)
			assert.True(t, (*time3).Sub(*time2) >= 100*time.Millisecond)
			assert.NotNil(t, time4)
			assert.True(t, (*time4).Sub(*time3) >= 100*time.Millisecond)
			if i == 0 {
				assert.Equal(t, "task1", results[0].Result)
				assert.Equal(t, "task2", results[1].Result)
			} else if i == 1 {
				assert.Equal(t, "task3", results[0].Result)
				assert.Equal(t, "task4", results[1].Result)
			}
			i++
		}
		assert.Equal(t, 2, i)
	}
}

func TestCascade_SerialAfterParallelError(t *testing.T) {
	var time1, time3, time4 *time.Time
	var t1 TaskFunc = func(ctx goctx.Context) *TaskResult {
		time.Sleep(100 * time.Millisecond)
		now := time.Now()
		time1 = &now
		return NewTaskResult("task1", nil)
	}
	var t2 TaskFunc = func(ctx goctx.Context) *TaskResult {
		return NewTaskResult(nil, gopkg.NewCodeError(0, "test"))
	}
	var t3 TaskFunc = func(ctx goctx.Context) *TaskResult {
		time.Sleep(100 * time.Millisecond)
		now := time.Now()
		time3 = &now
		return NewTaskResult("task3", nil)
	}
	var t4 TaskFunc = func(ctx goctx.Context) *TaskResult {
		time.Sleep(100 * time.Millisecond)
		now := time.Now()
		time4 = &now
		return NewTaskResult("task4", nil)
	}
	executors := []TaskExecutor{NewPoolExecutor(20, 20), NewAsyncExecutor()}
	for i := range executors {
		iter, err := ExecuteSerial(NewTask(t1, executors[i]), NewTask(t2, executors[i])).
			ExecuteParallel(NewTask(t3, executors[i]), NewTask(t4, executors[i])).
			Await(goctx.Background())
		assert.NotNil(t, err)
		i := 0
		for {
			results := iter.Next()
			if results == nil {
				break
			}
			assert.Equal(t, 2, len(results))
			assert.NotNil(t, time1)
			assert.Nil(t, time3)
			assert.Nil(t, time4)
			assert.Equal(t, "task1", results[0].Result)
			assert.Nil(t, results[1].Result)
			assert.NotNil(t, results[1].Error)
			i++
		}
		assert.Equal(t, 1, i)
	}
}

func TestCascade_SerialAfterParallelAfterParallel(t *testing.T) {
	var time1, time2, time3, time4, time5, time6 *time.Time
	var t1 TaskFunc = func(ctx goctx.Context) *TaskResult {
		time.Sleep(100 * time.Millisecond)
		now := time.Now()
		time1 = &now
		return NewTaskResult("task1", nil)
	}
	var t2 TaskFunc = func(ctx goctx.Context) *TaskResult {
		time.Sleep(100 * time.Millisecond)
		now := time.Now()
		time2 = &now
		return NewTaskResult("task2", nil)
	}
	var t3 TaskFunc = func(ctx goctx.Context) *TaskResult {
		time.Sleep(100 * time.Millisecond)
		now := time.Now()
		time3 = &now
		return NewTaskResult("task3", nil)
	}
	var t4 TaskFunc = func(ctx goctx.Context) *TaskResult {
		time.Sleep(100 * time.Millisecond)
		now := time.Now()
		time4 = &now
		return NewTaskResult("task4", nil)
	}
	var t5 TaskFunc = func(ctx goctx.Context) *TaskResult {
		time.Sleep(100 * time.Millisecond)
		now := time.Now()
		time5 = &now
		return NewTaskResult("task5", nil)
	}
	var t6 TaskFunc = func(ctx goctx.Context) *TaskResult {
		time.Sleep(100 * time.Millisecond)
		now := time.Now()
		time6 = &now
		return NewTaskResult("task6", nil)
	}
	executors := []TaskExecutor{NewPoolExecutor(20, 20), NewAsyncExecutor()}
	for i := range executors {
		iter, err := ExecuteSerial(NewTask(t1, executors[i]), NewTask(t2, executors[i])).
			ExecuteParallel(NewTask(t3, executors[i]), NewTask(t4, executors[i])).
			ExecuteParallel(NewTask(t5, executors[i]), NewTask(t6, executors[i])).
			Await(goctx.Background())
		assert.Nil(t, err)
		i := 0
		for {
			results := iter.Next()
			if results == nil {
				break
			}
			assert.Equal(t, 2, len(results))
			assert.NotNil(t, time1)
			assert.NotNil(t, time2)
			assert.True(t, (*time2).Sub(*time1) >= 100*time.Millisecond)
			assert.NotNil(t, time3)
			assert.True(t, (*time3).Sub(*time2) >= 100*time.Millisecond)
			assert.NotNil(t, time4)
			assert.True(t, (*time4).Sub(*time3) < 10*time.Millisecond)
			assert.NotNil(t, time5)
			assert.True(t, (*time5).Sub(*time4) >= 100*time.Millisecond)
			assert.NotNil(t, time6)
			assert.True(t, (*time6).Sub(*time5) < 10*time.Millisecond)
			if i == 0 {
				assert.Equal(t, "task1", results[0].Result)
				assert.Equal(t, "task2", results[1].Result)
			} else if i == 1 {
				assert.Equal(t, "task3", results[0].Result)
				assert.Equal(t, "task4", results[1].Result)
			} else if i == 2 {
				assert.Equal(t, "task5", results[0].Result)
				assert.Equal(t, "task6", results[1].Result)
			}
			i++
		}
		assert.Equal(t, 3, i)
	}
}

func TestExecution_Async(t *testing.T) {
	var time1, time2, time3, time4 *time.Time
	var t1 TaskFunc = func(ctx goctx.Context) *TaskResult {
		time.Sleep(100 * time.Millisecond)
		now := time.Now()
		time1 = &now
		return NewTaskResult("task1", nil)
	}
	var t2 TaskFunc = func(ctx goctx.Context) *TaskResult {
		time.Sleep(100 * time.Millisecond)
		now := time.Now()
		time2 = &now
		return NewTaskResult("task2", nil)
	}
	var t3 TaskFunc = func(ctx goctx.Context) *TaskResult {
		time.Sleep(100 * time.Millisecond)
		now := time.Now()
		time3 = &now
		return NewTaskResult("task3", nil)
	}
	var t4 TaskFunc = func(ctx goctx.Context) *TaskResult {
		time.Sleep(100 * time.Millisecond)
		now := time.Now()
		time4 = &now
		return NewTaskResult("task4", nil)
	}
	executors := []TaskExecutor{NewPoolExecutor(20, 20), NewAsyncExecutor()}
	for i := range executors {
		wg := sync.WaitGroup{}
		wg.Add(1)
		ExecuteParallel(NewTask(t1, executors[i]), NewTask(t2, executors[i])).
			ExecuteSerial(NewTask(t3, executors[i]), NewTask(t4, executors[i])).
			Async(goctx.Background(), executors[i], func(iter *ExecutionResultIterator, err gopkg.CodeError) {
				assert.Nil(t, err)
				i := 0
				for {
					results := iter.Next()
					if results == nil {
						break
					}
					assert.Equal(t, 2, len(results))
					assert.NotNil(t, time1)
					assert.NotNil(t, time2)
					assert.True(t, (*time2).Sub(*time1) <= 10*time.Millisecond)
					assert.NotNil(t, time3)
					assert.True(t, (*time3).Sub(*time2) >= 100*time.Millisecond)
					assert.NotNil(t, time4)
					assert.True(t, (*time4).Sub(*time3) >= 100*time.Millisecond)
					if i == 0 {
						assert.Equal(t, "task1", results[0].Result)
						assert.Equal(t, "task2", results[1].Result)
					} else if i == 1 {
						assert.Equal(t, "task3", results[0].Result)
						assert.Equal(t, "task4", results[1].Result)
					}
					i++
				}
				assert.Equal(t, 2, i)
				wg.Done()
			})
		wg.Wait()
	}
}
