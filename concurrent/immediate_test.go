package concurrent

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/cake/goctx"
	"testing"
)

func TestImmediateExecutor_Execute(t *testing.T) {
	underTest := NewImmediateExecutor()
	ret := underTest.Execute(goctx.Background(), func(ctx goctx.Context) *TaskResult {
		return NewTaskResult("test", nil)
	})
	result := ret.Get()
	assert.Nil(t, result.Error)
	assert.Equal(t, "test", result.Result)
}
