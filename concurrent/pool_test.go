package concurrent

import (
	"github.com/stretchr/testify/assert"
	"gitlab.com/cake/goctx"
	"testing"
)

func TestPoolExecutor_Execute(t *testing.T) {
	underTest := NewPoolExecutor(10, 10)
	ret := underTest.Execute(goctx.Background(), func(ctx goctx.Context) *TaskResult {
		return NewTaskResult("test", nil)
	})
	result := ret.Get()
	assert.Nil(t, result.Error)
	assert.Equal(t, "test", result.Result)
}
