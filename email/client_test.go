package email

import (
	"github.com/golang/mock/gomock"
	"gitlab.com/cake/goctx"
	"testing"
)

func TestEmailClient_Ping(t *testing.T) {
	ctrl := gomock.NewController(t)
	c := NewMocksmtpClient(ctrl)
	c.EXPECT().Quit()
	newSMTPClient = func(addr string) (s smtpClient, e error) {
		return c, nil
	}
	underTest := NewEmailClient("test")
	underTest.Ping()
}

type nopWriteCloser struct {
}

func (n2 nopWriteCloser) Write(p []byte) (n int, err error) {
	return 0, nil
}

func (n2 nopWriteCloser) Close() error {
	return nil
}

func TestEmailClient_Send(t *testing.T) {
	ctrl := gomock.NewController(t)
	c := NewMocksmtpClient(ctrl)
	c.EXPECT().Quit()
	c.EXPECT().Mail(gomock.Any())
	c.EXPECT().Rcpt(gomock.Any()).Times(2)
	c.EXPECT().Data().Return(nopWriteCloser{}, nil)
	newSMTPClient = func(addr string) (s smtpClient, e error) {
		return c, nil
	}
	underTest := NewEmailClient("test")
	underTest.Send(goctx.Background(), &Mail{
		To:          []string{"test"},
		Bcc:         []string{"test"},
		Subject:     "test",
		Message:     "test",
		ContentType: "text/html",
	})
}
