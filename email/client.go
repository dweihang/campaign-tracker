package email

import (
	"encoding/base64"
	"fmt"
	"io"
	"m800-campaign-tracker/errors"
	"net"
	"net/smtp"
	"time"

	"gitlab.com/cake/goctx"
	"gitlab.com/cake/gopkg"
	"gitlab.com/cake/m800log"
)

type Client interface {
	Ping() gopkg.CodeError
	Send(ctx goctx.Context, mail *Mail) gopkg.CodeError
}

type client struct {
	host string
}

type smtpClient interface {
	Mail(from string) error
	Rcpt(to string) error
	Data() (io.WriteCloser, error)
	Close() error
	Noop() error
	Quit() error
}

var newSMTPClient = func(addr string) (smtpClient, error) {
	conn, err := net.DialTimeout("tcp", addr, 10*time.Second)
	if err != nil {
		return nil, err
	}

	// Connect to the SMTP server
	host, _, _ := net.SplitHostPort(addr)
	return smtp.NewClient(conn, host)
}

type Mail struct {
	To          []string
	Bcc         []string
	Sender      string
	Subject     string
	Message     string
	ContentType string
}

func NewMail(to, bcc []string,
	subject string,
	message string, contentType string) *Mail {
	return &Mail{
		To:          to,
		Bcc:         bcc,
		Subject:     subject,
		Message:     message,
		ContentType: contentType,
	}
}

func NewEmailClient(smtpHost string) Client {
	return &client{
		host: smtpHost,
	}
}

func (e *client) Ping() gopkg.CodeError {
	if cli, err := newSMTPClient(e.host); err != nil {
		return gopkg.NewWrappedCodeError(errors.EmailSMTPServerNotAvailable,
			fmt.Sprintf("Unable to check connection with SMTP server, %+v", err), err)
	} else {
		cli.Quit()
	}
	return nil
}

func (e *client) Send(ctx goctx.Context, mail *Mail) gopkg.CodeError {
	cli, err := newSMTPClient(e.host)
	if err != nil {
		return gopkg.NewWrappedCodeError(errors.EmailSMTPServerNotAvailable,
			fmt.Sprintf("Unable to check connection with SMTP server, %+v", err), err)
	}
	defer cli.Quit()

	if len(mail.To) < 1 {
		m800log.Errorf(ctx, "Recipient addresses not found, please add one or more addresses")
		return gopkg.NewCodeError(errors.EmailNoRecipients, "Recipient addresses not found")
	}

	if err := cli.Mail(mail.Sender); err != nil {
		m800log.Errorf(ctx, "Unable to provide mail transaction: %s, %+v", mail.Sender, err)
		return gopkg.NewWrappedCodeError(errors.EmailStartTransactionError,
			fmt.Sprintf("Unable to start mail transaction, %+v", err), err)
	}

	var recipients string
	for i, recipient := range mail.To {
		if err := cli.Rcpt(recipient); err != nil {
			m800log.Errorf(ctx, "Error during adding recipient: %s, %+v", recipient, err)
			return gopkg.NewWrappedCodeError(errors.EmailRcptAddError,
				fmt.Sprintf("Unable to add one or more recipients, %+v", err), err)
		}

		if i == 0 {
			recipients = recipient
		} else {
			recipients = recipients + ", " + recipient
		}
	}

	if mail.Bcc != nil && len(mail.Bcc) > 0 {
		for _, recipient := range mail.Bcc {
			if err := cli.Rcpt(recipient); err != nil {
				m800log.Errorf(ctx, "Error during adding bcc recipient: %s, %+v", recipient, err)
				return gopkg.NewWrappedCodeError(errors.EmailRcptAddError,
					fmt.Sprintf("Unable to add one or more bcc recipients, %+v", err), err)

			}
		}
	}

	wc, err := cli.Data()
	if err != nil {
		m800log.Errorf(ctx, "Unable to init data writer: %+v", err)
		return gopkg.NewWrappedCodeError(errors.EmailStartDataWriterError,
			fmt.Sprintf("Data writer not initialized, %+v", err), err)
	}

	msg := "To: " + recipients + "\r\n" +
		"From: " + mail.Sender + "\r\n" +
		"Subject: " + mail.Subject + "\r\n" +
		"Content-Type: " + mail.ContentType + "; charset=\"UTF-8\"\r\n" +
		"Content-Transfer-Encoding: base64\r\n" +
		"\r\n" + base64.StdEncoding.EncodeToString([]byte(mail.Message))

	_, err = wc.Write([]byte(msg))
	if err != nil {
		m800log.Errorf(ctx, "Error mail message writing: %+v", err)
		return gopkg.NewWrappedCodeError(errors.EmailWriteBodyError,
			fmt.Sprintf("Mail message writing failed, %+v", err), err)
	}

	err = wc.Close()
	if err != nil {
		m800log.Errorf(ctx, "Close mail transaction error: %+v", err)
		return gopkg.NewWrappedCodeError(errors.EmailEndTransactionError,
			fmt.Sprintf("Template execution failed, %+v", err), err)
	}

	return nil

}
