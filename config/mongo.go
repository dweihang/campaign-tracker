package config

import (
	"errors"
	"github.com/spf13/viper"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readconcern"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"go.mongodb.org/mongo-driver/tag"
	"log"
	"net"
	"strconv"
	"strings"
	"time"
)

const (
	settingsPath = "local.mongo"

	serversAddressesPath = settingsPath + ".servers"

	authMechanismPath = settingsPath + ".auth.auth-mechanism"
	authUsernamePath  = settingsPath + ".auth.username"
	authPasswordPath  = settingsPath + ".auth.password"
	authSourcedPath   = settingsPath + ".auth.auth-source"

	poolMaxSizePath  = settingsPath + ".pool.max-size"
	poolConnIdleTime = settingsPath + ".pool.max-connection-idle-time"

	socketConnectTimeoutPath = settingsPath + ".socket.connect-timeout"
	socketTimeoutPath        = settingsPath + ".socket.timeout"

	replicaSetPath = settingsPath + ".replica-set"

	readConcernPath = settingsPath + ".read.concern"

	readPreferencePath                 = settingsPath + ".read.preference"
	readPreferenceModePath             = readPreferencePath + ".mode"
	readPreferenceTagsPath             = readPreferencePath + ".tags"
	readPreferenceWithMaxStalenessPath = readPreferencePath + ".max-staleness"
)

var LocalMongo *mongoConf

func init() {
	addInitHook(func() {
		LocalMongo = &mongoConf{
			Server: serverConfig{
				Addresses: viper.GetString(serversAddressesPath),
			},
			Auth: authConfig{
				Username:  viper.GetString(authUsernamePath),
				Password:  viper.GetString(authPasswordPath),
				Mechanism: viper.GetString(authMechanismPath),
				Sources:   viper.GetString(authSourcedPath),
			},
			Connection: &connectionConfig{
				PoolMaxSize:          viper.GetUint64(poolMaxSizePath),
				PoolConnIdleTime:     viper.GetInt64(poolConnIdleTime),
				SocketTimeout:        viper.GetInt64(socketTimeoutPath),
				SocketConnectTimeout: viper.GetInt64(socketConnectTimeoutPath),
				ReplicaSet:           viper.GetString(replicaSetPath),
			},
			ReadConcern: &readConcern{
				ReadConcern: viper.GetString(readConcernPath),
			},
			ReadPreference: &readPreference{
				Mode:         viper.GetString(readPreferenceModePath),
				Tags:         viper.GetString(readPreferenceTagsPath),
				MaxStaleness: viper.GetInt64(readPreferenceWithMaxStalenessPath),
			},
		}
	})
}

type mongoConf struct {
	Server         serverConfig
	Auth           authConfig
	Connection     *connectionConfig
	ReadConcern    *readConcern
	ReadPreference *readPreference
}

type serverConfig struct {
	Addresses string
}

type authConfig struct {
	Username  string
	Password  string
	Mechanism string
	Sources   string
}

type connectionConfig struct {
	PoolMaxSize          uint64
	PoolConnIdleTime     int64
	SocketTimeout        int64
	SocketConnectTimeout int64
	ReplicaSet           string
}

type readPreference struct {
	Mode         string
	Tags         string
	MaxStaleness int64
}

type readConcern struct {
	ReadConcern string
}

func (config *mongoConf) ClientOptions() *options.ClientOptions {
	clientOptions := options.Client()

	hosts := strings.Split(config.Server.Addresses, ",")
	if hosts == nil {
		log.Fatalf("mongo config error, hosts=%s", hosts)
	}
	err := checkConnectionHosts(hosts)
	if err != nil {
		log.Fatalf("mongo config error=%+v", err)
	}
	clientOptions.SetHosts(hosts)

	setAuthCredentials(&config.Auth, clientOptions)

	if config.Connection != nil {
		setConnectionOptions(config.Connection, clientOptions)
	}

	if config.ReadConcern != nil {
		err = setReadConcern(config.ReadConcern, clientOptions)
		if err != nil {
			log.Fatalf("mongo config error, readConcern=%+v", *config.ReadConcern)
		}
	}

	if config.ReadPreference != nil {
		err = setReadPreferences(config.ReadPreference, clientOptions)
		if err != nil {
			log.Fatalf("mongo config error, readPreference=%+v", *config.ReadPreference)
		}
	}
	return clientOptions
}

func checkConnectionHosts(hosts []string) error {
	for _, host := range hosts {
		err := checkPort(host)
		if err != nil {
			return err
		}
	}

	return nil
}

func checkPort(host string) error {
	_, port, err := net.SplitHostPort(host)

	if err != nil {
		if addrError, ok := err.(*net.AddrError); !ok || addrError.Err != "missing port in address" {
			return err
		}
	}

	if port != "" {
		d, err := strconv.Atoi(port)
		if err != nil {
			return errors.New("port must be an integer")
		}
		if d <= 0 || d >= 65536 {
			return errors.New("port must be in the range [1, 65535]")
		}
	}

	return nil
}

func setAuthCredentials(authConfig *authConfig, clientOptions *options.ClientOptions) {
	var username = authConfig.Username
	var password = authConfig.Password

	authMechanism := strings.ToUpper(authConfig.Mechanism)
	authSource := authConfig.Sources

	// AuthMechanism quick validation
	switch authMechanism {
	case "MONGODB-CR":
		fallthrough
	case "SCRAM-SHA-1":
		fallthrough
	case "SCRAM-SHA-256":
		break
	default:
		authMechanism = ""

	}

	var auth = options.Credential{
		AuthMechanism: authMechanism,
		AuthSource:    authSource,
		Username:      username,
		Password:      password,
	}

	clientOptions.SetAuth(auth)
}

func setConnectionOptions(connectionConfig *connectionConfig, clientOptions *options.ClientOptions) {
	poolMaxSize := connectionConfig.PoolMaxSize
	if poolMaxSize > 0 {
		clientOptions.SetMaxPoolSize(connectionConfig.PoolMaxSize)
	}

	connIdleTime := connectionConfig.PoolConnIdleTime
	if connIdleTime > 0 {
		clientOptions.SetMaxConnIdleTime(time.Duration(connIdleTime) * time.Millisecond)
	}

	timeout := connectionConfig.SocketConnectTimeout
	if timeout > 0 {
		clientOptions.SetConnectTimeout(time.Duration(timeout) * time.Millisecond)
	}

	timeout = connectionConfig.SocketTimeout
	if timeout > 0 {
		clientOptions.SetSocketTimeout(time.Duration(timeout) * time.Millisecond)
	}

	replicaSet := connectionConfig.ReplicaSet
	if len(replicaSet) > 0 {
		clientOptions.SetReplicaSet(replicaSet)
	}
}

func setReadConcern(readConfig *readConcern, clientOptions *options.ClientOptions) error {
	level := strings.ToLower(readConfig.ReadConcern)
	if len(level) > 0 {
		switch level {
		case "local":
			clientOptions.SetReadConcern(readconcern.Local())
		case "majority":
			clientOptions.SetReadConcern(readconcern.Majority())
		case "linearizable":
			clientOptions.SetReadConcern(readconcern.Linearizable())
		case "available":
			clientOptions.SetReadConcern(readconcern.Available())
		case "snapshot":
			clientOptions.SetReadConcern(readconcern.Snapshot())
		default:
			return errors.New("unknown ReadConcern for replica sets")
		}
	}

	return nil
}

func setReadPreferences(readPreference *readPreference, clientOptions *options.ClientOptions) error {
	modeString := strings.ToLower(readPreference.Mode)
	if len(modeString) > 0 {
		mode, err := readpref.ModeFromString(modeString)

		if err != nil {
			return err
		}

		opts := make([]readpref.Option, 0, 1)

		tags := viper.GetStringMapString(readPreference.Tags)
		if tags != nil {
			tagSet := tag.NewTagSetFromMap(tags)
			opts = append(opts, readpref.WithTagSets(tagSet))
		}

		maxStalenessInt := readPreference.MaxStaleness
		if maxStalenessInt != 0 {
			maxStaleness := time.Duration(maxStalenessInt) * time.Second
			opts = append(opts, readpref.WithMaxStaleness(maxStaleness))
		}

		readPref, err := readpref.New(mode, opts...)
		if err != nil {
			return err
		}

		clientOptions.SetReadPreference(readPref)
	}

	return nil
}
