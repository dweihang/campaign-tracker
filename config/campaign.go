package config

import (
	"github.com/spf13/viper"
	"strings"
)

type campaignConf struct {
	TrackingIdLength int
	ExcludePaths     string
	ExcludePathMap   map[string]*struct{}
}

var placeHolder = &struct{}{}

func resolveExcludedPathMap(c *campaignConf) {
	ret := make(map[string]*struct{})
	arr := strings.Split(c.ExcludePaths, ",")
	for _, excludedPath := range arr {
		ret[strings.TrimSpace(excludedPath)] = &struct{}{}
	}
	c.ExcludePathMap = ret
}

var Campaign *campaignConf

func init() {
	addInitHook(func() {
		Campaign = &campaignConf{
			TrackingIdLength: viper.GetInt("campaign.tracking-id-length"),
			ExcludePaths:     viper.GetString("campaign.exclude-http-paths"),
		}
		resolveExcludedPathMap(Campaign)
	})
}
