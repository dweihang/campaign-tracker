package config

import (
	"github.com/spf13/viper"
	"log"
	"strings"
)

type initHook func()

var initHooks []initHook

func Init(configFile string) {
	viper.SetConfigFile(configFile)
	replacer := strings.NewReplacer(".", "_")
	viper.SetEnvKeyReplacer(replacer)
	viper.AutomaticEnv()

	if err := viper.ReadInConfig(); err != nil {
		log.Fatalf("Failed to read config with err=%+v", err)
	}

	for _, hook := range initHooks {
		hook()
	}

	// release the memory
	initHooks = nil
}

func addInitHook(hookFunc initHook) {
	initHooks = append(initHooks, hookFunc)
}