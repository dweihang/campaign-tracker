package config

import (
	"github.com/spf13/viper"
	"time"
)

type jiraConf struct {
	Url               string
	HttpClientTimeout time.Duration
	Username          string
	Password          string
}

var Jira *jiraConf

func init() {
	addInitHook(func() {
		Jira = &jiraConf{
			Url:               viper.GetString("jira.url"),
			HttpClientTimeout: viper.GetDuration("jira.http-timeout"),
			Username:          viper.GetString("jira.username"),
			Password:          viper.GetString("jira.password"),
		}
	})
}
