package config

import "github.com/spf13/viper"

type emailConf struct {
	Host string
}

var Email *emailConf

func init() {
	addInitHook(func() {
		Email = &emailConf{
			Host: viper.GetString("email.host"),
		}
	})
}
