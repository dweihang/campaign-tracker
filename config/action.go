package config

import (
	"m800-campaign-tracker/concurrent"
	"github.com/spf13/viper"
	"log"
)

type actionConf struct {
	ExecutorConf actionExecutorConf
	Executor concurrent.TaskExecutor
}

type actionExecutorConf struct {
	Type      string
	PoolSize  int
	QueueSize int
}

var ActionEmailWithReminder *actionConf
var ActionJIRA *actionConf
var ActionRedirect *actionConf
var ActionGeneratePng *actionConf
var ActionMongo *actionConf

func init() {
	addInitHook(func() {
		ActionEmailWithReminder = &actionConf{
			ExecutorConf: actionExecutorConf{
				Type:      viper.GetString("action.email-with-reminder.executor.type"),
				PoolSize:  viper.GetInt("action.email-with-reminder.executor.pool-size"),
				QueueSize: viper.GetInt("action.email-with-reminder.executor.queue-size"),
			}}
		getExecutor(ActionEmailWithReminder)
		ActionJIRA = &actionConf{
			ExecutorConf: actionExecutorConf{
				Type:      viper.GetString("action.jira.executor.type"),
				PoolSize:  viper.GetInt("action.jira.executor.pool-size"),
				QueueSize: viper.GetInt("action.jira.executor.queue-size"),
			},
		}
		getExecutor(ActionJIRA)
		ActionRedirect = &actionConf{
			ExecutorConf: actionExecutorConf{
				Type:      viper.GetString("action.redirect.executor.type"),
				PoolSize:  viper.GetInt("action.redirect.executor.pool-size"),
				QueueSize: viper.GetInt("action.redirect.executor.queue-size"),
			}}
		getExecutor(ActionRedirect)
		ActionMongo = &actionConf{
			ExecutorConf: actionExecutorConf{
				Type:      viper.GetString("action.mongo.executor.type"),
				PoolSize:  viper.GetInt("action.mongo.executor.pool-size"),
				QueueSize: viper.GetInt("action.mongo.executor.queue-size"),
			}}
		getExecutor(ActionMongo)
		ActionGeneratePng = &actionConf{
			ExecutorConf:actionExecutorConf{},
			Executor:concurrent.NewAsyncExecutor(),
		}
	})
}

func getExecutor(conf *actionConf) {
	execConf := conf.ExecutorConf
	switch execConf.Type {
	case "pool":
		if execConf.PoolSize <= 0 || execConf.QueueSize <= 0 {
			log.Fatalf("invalid config %+v", *conf)
		} else {
			conf.Executor = concurrent.NewPoolExecutor(execConf.PoolSize, execConf.QueueSize)
		}
	case "immediate":
		conf.Executor = concurrent.NewImmediateExecutor()
	case "async":
		conf.Executor = concurrent.NewAsyncExecutor()
	default:
		log.Fatalf("invalid config %+v", *conf)
	}
}