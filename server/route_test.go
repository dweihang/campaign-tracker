package server

import (
	"github.com/gin-gonic/gin"
	"github.com/stretchr/testify/assert"
	"net/http"
	"testing"
)

func TestRouteGroup_Delete(t *testing.T) {
	underTest := NewRouteGroup("/test")
	underTest.Delete("delete").To(func(context *gin.Context) {})
	assert.Equal(t, 1, len(underTest.getRoutes()))
	assert.Equal(t, underTest.routes[0].httpPath, "/test/delete")
	assert.Equal(t, underTest.routes[0].httpMethod, http.MethodDelete)
	assert.Equal(t, len(underTest.routes[0].handlers), 1)
}

func TestRouteGroup_DeleteAbsolutePath(t *testing.T) {
	underTest := NewRouteGroup("/test")
	underTest.DeleteAbsolutePath("/delete").To(func(context *gin.Context) {})
	assert.Equal(t, 1, len(underTest.getRoutes()))
	assert.Equal(t, underTest.routes[0].httpPath, "/delete")
	assert.Equal(t, underTest.routes[0].httpMethod, http.MethodDelete)
	assert.Equal(t, len(underTest.routes[0].handlers), 1)
}

func TestRouteGroup_Patch(t *testing.T) {
	underTest := NewRouteGroup("/test")
	underTest.Patch("patch").To(func(context *gin.Context) {})
	assert.Equal(t, 1, len(underTest.getRoutes()))
	assert.Equal(t, underTest.routes[0].httpPath, "/test/patch")
	assert.Equal(t, underTest.routes[0].httpMethod, http.MethodPatch)
	assert.Equal(t, len(underTest.routes[0].handlers), 1)
}

func TestRouteGroup_PatchAbsolutePath(t *testing.T) {
	underTest := NewRouteGroup("/test")
	underTest.PatchAbsolutePath("/patch").To(func(context *gin.Context) {})
	assert.Equal(t, 1, len(underTest.getRoutes()))
	assert.Equal(t, underTest.routes[0].httpPath, "/patch")
	assert.Equal(t, underTest.routes[0].httpMethod, http.MethodPatch)
	assert.Equal(t, len(underTest.routes[0].handlers), 1)
}

func TestRouteGroup_Put(t *testing.T) {
	underTest := NewRouteGroup("/test")
	underTest.Put("put").To(func(context *gin.Context) {})
	assert.Equal(t, 1, len(underTest.getRoutes()))
	assert.Equal(t, underTest.routes[0].httpPath, "/test/put")
	assert.Equal(t, underTest.routes[0].httpMethod, http.MethodPut)
	assert.Equal(t, len(underTest.routes[0].handlers), 1)
}

func TestRouteGroup_PutAbsolutePath(t *testing.T) {
	underTest := NewRouteGroup("/test")
	underTest.PutAbsolutePath("/put").To(func(context *gin.Context) {})
	assert.Equal(t, 1, len(underTest.getRoutes()))
	assert.Equal(t, underTest.routes[0].httpPath, "/put")
	assert.Equal(t, underTest.routes[0].httpMethod, http.MethodPut)
	assert.Equal(t, len(underTest.routes[0].handlers), 1)
}

func TestRouteGroup_Get(t *testing.T) {
	underTest := NewRouteGroup("/test")
	pathParam := 1
	underTest.Get("get/%d", pathParam).To(func(context *gin.Context) {})
	assert.Equal(t, 1, len(underTest.getRoutes()))
	assert.Equal(t, underTest.routes[0].httpPath, "/test/get/1")
	assert.Equal(t, underTest.routes[0].httpMethod, http.MethodGet)
	assert.Equal(t, len(underTest.routes[0].handlers), 1)
}

func TestRouteGroup_GetAbsolutePath(t *testing.T) {
	underTest := NewRouteGroup("/test")
	underTest.GetAbsolutePath("/get").To(func(context *gin.Context) {})
	assert.Equal(t, 1, len(underTest.getRoutes()))
	assert.Equal(t, underTest.routes[0].httpPath, "/get")
	assert.Equal(t, underTest.routes[0].httpMethod, http.MethodGet)
	assert.Equal(t, len(underTest.routes[0].handlers), 1)
}

func TestRouteGroup_Post(t *testing.T) {
	underTest := NewRouteGroup("/test")
	pathParam := "param"
	underTest.Post("post/:%s", pathParam).To(func(context *gin.Context) {})
	assert.Equal(t, 1, len(underTest.getRoutes()))
	assert.Equal(t, underTest.routes[0].httpPath, "/test/post/:param")
	assert.Equal(t, underTest.routes[0].httpMethod, http.MethodPost)
	assert.Equal(t, len(underTest.routes[0].handlers), 1)
}

func TestRouteGroup_PostAbsolutePath(t *testing.T) {
	underTest := NewRouteGroup("/test")
	underTest.PostAbsolutePath("/post").To(func(context *gin.Context) {})
	assert.Equal(t, 1, len(underTest.getRoutes()))
	assert.Equal(t, underTest.routes[0].httpPath, "/post")
	assert.Equal(t, underTest.routes[0].httpMethod, http.MethodPost)
	assert.Equal(t, len(underTest.routes[0].handlers), 1)
}
