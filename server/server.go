package server

import (
	"context"
	"fmt"
	"github.com/getkin/kin-openapi/openapi3filter"
	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
	"log"
	"net/http"
	"strings"
	"time"
)

var httpServer *http.Server
var config *HttpServerConfig
var router *openapi3filter.Router

const (
	serviceMode = "service.mode"

	httpPort         = "http.port"
	httpReadTimeout  = "http.read-timeout"
	httpWriteTimeout = "http.write-timeout"
)

type HttpServerConfig struct {
	Mode         string
	Port         string
	ReadTimeout  time.Duration
	WriteTimeout time.Duration
	Middlewares  []gin.HandlerFunc
}

func InitServerWithConfig(serverConfig *HttpServerConfig, apis []API) (router *gin.Engine) {
	return initHttpServer(serverConfig, apis...)
}

func InitServerWithMiddleware(middlewares []gin.HandlerFunc, apis []API) (router *gin.Engine) {
	serverConfig := getConfFromViper()
	serverConfig.Middlewares = middlewares

	return InitServerWithConfig(serverConfig, apis)
}

func InitServer(apis []API) (router *gin.Engine) {
	return InitServerWithConfig(getConfFromViper(), apis)
}

func getConfFromViper() *HttpServerConfig {
	return &HttpServerConfig{
		Mode:         viper.GetString(serviceMode),
		Port:         viper.GetString(httpPort),
		ReadTimeout:  viper.GetDuration(httpReadTimeout),
		WriteTimeout: viper.GetDuration(httpWriteTimeout),
	}
}

func initHttpServer(serverConfig *HttpServerConfig, apis ...API) (router *gin.Engine) {
	switch serverConfig.Mode {
	case gin.DebugMode:
		gin.SetMode(gin.DebugMode)
	default:
		gin.SetMode(gin.ReleaseMode)
	}

	router = gin.New()
	router.Use(serverConfig.Middlewares...)
	router.Use(gin.Recovery())

	for _, api := range apis {
		rg := api.RouteGroup().getRoutes()
		for _, r := range rg {
			router.Handle(strings.ToUpper(r.httpMethod), r.httpPath, r.handlers...)
		}
	}

	_ = openapi3filter.NewRouter().WithSwagger(&DocRoot)
	b, _ := DocRoot.MarshalJSON()
	log.Println(fmt.Sprintf("openapischema: %s", string(b)))

	hs := &http.Server{
		Addr:         ":" + serverConfig.Port,
		Handler:      router,
		ReadTimeout:  serverConfig.ReadTimeout,
		WriteTimeout: serverConfig.WriteTimeout,
	}

	httpServer = hs
	config = serverConfig
	return
}

func Listen() {
	go func() {
		log.Printf("Server running at port %s", config.Port)
		if err := httpServer.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()
}

func ShutdownServer() {
	log.Println("Server shutting down...")

	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := httpServer.Shutdown(ctx); err != nil {
		log.Fatal("Http server could not shutdown gracefully", err)
	}
	log.Println("Server exiting")
}
