package server

import (
	"encoding/json"
	"fmt"
	"github.com/stretchr/testify/assert"
	"strings"
	"testing"
)

func TestDocHeader(t *testing.T) {
	testStruct := struct {
		TestString  string `json:"str" doc:"required desc(test()) pattern(^[A-Z](.*))"`
		TestString2 string `json:"str2" doc:"required desc(test) pattern(^[A-Z].+)"`
		TestMap map[string]interface{} `json:"map" doc:"required desc(service limit)"`
	}{
		TestString:  "test",
		TestString2: "t2",
		TestMap: map[string]interface{}{
			"number":10,
			"string":"20",
			"bool":true,
		},
	}
	sch := extractSchema(testStruct)
	b, err := sch.MarshalJSON()
	fmt.Println(string(b))
	assert.Nil(t, err)
	assert.NotNil(t, b)
	m := make(map[string]interface{})
	err = json.Unmarshal(b, &m)
	assert.Nil(t, err)
	mapVal := func(mapDat map[string]interface{}, key string) interface{} {
		m := mapDat
		depths := strings.Split(key, ".")
		var k string
		for i, dep := range depths {
			if i == len(depths)-1 {
				k = dep
				break
			} else {
				m = m[dep].(map[string]interface{})
			}
		}
		return m[k]
	}
	assert.Equal(t, "test", mapVal(m, "properties.str.example"))
	assert.Equal(t, "string", mapVal(m, "properties.str.type"))
	assert.Equal(t, "test()", mapVal(m, "properties.str.description"))
	assert.Equal(t, "^[A-Z](.*)", mapVal(m, "properties.str.pattern"))
	assert.Equal(t, "t2", mapVal(m, "properties.str2.example"))
	assert.Equal(t, "string", mapVal(m, "properties.str2.type"))
	assert.Equal(t, "test", mapVal(m, "properties.str2.description"))
	assert.Equal(t, "^[A-Z].+", mapVal(m, "properties.str2.pattern"))
	required := mapVal(m, "required").([]interface{})
	assert.Equal(t, "str", required[0])
}

type TypeString string

func TestExtractString(t *testing.T) {
	t1 := "test"
	out := extractString(t1)
	assert.NotNil(t, out)
	assert.Equal(t, out.Value.Interface(), "test")

	t2 := &t1
	out2 := extractString(t2)
	assert.NotNil(t, out2)
	assert.Equal(t, out2.Value.Interface(), "test")

	var t3 TypeString = "test"
	out3 := extractString(t3)
	assert.NotNil(t, out3)
	assert.Equal(t, out3.Value.Interface(), TypeString("test"))
}

type TypeInt int

func TestExtractInteger(t *testing.T) {
	var t1 int64 = 1
	out := extractInteger(t1)
	assert.NotNil(t, out)
	assert.Equal(t, out.Value.Interface(), int64(1))

	t2 := &t1
	out2 := extractInteger(t2)
	assert.NotNil(t, out2)
	assert.Equal(t, out2.Value.Interface(), int64(1))

	var t3 TypeInt = 1
	out3 := extractInteger(t3)
	assert.NotNil(t, out3)
	assert.Equal(t, out3.Value.Interface(), TypeInt(1))
}

type TypeBool bool

func TestExtractBoolean(t *testing.T) {
	t1 := true
	out := extractBoolean(t1)
	assert.NotNil(t, out)
	assert.Equal(t, out.Value.Interface(), true)

	t2 := &t1
	out2 := extractBoolean(t2)
	assert.NotNil(t, out2)
	assert.Equal(t, out2.Value.Interface(), true)

	var t3 TypeBool = true
	out3 := extractBoolean(t3)
	assert.NotNil(t, out3)
	assert.Equal(t, out3.Value.Interface(), TypeBool(true))
}

func TestExtractStruct(t *testing.T) {
	t1 := struct {
		Test string
	}{}
	out := extractStruct(t1)
	assert.NotNil(t, out)

	t2 := &t1
	out2 := extractStruct(t2)
	assert.NotNil(t, out2)
}
