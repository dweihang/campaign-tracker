package server

import (
	"github.com/gin-gonic/gin"
	"github.com/spf13/viper"
	"net/http"
	"testing"
	"time"
)

type testAPI struct {
}

func (t *testAPI) RouteGroup() *RouteGroup {
	rg := NewRouteGroup("/test")
	rg.Get("/get").To(t.OK)
	return rg
}

func NewTestAPI() API {
	return &testAPI{}
}

func (t *testAPI) OK(ctx *gin.Context) {
	ctx.AbortWithStatus(http.StatusOK)
}

func TestInitServer(t *testing.T) {
	viper.Set(serviceMode, gin.DebugMode)
	viper.Set(httpPort, 35332)
	viper.Set(httpReadTimeout, 1000)
	viper.Set(httpWriteTimeout, 1000)
	InitServer([]API{NewTestAPI()})
	ShutdownServer()
}

func TestInitServerWithConfig(t *testing.T) {
	viper.Set(serviceMode, gin.ReleaseMode)
	viper.Set(httpPort, 35332)
	viper.Set(httpReadTimeout, 1000)
	viper.Set(httpWriteTimeout, 1000)
	InitServerWithConfig(getConfFromViper(), []API{NewTestAPI()})
	time.Sleep(time.Duration(2) * time.Second)
	ShutdownServer()
}

func TestInitServerWithMiddleware(t *testing.T) {
	viper.Set(serviceMode, gin.TestMode)
	viper.Set(httpPort, 35332)
	viper.Set(httpReadTimeout, 1000)
	viper.Set(httpWriteTimeout, 1000)
	InitServerWithMiddleware(nil, []API{NewTestAPI()})
	time.Sleep(time.Duration(2) * time.Second)
	ShutdownServer()
}
