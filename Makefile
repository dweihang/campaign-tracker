APP=m800-campaign-tracker
SKAFFOLD_INT_CONF=devops/int/skaffold.yaml
SKAFFOLD_DEV_CONF=devops/dev/skaffold.yaml
PWD=$(shell pwd)
PORT=$(shell head -10 resources/http.toml | grep port | cut -d'=' -f 2 |tr -d '[:space:]'| tr -d '"')
SOURCE=./...
GOPATH=$(shell env | grep GOPATH | cut -d'=' -f 2)

initrun:
	go run ./

update:
	git pull

build:
	GO111MODULE=on go build -mod=vendor

test:
	GO111MODULE=on go test -mod=vendor -race -cover ./...

dockertest:
	docker run -ti --rm -v $(shell pwd):/$(shell basename $(shell pwd)) -w="/$(shell basename $(shell pwd))" --network host  artifactory.devops.maaii.com/lc-docker-local/golang:1.11.6-kafka "go" "test" "-mod=vendor" "-race" "-cover" "./..."

run: build
	./$(APP)

clean:
	rm -rf bin pkg

modvendor:
	GO111MODULE=on go build -v $(SOURCE)
	GO111MODULE=on go mod tidy
	GO111MODULE=on go mod vendor

docker:
	docker build -t $(APP) -f devops/Dockerfile .
	docker run -p $(PORT):$(PORT) $(APP):latest

skbuild:
	@echo "Start skaffold build..."
	skaffold build -f $(SKAFFOLD_DEV_CONF)

skrun:
	@echo "Start skaffold build..."
	skaffold run -f $(SKAFFOLD_DEV_CONF)

dev:
	@echo "Start skaffold build..."
	skaffold run -f $(SKAFFOLD_DEV_CONF)

int:
	@echo "Start skaffold build..."
	skaffold run -f $(SKAFFOLD_INT_CONF)

skdelete:
	@echo "Start skaffold build..."
	skaffold delete -f $(SKAFFOLD_DEV_CONF)

