package trace

import (
	"net/http"
	"net/http/httputil"
	"sync"
)

var DefaultHTTPTracer = NewHTTPTracer()

type Config struct {
	Enable              bool
	DumpIncomingRequest bool
}

type HTTPTraceInfo struct {
	Key                               string
	IncomingRequest                   []byte
	OutBoundData                      []RequestResponsePair
	IncomingRequestResponseHttpStatus int
}

type RequestResponsePair struct {
	Request         []byte
	Response        []byte
	TimeElapsedNano int64
}

type HTTPTracer interface {
	RegisterTrace(traceKey string, req *http.Request) *HTTPTraceInfo
	GetTrace(traceKey string) *HTTPTraceInfo
	AppendOutboundHTTPData(trace *HTTPTraceInfo, request []byte, response []byte, TimeElapsedNano int64)
	ReleaseTrace(traceKey string)
	UpdateConfig(conf Config)
}

func NewHTTPTracer() HTTPTracer {
	var m sync.Map
	return &httpTracer{keyTraceInfoMap: m, conf: Config{Enable: true, DumpIncomingRequest: true}}
}

type httpTracer struct {
	keyTraceInfoMap sync.Map
	conf            Config
}

func (h *httpTracer) UpdateConfig(conf Config) {
	h.conf.Enable = conf.Enable
	h.conf.DumpIncomingRequest = conf.DumpIncomingRequest
}

func (h *httpTracer) AppendOutboundHTTPData(trace *HTTPTraceInfo, request []byte, response []byte, timeElapsedNano int64) {
	trace.OutBoundData = append(trace.OutBoundData, RequestResponsePair{
		Request:         request,
		Response:        response,
		TimeElapsedNano: timeElapsedNano,
	})
}

func (h *httpTracer) RegisterTrace(traceKey string, req *http.Request) *HTTPTraceInfo {
	if !h.conf.Enable {
		return nil
	}
	ret := &HTTPTraceInfo{
		Key: traceKey,
	}
	if h.conf.DumpIncomingRequest {
		ret.IncomingRequest, _ = httputil.DumpRequest(req, true)
	}
	h.keyTraceInfoMap.Store(traceKey, ret)
	return ret
}

func (h *httpTracer) ReleaseTrace(traceKey string) {
	h.keyTraceInfoMap.Delete(traceKey)
}

func (h *httpTracer) GetTrace(traceKey string) *HTTPTraceInfo {
	if val, ok := h.keyTraceInfoMap.Load(traceKey); ok {
		if ret, ok := val.(*HTTPTraceInfo); ok {
			return ret
		}
	}
	return nil
}
