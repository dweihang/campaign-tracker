package trace

type HTTPTraceDisposer interface {
	Dispose(info *HTTPTraceInfo)
}

type HTTPTraceDisposerRunner interface {
	HTTPTraceDisposer
	AddDisposer(disposer HTTPTraceDisposer)
}

type httpTraceDisposerRunner struct {
	inbox     chan *HTTPTraceInfo
	disposers []HTTPTraceDisposer
}

func (h *httpTraceDisposerRunner) AddDisposer(disposer HTTPTraceDisposer) {
	h.disposers = append(h.disposers, disposer)
}

func (h *httpTraceDisposerRunner) RemoveDisposer(disposer HTTPTraceDisposer) {
	index := -1
	for i, d := range h.disposers {
		if disposer == d {
			index = i
		}
	}
	if index >= 0 {
		h.disposers = append(h.disposers[:index], h.disposers[index+1:]...)
	}
}

func NewHttpTraceDisposerRunner() HTTPTraceDisposerRunner {
	ret := &httpTraceDisposerRunner{inbox: make(chan *HTTPTraceInfo)}
	ret.run()
	return ret
}

func (h *httpTraceDisposerRunner) Dispose(info *HTTPTraceInfo) {
	h.inbox <- info
}

func (h *httpTraceDisposerRunner) run() {
	runner := func() {
		for {
			info := <-h.inbox
			h.dispose(info)
		}
	}
	go runner()
}

func (h *httpTraceDisposerRunner) dispose(info *HTTPTraceInfo) {
	for _, d := range h.disposers {
		d.Dispose(info)
	}
}
