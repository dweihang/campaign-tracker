package trace

import (
	"errors"
	"github.com/stretchr/testify/assert"
	"testing"
	"time"
)

func TestHttpTraceDisposer_Dispose(t *testing.T) {
	disposer := NewHttpTraceDisposerRunner()
	runnerChannel := make(chan struct{})
	var err error
	runner := func() {
		disposer.Dispose(&HTTPTraceInfo{
			Key:  "test",
			OutBoundData: nil,
		})
		disposer.Dispose(&HTTPTraceInfo{
			Key:  "test",
			OutBoundData: nil,
		})
		runnerChannel <- struct{}{}
	}
	go runner()
	select {
	case _ = <-runnerChannel:
		err = nil
	case _ = <-time.After(time.Second * 1):
		err = errors.New("timeout")
	}
	assert.Nil(t, err)
}
