package trace

import (
	"github.com/stretchr/testify/assert"
	"net/http"
	"testing"
)

func TestHttpTracer_RegisterTrace(t *testing.T) {
	req, _ := http.NewRequest(http.MethodGet, "http://test.com", nil)
	DefaultHTTPTracer.RegisterTrace("test", req)
	val := DefaultHTTPTracer.GetTrace("test")
	assert.NotNil(t, val)
}

func TestHttpTracer_ReleaseTrace(t *testing.T) {
	req, _ := http.NewRequest(http.MethodGet, "http://test.com", nil)
	DefaultHTTPTracer.RegisterTrace("test", req)
	DefaultHTTPTracer.ReleaseTrace("test")
	val := DefaultHTTPTracer.GetTrace("test")
	assert.Nil(t, val)
}