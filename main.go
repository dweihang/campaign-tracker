package main

import (
	"github.com/gin-gonic/gin"
	"m800-campaign-tracker/action"
	"m800-campaign-tracker/api"
	"m800-campaign-tracker/cache"
	"m800-campaign-tracker/config"
	"m800-campaign-tracker/dbaccess"
	"m800-campaign-tracker/dbaccess/local/dbop"
	"m800-campaign-tracker/email"
	"m800-campaign-tracker/metrics"
	"m800-campaign-tracker/server"
	"os"
	"os/signal"
)

func main() {
	config.Init("./resources/config.toml")
	metrics.Init()
	dbaccess.Init()
	cache.ReloadInMemoryCache()
	action.Init()

	emailCli := email.NewEmailClient(config.Email.Host)
	emailReminderDbOp := dbop.NewEmailReminder()
	emailTemplateDbOp := dbop.NewEmailTemplate()

	var apis []server.API
	apis = append(apis, api.NewHealthAPI(emailCli))
	apis = append(apis, api.NewEmailTemplateAPI(emailTemplateDbOp))
	apis = append(apis, api.NewCampaignEventAPI(emailReminderDbOp,action.NewResolver()))
	apis = append(apis, api.NewCronAPI(emailReminderDbOp, emailTemplateDbOp, emailCli))
	apis = append(apis, api.NewCacheAPI())
	entryAPi := api.NewCampaignEntryAPI(action.NewResolver())
	_ = entryAPi.RouteGroup()

	r := server.InitServerWithMiddleware([]gin.HandlerFunc{entryAPi.MiddleWare()}, apis)
	metrics.Use(r)
	server.Listen()

	// Block until signal to shutdown has been received
	quit := make(chan os.Signal, 1)
	// Only receive interrupts in the channel
	signal.Notify(quit, os.Interrupt)

	defer func() {
		server.ShutdownServer()
	}()

	// Wait for signal to quit
	<-quit
}
