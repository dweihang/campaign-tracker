package action

import (
	"bytes"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"gitlab.com/cake/gopkg"
	"m800-campaign-tracker/cache"
	"m800-campaign-tracker/concurrent"
	"m800-campaign-tracker/dbaccess/local/model"
	"m800-campaign-tracker/errors"
	"m800-campaign-tracker/util"
	"text/template"
)

func NewResolver() Resolver {
	return &resolver{}
}

type Resolver interface {
	Resolve(campaign *model.MarketingCampaign, eventName string, ctx *gin.Context, respWrittenChannel chan struct{}) ([]Action, *concurrent.Execution, gopkg.CodeError)
}
type resolver struct {
}

func (ar *resolver) Resolve(campaign *model.MarketingCampaign, trackerName string, ginCtx *gin.Context, respWrittenChannel chan struct{}) ([]Action, *concurrent.Execution, gopkg.CodeError) {
	if tracker := getTrackerByName(campaign, trackerName); tracker == nil {
		return nil, nil, gopkg.NewCodeError(errors.CampaignUnknownTracker, "unknown event name")
	} else {
		var acts []Action
		iObj := util.GetInternalObjectFromGin(ginCtx)
		seqMaps := make(map[int][]Action)
		for _, a := range tracker.Actions {
			byteBody, err := json.Marshal(a.Params)
			if err != nil {
				return nil, nil, gopkg.NewWrappedCodeError(errors.JsonMarshallingFailed, "cannot marshal obj", err)
			}

			switch a.Type {
			case NameCloseJiraTicket:
				out, err := fillInParams(iObj.GetEvent(), a.Type, string(byteBody), ginCtx)
				if err != nil {
					return nil, nil, err
				}
				p := &ParamCloseJiraTicket{}
				if err := json.Unmarshal(out, p); err != nil {
					return nil, nil, gopkg.NewWrappedCodeError(errors.JsonUnmarshallingFailed, "failed to unmarshall action", err)
				} else {
					act := NewActionCloseJiraTicket(p, a.ActionId)
					acts = append(acts, act)
					if seqActs := seqMaps[a.Sequence]; len(seqActs) == 0 {
						seqActs = []Action{act}
						seqMaps[a.Sequence] = seqActs
					} else {
						seqActs = append(seqActs, act)
						seqMaps[a.Sequence] = seqActs
					}
				}
			case NameCreateJiraTicketComment:
				out, err := fillInParams(iObj.GetEvent(), a.Type, string(byteBody), ginCtx)
				if err != nil {
					return nil, nil, err
				}
				p := &ParamCreateJiraTicketComment{}
				if err := json.Unmarshal(out, p); err != nil {
					return nil, nil, gopkg.NewWrappedCodeError(errors.JsonUnmarshallingFailed, "failed to unmarshall action", err)
				} else {
					act := NewActionCreateJiraComment(p, a.ActionId)
					acts = append(acts, act)
					if seqActs := seqMaps[a.Sequence]; len(seqActs) == 0 {
						seqActs = []Action{act}
						seqMaps[a.Sequence] = seqActs
					} else {
						seqActs = append(seqActs, act)
						seqMaps[a.Sequence] = seqActs
					}
				}
			case NameCreateJiraTicket:
				out, err := fillInParams(iObj.GetEvent(), a.Type, string(byteBody), ginCtx)
				if err != nil {
					return nil, nil, err
				}
				p := &ParamJira{}
				if err := json.Unmarshal(out, p); err != nil {
					return nil, nil, gopkg.NewWrappedCodeError(errors.JsonUnmarshallingFailed, "failed to unmarshall action", err)
				} else {
					act := NewActionCreateJiraTicket(p, a.ActionId)
					acts = append(acts, act)
					if seqActs := seqMaps[a.Sequence]; len(seqActs) == 0 {
						seqActs = []Action{act}
						seqMaps[a.Sequence] = seqActs
					} else {
						seqActs = append(seqActs, act)
						seqMaps[a.Sequence] = seqActs
					}
				}
			case NameRedirect:
				out, err := fillInParams(iObj.GetEvent(), a.Type, string(byteBody), ginCtx)
				if err != nil {
					return nil, nil, err
				}
				p := &ParamRedirect{}
				if err := json.Unmarshal(out, p); err != nil {
					return nil, nil, gopkg.NewWrappedCodeError(errors.JsonUnmarshallingFailed, "failed to unmarshall action", err)
				} else {
					act := NewActionRedirect(p, a.ActionId)
					acts = append(acts, act)
					if seqActs := seqMaps[a.Sequence]; len(seqActs) == 0 {
						seqActs = []Action{act}
						seqMaps[a.Sequence] = seqActs
					} else {
						seqActs = append(seqActs, act)
						seqMaps[a.Sequence] = seqActs
					}
				}
			case NameSendEmailWithReminder:
				out, err := fillInParams(iObj.GetEvent(), a.Type, string(byteBody), ginCtx)
				if err != nil {
					return nil, nil, err
				}
				p := &ParamSendEmailWithReminder{}
				if err := json.Unmarshal(out, p); err != nil {
					return nil, nil, gopkg.NewWrappedCodeError(errors.JsonUnmarshallingFailed, "failed to unmarshall action", err)
				} else {
					act := NewActionSendEmailWithReminder(p, a.ActionId)
					acts = append(acts, act)
					if seqActs := seqMaps[a.Sequence]; len(seqActs) == 0 {
						seqActs = []Action{act}
						seqMaps[a.Sequence] = seqActs
					} else {
						seqActs = append(seqActs, act)
						seqMaps[a.Sequence] = seqActs
					}
				}
			case NameGenerateTrackingPng:
				act := NewActionGeneratePng(a.ActionId)
				acts = append(acts, act)
				if seqActs := seqMaps[a.Sequence]; len(seqActs) == 0 {
					seqActs = []Action{act}
					seqMaps[a.Sequence] = seqActs
				} else {
					seqActs = append(seqActs, act)
					seqMaps[a.Sequence] = seqActs
				}
			case NameReturnImmediateResponse:
				out, err := fillInParams(iObj.GetEvent(), a.Type, string(byteBody), ginCtx)
				if err != nil {
					return nil, nil, err
				}
				p := &ParamReturnImmediateResponse{}
				if err := json.Unmarshal(out, p); err != nil {
					return nil, nil, gopkg.NewWrappedCodeError(errors.JsonUnmarshallingFailed, "failed to unmarshall action", err)
				} else {
					act := NewActionReturnImmediateResponse(p, a.ActionId)
					acts = append(acts, act)
					if seqActs := seqMaps[a.Sequence]; len(seqActs) == 0 {
						seqActs = []Action{act}
						seqMaps[a.Sequence] = seqActs
					} else {
						seqActs = append(seqActs, act)
						seqMaps[a.Sequence] = seqActs
					}
				}
			case NameSaveEventToMongo:
				out, err := fillInParams(iObj.GetEvent(), a.Type, string(byteBody), ginCtx)
				if err != nil {
					return nil, nil, err
				}
				p := &ParamSaveEventToMongo{}
				if err := json.Unmarshal(out, p); err != nil {
					return nil, nil, gopkg.NewWrappedCodeError(errors.JsonUnmarshallingFailed, "failed to unmarshall action", err)
				} else {
					act := NewActionSaveEventToMongo(p, a.ActionId)
					acts = append(acts, act)
					if seqActs := seqMaps[a.Sequence]; len(seqActs) == 0 {
						seqActs = []Action{act}
						seqMaps[a.Sequence] = seqActs
					} else {
						seqActs = append(seqActs, act)
						seqMaps[a.Sequence] = seqActs
					}
				}
			default:
				return nil, nil, gopkg.NewCodeError(errors.CampaignUnknownTracker, "can not determine event type "+a.Type)
			}

		}

		return acts, resolveOrderedExecution(seqMaps, ginCtx, respWrittenChannel), nil
	}
}

func resolveOrderedExecution(seqMaps map[int][]Action, ginCtx *gin.Context, respWrittenChannel chan struct{}) *concurrent.Execution {
	type orderActions struct {
		order   int
		actions []Action
	}
	var m []orderActions
	for order, acts := range seqMaps {
		if len(m) == 0 {
			m = append(m, orderActions{
				order:   order,
				actions: acts,
			})
		} else {
			insertIdx := 0
			for i := len(m) - 1; i >= 0; i-- {
				if order > m[i].order {
					insertIdx = i + 1
					break
				}
			}
			rear := append([]orderActions{}, m[insertIdx:]...)
			m = append(m[0:insertIdx], orderActions{
				order:   order,
				actions: acts,
			})
			m = append(m, rear...)
		}
	}
	var ret *concurrent.Execution
	for _, ora := range m {
		var tasks []*concurrent.Task
		for _, act := range ora.actions {
			tasks = append(tasks, act.Task(ginCtx, respWrittenChannel))
		}
		if ret == nil {
			ret = concurrent.ExecuteParallel(tasks...)
		} else {
			ret = ret.ExecuteParallel(tasks...)
		}
	}
	return ret
}

func fillInParams(eventName, eventActionType, tmpl string, ginCtx *gin.Context) ([]byte, gopkg.CodeError) {
	k := eventName + eventActionType
	var t *template.Template
	var parseErr error
	if cache.EventActionTemplateCache[k] != nil {
		t = cache.EventActionTemplateCache[k]
	} else {
		t, parseErr = template.New(k).Option("missingkey=zero").Parse(tmpl)
		if parseErr != nil {
			return nil, gopkg.NewCarrierCodeError(errors.CampaignUnknownTracker, "failed to parse action template")
		}
		cache.EventActionTemplateCache[k] = t
	}
	buffer := new(bytes.Buffer)

	fillInObj := map[string]map[string]interface{}{
		"internal": util.GetInternalObjectFromGin(ginCtx),
		"header":   util.GetHeaderAsMapFromGin(ginCtx),
		"query":    util.GetQueryAsMapFromGin(ginCtx),
	}
	if executeTemplateErr := t.Execute(buffer, fillInObj); executeTemplateErr != nil {
		return nil, gopkg.NewWrappedCodeError(errors.EmailCannotParseTemplate, "cannot fill in the email template parameters", executeTemplateErr)
	}
	return buffer.Bytes(), nil
}

func getTrackerByName(campaign *model.MarketingCampaign, trackerName string) *model.Tracker {
	for _, t := range campaign.Trackers {
		if t.Name == trackerName {
			return &t
		}
	}
	return nil
}
