package action

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.com/cake/goctx"
	"gitlab.com/cake/gopkg"
	"gitlab.com/cake/m800log"
	"m800-campaign-tracker/concurrent"
	"m800-campaign-tracker/config"
	"m800-campaign-tracker/dbaccess/local/model"
	"m800-campaign-tracker/errors"
	"m800-campaign-tracker/jira/jiradto"
	"m800-campaign-tracker/util"
)

const NameCreateJiraTicketComment = "createJiraTicketComment"

type createJiraComment struct {
	param *ParamCreateJiraTicketComment
	id    string
}

func NewActionCreateJiraComment(param *ParamCreateJiraTicketComment, id string) Action {
	return &createJiraComment{
		param: param,
		id:    id,
	}
}

func (c *createJiraComment) Validate(ctx *gin.Context) gopkg.CodeError {
	return nil
}

func (c *createJiraComment) Task(ginCtx *gin.Context, respWrittenChannel chan struct{}) *concurrent.Task {
	taskFunc := func(ctx goctx.Context) *concurrent.TaskResult {
		iObj := util.GetInternalObjectFromGin(ginCtx)
		dbObj, err := actionStatusDbOp.Get(ctx, iObj.GetTrackingId(), c.param.CreateJiraTicketActionId)
		if err != nil {
			m800log.Errorf(ctx, "cannot find jira ticket number with trackingId=%s actionId=%s", iObj.GetTrackingId(), c.param.CreateJiraTicketActionId)
			return concurrent.NewTaskResult(nil, nil)
		}
		if dbObj.Status == model.ActionStatusError {
			err = gopkg.NewCarrierCodeError(errors.JIRACreateTicketCommentError, fmt.Sprintf("create ticket before encountered an error with trackingId=%s actionId=%s", iObj.GetTrackingId(), c.param.CreateJiraTicketActionId))
			m800log.Errorf(ctx, "%+v", err)
			return concurrent.NewTaskResult(nil, err)
		}
		param := &jiradto.CreateJiraTicketCommentRequest{
			Body:         c.param.Comment,
			TicketNumber: dbObj.ActionOutputData["id"].(string),
		}
		resp, err := jiraCli.CreateJiraTicketComment(ctx, param)
		if resp != nil {
			_ = actionStatusDbOp.Upsert(ctx, &model.ActionStatus{
				CampaignId:  iObj.GetCampaignId(),
				ActionName:  NameCreateJiraTicketComment,
				ActionId:    c.id,
				TrackerName: iObj.GetEvent(),
				TrackingId:  iObj.GetTrackingId(),
				Status:      model.ActionStatusDone,
				ErrMsg:      nil,
				TriggeredAt: iObj.GetEventTriggerTime(),
			})
		} else if err != nil {
			errMsg := err.FullError()
			_ = actionStatusDbOp.Upsert(ctx, &model.ActionStatus{
				CampaignId:  iObj.GetCampaignId(),
				ActionName:  NameCreateJiraTicketComment,
				ActionId:    c.id,
				TrackerName: iObj.GetEvent(),
				TrackingId:  iObj.GetTrackingId(),
				Status:      model.ActionStatusError,
				ErrMsg:      &errMsg,
				TriggeredAt: iObj.GetEventTriggerTime(),
			})
		}

		return concurrent.NewTaskResult(nil, err)
	}
	return concurrent.NewTask(taskFunc, config.ActionJIRA.Executor)
}

type ParamCreateJiraTicketComment struct {
	CreateJiraTicketActionId string `json:"createJiraTicketActionId" bson:"createJiraTicketActionId"`
	Comment                  string `json:"comment" bson:"comment"`
}
