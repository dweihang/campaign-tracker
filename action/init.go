package action

import (
	"m800-campaign-tracker/config"
	"m800-campaign-tracker/dbaccess/local/dbop"
	"m800-campaign-tracker/email"
	"m800-campaign-tracker/gohttp"
	"m800-campaign-tracker/jira"
	"net/http"
)

var emailCli email.Client
var jiraCli jira.Client
var emailTemplateDbOp dbop.EmailTemplate
var trackerEventDbOp dbop.TrackerEvent
var emailReminderDbOp dbop.EmailReminder
var actionStatusDbOp dbop.ActionStatus

func Init() {
	emailCli = email.NewEmailClient(config.Email.Host)
	jiraCli = jira.NewJIRACli(config.Jira.Url, gohttp.NewClient(gohttp.NewConfig(
		http.Client{Timeout: config.Jira.HttpClientTimeout}, nil)), config.Jira.Username, config.Jira.Password)
	emailTemplateDbOp = dbop.NewEmailTemplate()
	trackerEventDbOp = dbop.NewTrackerEvent()
	emailReminderDbOp = dbop.NewEmailReminder()
	actionStatusDbOp = dbop.NewActionStatus()
}
