package action

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/cake/goctx"
	"gitlab.com/cake/gopkg"
	"m800-campaign-tracker/concurrent"
	"m800-campaign-tracker/config"
	"m800-campaign-tracker/dbaccess/local/model"
	"m800-campaign-tracker/jira/jiradto"
	"m800-campaign-tracker/util"
)

const (
	NameCreateJiraTicket = "createJiraTicket"
)

type actionCreateJiraTicket struct {
	id    string
	param *ParamJira
}

func (a actionCreateJiraTicket) Validate(ctx *gin.Context) gopkg.CodeError {
	return nil
}

func (a actionCreateJiraTicket) Task(ginctx *gin.Context, respWrittenChannel chan struct{}) *concurrent.Task {
	taskFunc := func(ctx goctx.Context) *concurrent.TaskResult {
		param := &jiradto.CreateJiraTicketRequest{
			ProjectKey:   a.param.ProjectKey,
			Summary:      a.param.Summary,
			Labels:       a.param.Labels,
			Description:  a.param.Description,
			Assignee:     a.param.Assignee,
			IssueType:    a.param.IssueType,
			CustomFields: a.param.CustomerFields,
		}
		resp, err := jiraCli.CreateJiraTicket(ctx, param)
		iObj := util.GetInternalObjectFromGin(ginctx)
		if resp != nil {
			_ = actionStatusDbOp.Upsert(ctx, &model.ActionStatus{
				CampaignId:  iObj.GetCampaignId(),
				ActionName:  NameCreateJiraTicket,
				ActionId:    a.id,
				TrackerName: iObj.GetEvent(),
				TrackingId:  iObj.GetTrackingId(),
				Status:      model.ActionStatusDone,
				ErrMsg:      nil,
				TriggeredAt: iObj.GetEventTriggerTime(),
				ActionOutputData: map[string]interface{}{
					"self": resp.Self,
					"id":   resp.Id,
					"key":  resp.Key,
				},
				ActionExtraData: a.param.ActionStatusExtraData,
			})
		} else if err != nil {
			errMsg := err.FullError()
			_ = actionStatusDbOp.Upsert(ctx, &model.ActionStatus{
				CampaignId:  iObj.GetCampaignId(),
				ActionName:  NameCreateJiraTicket,
				ActionId:    a.id,
				TrackerName: iObj.GetEvent(),
				TrackingId:  iObj.GetTrackingId(),
				Status:      model.ActionStatusError,
				ErrMsg:      &errMsg,
				TriggeredAt: iObj.GetEventTriggerTime(),
			})
		}

		return concurrent.NewTaskResult(nil, err)
	}
	return concurrent.NewTask(taskFunc, config.ActionJIRA.Executor)
}

func NewActionCreateJiraTicket(param *ParamJira, id string) Action {
	return &actionCreateJiraTicket{
		id:    id,
		param: param,
	}
}

type ParamJira struct {
	ProjectKey            string                 `json:"projectKey" bson:"projectKey"`
	Summary               string                 `json:"summary" bson:"summary"`
	Assignee              string                 `json:"assignee" bson:"assignee"`
	IssueType             string                 `json:"issueType" bson:"issueType"`
	Description           string                 `json:"description" bson:"description"`
	Labels                []string               `json:"labels,omitempty" bson:"labels,omitempty"`
	CustomerFields        map[string]interface{} `json:"customFields,omitempty" bson:"customerFields,omitempty"`
	ActionStatusExtraData map[string]interface{} `json:"actionStatusExtraData,omitempty" bson:"actionStatusExtraData,omitempty"`
}
