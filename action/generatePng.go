package action

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/cake/goctx"
	"gitlab.com/cake/gopkg"
	"m800-campaign-tracker/concurrent"
	"m800-campaign-tracker/config"
	"net/http"
)

const transPixel = "\x47\x49\x46\x38\x39\x61\x01\x00\x01\x00\x80\x00\x00\x00\x00\x00\x00\x00\x00\x21\xF9\x04\x01\x00\x00\x00\x00\x2C\x00\x00\x00\x00\x01\x00\x01\x00\x00\x02\x02\x44\x01\x00\x3B"

const NameGenerateTrackingPng = "generateTrackingPng"

type actionGeneratePng struct {
	id string
}

func (a actionGeneratePng) Validate(_ *gin.Context) gopkg.CodeError {
	return nil
}

func (a actionGeneratePng) Task(ginctx *gin.Context, respWrittenChannel chan struct{}) *concurrent.Task {
	taskFunc := func(ctx goctx.Context) *concurrent.TaskResult {
		var err gopkg.CodeError

		ginctx.Data(http.StatusOK, "image/png", []byte(transPixel))
		ginctx.Abort()
		respWrittenChannel <- struct{}{}
		return concurrent.NewTaskResult(nil, err)
	}
	return concurrent.NewTask(taskFunc, config.ActionGeneratePng.Executor)
}

func NewActionGeneratePng(actionId string) Action {
	return &actionGeneratePng{
		id: actionId,
	}
}
