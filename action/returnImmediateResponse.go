package action

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/cake/goctx"
	"gitlab.com/cake/gopkg"
	"gitlab.com/cake/m800log"
	"m800-campaign-tracker/api/dto"
	"m800-campaign-tracker/concurrent"
	"net/http"
)

const (
	NameReturnImmediateResponse = "returnImmediateResponse"
)

type returnImmediateResponse struct {
	id    string
	param *ParamReturnImmediateResponse
}

func NewActionReturnImmediateResponse(param *ParamReturnImmediateResponse, id string) *returnImmediateResponse {
	return &returnImmediateResponse{
		param: param,
		id:    id,
	}
}

func (r returnImmediateResponse) Validate(_ *gin.Context) gopkg.CodeError {
	return nil
}

func (r returnImmediateResponse) Task(ginctx *gin.Context, respWrittenChannel chan struct{}) *concurrent.Task {
	taskFunc := func(ctx goctx.Context) *concurrent.TaskResult {
		m800log.Infof(ctx, "send direct response with message=%s", r.param.Message)
		if r.param.StatusCode != nil {
			ginctx.AbortWithStatusJSON(*r.param.StatusCode, dto.NewHttpResponse(ctx, 0, r.param.Message))
		} else {
			ginctx.AbortWithStatusJSON(http.StatusOK, dto.NewHttpResponse(ctx, 0, r.param.Message))
		}
		respWrittenChannel <- struct{}{}
		return concurrent.NewTaskResult(nil, nil)
	}
	return concurrent.NewTask(taskFunc, concurrent.NewAsyncExecutor())
}

type ParamReturnImmediateResponse struct {
	StatusCode *int   `json:"httpStatusCode,omitempty" bson:"httpStatusCode,omitempty"`
	Message    string `json:"message" bson:"message"`
}
