package action

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/cake/goctx"
	"gitlab.com/cake/gopkg"
	"m800-campaign-tracker/concurrent"
	"m800-campaign-tracker/config"
	"net/http"
	"net/url"
)

const (
	NameRedirect = "redirect"
)

type actionRedirect struct {
	id    string
	param *ParamRedirect
}

func (a actionRedirect) Validate(_ *gin.Context) gopkg.CodeError {
	return nil
}

func (a actionRedirect) Task(ginctx *gin.Context, respWrittenChannel chan struct{}) *concurrent.Task {
	taskFunc := func(ctx goctx.Context) *concurrent.TaskResult {
		var err gopkg.CodeError
		uri, _ := url.Parse(a.param.RedirectUrl)

		ginctx.Redirect(http.StatusTemporaryRedirect, uri.String())
		ginctx.Abort()
		respWrittenChannel <- struct{}{}
		return concurrent.NewTaskResult(nil, err)
	}
	return concurrent.NewTask(taskFunc, config.ActionRedirect.Executor)
}

func NewActionRedirect(param *ParamRedirect, id string) Action {
	return &actionRedirect{
		id:    id,
		param: param,
	}
}

type ParamRedirect struct {
	RedirectUrl string `json:"redirectUrl"`
}
