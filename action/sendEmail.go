package action

import (
	"bytes"
	"github.com/gin-gonic/gin"
	"gitlab.com/cake/goctx"
	"gitlab.com/cake/gopkg"
	"gitlab.com/cake/m800log"
	"m800-campaign-tracker/cache"
	"m800-campaign-tracker/concurrent"
	"m800-campaign-tracker/config"
	"m800-campaign-tracker/dbaccess/local/model"
	"m800-campaign-tracker/email"
	"m800-campaign-tracker/errors"
	"m800-campaign-tracker/util"
	"regexp"
	"text/template"
	"time"
)

const (
	NameSendEmailWithReminder = "sendEmailWithReminder"
)

type actionSendEmailWithReminder struct {
	id string
	param *ParamSendEmailWithReminder
}

var EmailValidateRegex = regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")

func (a *actionSendEmailWithReminder) Validate(ctx *gin.Context) gopkg.CodeError {
	c := util.GetContextFromGin(ctx)
	customerEmail := ctx.Query("email")
	if !EmailValidateRegex.MatchString(customerEmail) {
		return gopkg.NewCodeError(errors.EmailValidationFailed, "email validation failed")
	}
	model, err := trackerEventDbOp.GetByFields(c, []string{"data.email", "campaignId"}, []interface{}{customerEmail, util.GetInternalObjectFromGin(ctx).GetCampaignId()})
	if err != nil && err.ErrorCode() == errors.MongoEntityNotExists {
		return nil
	} else if model != nil {
		return gopkg.NewCodeError(errors.EmailAlreadySent, a.param.EmailRegisteredErrorMessage)
	}
	return err
}

func (a *actionSendEmailWithReminder) Task(ginCtx *gin.Context, respWrittenChannel chan struct{}) *concurrent.Task {
	taskFunc := func(ctx goctx.Context) *concurrent.TaskResult {
		var err gopkg.CodeError

		customerEmail := ginCtx.Query("email")
		if len(customerEmail) == 0 {
			return concurrent.NewTaskResult(nil, nil)
		}
		for _, emailParam := range a.param.Emails {
			emailTemplate := cache.DbTemplateCache[emailParam.TemplateName]
			if emailTemplate == nil {
				emailTemplate, err = emailTemplateDbOp.GetTemplateByName(ctx, emailParam.TemplateName)
				if err != nil {
					m800log.Errorf(ctx, "failed to get template with error=%+v", err)
				}
			}
			if emailTemplate != nil {
				cache.DbTemplateCache[emailTemplate.Name] = emailTemplate
				recipients := []string{customerEmail}

				if languageTemplate := emailTemplate.Templates[emailParam.Language]; languageTemplate == nil {
					if err != nil {
						err = gopkg.NewWrappedCarrierCodeError(errors.EmailMissingLanguageTemplate, "missing email template for language "+emailParam.Language, err)
					} else {
						err = gopkg.NewCodeError(errors.EmailMissingLanguageTemplate, "missing email template for language "+emailParam.Language)
					}
				} else {
					for _, recipient := range emailParam.Recipients {
						recipients = append(recipients, recipient)
					}
					var tplErr error
					emTpl := cache.EmailTemplateCache[emailParam.TemplateName+":"+emailParam.Language]
					if emTpl == nil {
						emTpl, tplErr = template.New(emailParam.TemplateName).Parse(languageTemplate.Content)
						if tplErr != nil {
							if err != nil {
								err = gopkg.NewWrappedCodeError(errors.EmailCannotParseTemplate, "cannot parse email template", err)
							} else {
								err = gopkg.NewCodeError(errors.EmailCannotParseTemplate, "cannot parse email template")
							}
						}
						cache.EmailTemplateCache[emailParam.TemplateName+":"+emailParam.Language] = emTpl
					}

					if emTpl != nil {
						buffer := new(bytes.Buffer)
						if executeTemplateErr := emTpl.Execute(buffer, emailParam.TemplateParams); executeTemplateErr != nil {
							if err != nil {
								err = gopkg.NewWrappedCodeError(errors.EmailCannotParseTemplate, "cannot fillin the email template parameters", err)
							} else {
								err = gopkg.NewWrappedCodeError(errors.EmailCannotParseTemplate, "cannot fillin the email template parameters", executeTemplateErr)
							}
						} else {
							m := &email.Mail{
								To:          recipients,
								Bcc:         emailParam.Bcc,
								Sender:      emailParam.Sender,
								Subject:     languageTemplate.Subject,
								Message:     string(buffer.Bytes()),
								ContentType: emailParam.ContentType,
							}
							sentErr := emailCli.Send(ctx, m)
							if sentErr != nil {
								if err != nil {
									err = gopkg.NewWrappedCodeError(sentErr.ErrorCode(), sentErr.ErrorMsg(), err)
								} else {
									err = sentErr
								}
							} else {
								m800log.Infof(ctx, "successfully send email to=%+v, bcc=%+v sender=%s subject=%s contentType=%s", m.To, m.Bcc, m.Sender, m.Subject, m.ContentType)
							}
						}
					}
				}
			}

			//Set reminders
			reminder := emailParam.Reminder
			if reminder != nil {
				recipients := append(emailParam.Recipients, customerEmail)
				if reminder.RemindTimes > 0 {
					iObj := util.GetInternalObjectFromGin(ginCtx)
					if reminder.CancelByEvent != nil && len(iObj.GetTrackingId()) == 0 {
						m800log.Error(ctx, "Tracking id cannot be null when reminder has cancel event")
					} else {
						emailReminder := &model.EmailReminder{
							RemindAt:       time.Now().Unix()*1000 + reminder.RemindInterval,
							RemindInterval: reminder.RemindInterval,
							TemplateName:   reminder.TemplateName,
							TemplateParams: reminder.TemplateParams,
							Tries:          reminder.RemindTimes,
							Sender:         emailParam.Sender,
							Recipients:     recipients,
							BccRecipients:  emailParam.Bcc,
							Language:       emailParam.Language,
							Cron:           reminder.Cron,
							CancelByEvent:  reminder.CancelByEvent,
							ContentType:    emailParam.ContentType,
							TrackingId:     iObj.GetTrackingId(),
						}
						createReminderErr := emailReminderDbOp.Create(ctx, emailReminder)
						if createReminderErr != nil {
							m800log.Errorf(ctx, "failed to create email reminder: %+v", err)
							if err != nil {
								err = gopkg.NewWrappedCodeError(createReminderErr.ErrorCode(), createReminderErr.ErrorMsg(), err)
							} else {
								err = createReminderErr
							}
						}
					}
				}
			}
		}

		return concurrent.NewTaskResult(nil, err)
	}
	return concurrent.NewTask(taskFunc, config.ActionEmailWithReminder.Executor)
}

func NewActionSendEmailWithReminder(param *ParamSendEmailWithReminder, id string) Action {
	return &actionSendEmailWithReminder{
		id:    id,
		param: param,
	}
}

type ParamSendEmailWithReminder struct {
	EmailRegisteredErrorMessage string                             `json:"emailRegisteredErrorMessage" bson:"emailRegisteredErrorMessage"`
	Emails                      []ParamSendEmailWithReminder_Email `json:"emails" bson:"emails"`
}

type ParamSendEmailWithReminder_Email struct {
	Sender         string                                     `json:"sender" bson:"sender"`
	Language       string                                     `json:"language" bson:"language"`
	Recipients     []string                                   `json:"recipients" bson:"recipients"`
	Bcc            []string                                   `json:"bcc" bson:"bcc"`
	ContentType    string                                     `json:"contentType" bson:"contentType"`
	TemplateName   string                                     `json:"templateName" bson:"templateName"`
	TemplateParams map[string]string                          `json:"templateParams" bson:"templateParams"`
	Reminder       *ParamSendEmailWithReminder_Email_Reminder `json:"reminder,omitempty" bson:"reminder,omitempty"`
}

type ParamSendEmailWithReminder_Email_Reminder struct {
	CancelByEvent  *string           `json:"cancelByEvent" bson:"cancelByEvent"`
	RemindInterval int64             `json:"remindInterval" bson:"remindInterval"`
	RemindTimes    int               `json:"remindTimes" bson:"remindTimes"`
	Cron           string            `json:"cron" bson:"cron"`
	TemplateName   string            `json:"templateName" bson:"templateName"`
	TemplateParams map[string]string `json:"templateParams" bson:"templateParams"`
}
