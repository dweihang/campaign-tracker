package action

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.com/cake/goctx"
	"gitlab.com/cake/gopkg"
	"gitlab.com/cake/m800log"
	"m800-campaign-tracker/concurrent"
	"m800-campaign-tracker/config"
	"m800-campaign-tracker/dbaccess/local/model"
	"m800-campaign-tracker/errors"
	"m800-campaign-tracker/jira/jiradto"
	"m800-campaign-tracker/util"
)

const NameCloseJiraTicket = "closeJiraTicket"

type closeJiraTicket struct {
	param *ParamCloseJiraTicket
	id    string
}

func NewActionCloseJiraTicket(param *ParamCloseJiraTicket, id string) Action {
	return &closeJiraTicket{
		param: param,
		id:    id,
	}
}

func (c *closeJiraTicket) Validate(ctx *gin.Context) gopkg.CodeError {
	return nil
}

func (c *closeJiraTicket) Task(ginCtx *gin.Context, respWrittenChannel chan struct{}) *concurrent.Task {
	taskFunc := func(ctx goctx.Context) *concurrent.TaskResult {
		iObj := util.GetInternalObjectFromGin(ginCtx)
		dbObj, err := actionStatusDbOp.Get(ctx, iObj.GetTrackingId(), c.param.CreateJiraTicketActionId)
		if err != nil {
			m800log.Errorf(ctx, "cannot find jira ticket number with trackingId=%s actionId=%s", iObj.GetTrackingId(), c.param.CreateJiraTicketActionId)
			return concurrent.NewTaskResult(nil, nil)
		}
		if dbObj.Status == model.ActionStatusError {
			err = gopkg.NewCarrierCodeError(errors.JIRACreateTicketCommentError, fmt.Sprintf("create ticket before encountered an error with trackingId=%s actionId=%s", iObj.GetTrackingId(), c.param.CreateJiraTicketActionId))
			m800log.Errorf(ctx, "%+v", err)
			return concurrent.NewTaskResult(nil, err)
		}
		param := &jiradto.CloseJiraTicketRequest{
			TicketNumber:     dbObj.ActionOutputData["id"].(string),
			JIRATransitionId: c.param.JIRATransitionId,
		}
		err = jiraCli.CloseJiraTicket(ctx, param)
		if err == nil {
			_ = actionStatusDbOp.Upsert(ctx, &model.ActionStatus{
				CampaignId:  iObj.GetCampaignId(),
				ActionName:  NameCloseJiraTicket,
				ActionId:    c.id,
				TrackerName: iObj.GetEvent(),
				TrackingId:  iObj.GetTrackingId(),
				Status:      model.ActionStatusDone,
				ErrMsg:      nil,
				TriggeredAt: iObj.GetEventTriggerTime(),
			})
		} else {
			errMsg := err.FullError()
			_ = actionStatusDbOp.Upsert(ctx, &model.ActionStatus{
				CampaignId:  iObj.GetCampaignId(),
				ActionName:  NameCloseJiraTicket,
				ActionId:    c.id,
				TrackerName: iObj.GetEvent(),
				TrackingId:  iObj.GetTrackingId(),
				Status:      model.ActionStatusError,
				ErrMsg:      &errMsg,
				TriggeredAt: iObj.GetEventTriggerTime(),
			})
		}

		return concurrent.NewTaskResult(nil, err)
	}
	return concurrent.NewTask(taskFunc, config.ActionJIRA.Executor)
}

type ParamCloseJiraTicket struct {
	CreateJiraTicketActionId string `json:"createJiraTicketActionId" bson:"createJiraTicketActionId"`
	JIRATransitionId         string `json:"jiraTransitionId" bson:"jiraTransitionId"`
}
