package action

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/cake/goctx"
	"gitlab.com/cake/gopkg"
	"m800-campaign-tracker/concurrent"
	"m800-campaign-tracker/config"
	"m800-campaign-tracker/dbaccess/local/model"
	"m800-campaign-tracker/util"
)

const NameSaveEventToMongo = "saveEventToMongo"

type saveEventToMongo struct {
	id    string
	param *ParamSaveEventToMongo
}

func (a saveEventToMongo) Validate(ctx *gin.Context) gopkg.CodeError {
	return nil
}

func (a saveEventToMongo) Task(ginctx *gin.Context, respWrittenChannel chan struct{}) *concurrent.Task {
	taskFunc := func(ctx goctx.Context) *concurrent.TaskResult {
		iObj := util.GetInternalObjectFromGin(ginctx)
		err := trackerEventDbOp.Create(ctx, &model.TrackerEvent{
			TrackerName: iObj.GetEvent(),
			CampaignID:  iObj.GetCampaignId(),
			TrackingID:  iObj.GetTrackingId(),
			TriggeredAt: iObj.GetEventTriggerTime(),
			IP:          iObj.GetIP(),
			Data:        a.param.Data,
		}, a.param.AllowSaveMultiple)
		return concurrent.NewTaskResult(nil, err)
	}
	return concurrent.NewTask(taskFunc, config.ActionMongo.Executor)
}

func NewActionSaveEventToMongo(param *ParamSaveEventToMongo, id string) Action {
	return &saveEventToMongo{
		id:    id,
		param: param,
	}
}

type ParamSaveEventToMongo struct {
	AllowSaveMultiple bool                   `bson:"allowSaveMultiple" json:"allowSaveMultiple"`
	Data              map[string]interface{} `bson:"data" json:"data"`
}
