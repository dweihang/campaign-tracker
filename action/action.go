package action

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/cake/gopkg"
	"m800-campaign-tracker/concurrent"
)

type Action interface {
	Validate(ctx *gin.Context) gopkg.CodeError
	Task(ctx *gin.Context, respWrittenChannel chan struct{}) *concurrent.Task
}
